import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.ArrayList;
 



import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
 
/**
 * @author helloworld922
 *         <p>
 * @version 1.0
 *          <p>
 *          copyright 2010 <br>
 * 
 *          You are welcome to use/modify this code for any purposes you want so
 *          long as credit is given to me.
 */
public class JTreeTransferHandler extends TransferHandler
{
	/**
	 * Using tree models allows us to add/remove nodes from a tree and pass the
	 * appropriate messages.
	 */
	protected JTree tree;
	/**
	 * 
	 */
	private static final long serialVersionUID = -6851440217837011463L;
 
	/**
	 * Creates a JTreeTransferHandler to handle a certain tree. Note that this
	 * constructor does NOT set this transfer handler to be that tree's transfer
	 * handler, you must still add it manually.
	 * 
	 * @param tree
	 */
	public JTreeTransferHandler(DnDJTree tree)
	{
		super();
		this.tree =  tree;
	}
 
	/**
	 * 
	 * @param c
	 * @return
	 */
	@Override
	public int getSourceActions(JComponent c)
	{
		return TransferHandler.COPY_OR_MOVE;
	}
 
	/**
	 * 
	 * @param c
	 * @return null if no nodes were selected, or this transfer handler was not
	 *         added to a DnDJTree. I don't think it's possible because of the
	 *         constructor layout, but one more layer of safety doesn't matter.
	 */
	@Override
	protected Transferable createTransferable(JComponent c)
	{
		if (c instanceof DnDJTree)
		{
			return new DnDTreeList(((DnDJTree) c).getSelection());
		}
		else
		{
			return null;
		}
	}
 
	/**
	 * @param c
	 * @param t
	 * @param action
	 */
	@Override
	protected void exportDone(JComponent c, Transferable t, int action)
	{
		if (action == TransferHandler.MOVE)
		{
			// we need to remove items imported from the appropriate source.
			try
			{
				// get back the list of items that were transfered
				ArrayList<TreePath> list = ((DnDTreeList) t
						.getTransferData(DnDTreeList.DnDTreeList_FLAVOR)).getNodes();
				for (int i = 0; i < list.size(); i++)
				{
					// remove them
					//this.tree.removeNodeFromParent((DnDNode) list.get(i).getLastPathComponent());
				}
			}
			catch (UnsupportedFlavorException exception)
			{
				// for debugging purposes (and to make the compiler happy). In
				// theory, this shouldn't be reached.
				exception.printStackTrace();
			}
			catch (IOException exception)
			{
				// for debugging purposes (and to make the compiler happy). In
				// theory, this shouldn't be reached.
				exception.printStackTrace();
			}
		}
	}
 
	/**
	 * 
	 * @param supp
	 * @return
	 */
	@Override
	public boolean canImport(TransferSupport supp)
	{
		// Setup so we can always see what it is we are dropping onto.
		supp.setShowDropLocation(true);
		//if (supp.isDataFlavorSupported(DnDTreeList.DnDTreeList_FLAVOR))
		//{
			// at the moment, only allow us to import list of DnDNodes
 
			// Fetch the drop path
			TreePath dropPath = ((JTree.DropLocation) supp.getDropLocation()).getPath();
			if (dropPath == null)
			{
				// Debugging a few anomalies with dropPath being null. In the
				// future hopefully this will get removed.
				System.out.println("Drop path somehow came out null");
				return false;
			}
			// Determine whether we accept the location
			TreeNode[] nodes = ((DefaultTreeModel)tree.getModel()).getPathToRoot((TreeNode) dropPath.getLastPathComponent());
            TreePath path = new TreePath(nodes);
            System.out.println(path.toString());    // Able to get the exact node here
            tree.setExpandsSelectedPaths(true);              
            tree.setSelectionPath(new TreePath(nodes));
            DefaultTreeModel model = (DefaultTreeModel)tree.getModel();
            DefaultMutableTreeNode root = (DefaultMutableTreeNode)model.getRoot();
            root.add(new DefaultMutableTreeNode("another_child"));
            model.reload(root);
		//}
		// something prevented this import from going forward
		return true;
	}
 
	/**
	 * 
	 * @param supp
	 * @return
	 */
	@Override
	public boolean importData(TransferSupport supp)
	{
		if (this.canImport(supp))
		{
 
			try
			{
				// Fetch the data to transfer
				Transferable t = supp.getTransferable();
				ArrayList<TreePath> list = ((DnDTreeList) t
						.getTransferData(DnDTreeList.DnDTreeList_FLAVOR)).getNodes();
				// Fetch the drop location
				TreePath loc = ((javax.swing.JTree.DropLocation) supp.getDropLocation()).getPath();
				// Insert the data at this location
				for (int i = 0; i < list.size(); i++)
				{
					/*this.tree.insertNodeInto((DnDNode) list.get(i).getLastPathComponent(),
							(DnDNode) loc.getLastPathComponent(), ((DnDNode) loc
									.getLastPathComponent()).getAddIndex((DnDNode) list.get(i)
									.getLastPathComponent()));*/
				}
				// success!
				return true;
			}
			catch (UnsupportedFlavorException e)
			{
				// In theory, this shouldn't be reached because we already
				// checked to make sure imports were valid.
				e.printStackTrace();
			}
			catch (IOException e)
			{
				// In theory, this shouldn't be reached because we already
				// checked to make sure imports were valid.
				e.printStackTrace();
			}
		}
		// import isn't allowed at this time.
		return false;
	}
}