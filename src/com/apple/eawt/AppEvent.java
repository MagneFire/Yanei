package com.apple.eawt;

import java.net.URI;
import java.util.EventObject;
import java.util.List;

/** @since 10.6 Update 3 and 10.5 Update 8 */
public abstract class AppEvent extends EventObject {
	private static final long serialVersionUID = 1L;
	AppEvent() {super(Application.getApplication());}

    public static class AboutEvent extends AppEvent {private static final long serialVersionUID = 1L;}
    public static class AppForegroundEvent extends AppEvent {private static final long serialVersionUID = 1L;}
    public static class AppHiddenEvent extends AppEvent {private static final long serialVersionUID = 1L;}
    public static class AppReOpenedEvent extends AppEvent {private static final long serialVersionUID = 1L;}
    public static class OpenURIEvent extends AppEvent {
    	private static final long serialVersionUID = 1L;
        public URI getURI() {return null;}
    }
    public static class PreferencesEvent extends AppEvent {private static final long serialVersionUID = 1L;}

    public static class QuitEvent extends AppEvent {private static final long serialVersionUID = 1L;}
    public static class ScreenSleepEvent extends AppEvent {private static final long serialVersionUID = 1L;}
    public static class SystemSleepEvent extends AppEvent {private static final long serialVersionUID = 1L;}
    public static class UserSessionEvent extends AppEvent {private static final long serialVersionUID = 1L;}

    public static class FilesEvent extends AppEvent {
    	private static final long serialVersionUID = 1L;
        public List<?> getFiles() {return null;}
    }
    public static class PrintFilesEvent extends FilesEvent {private static final long serialVersionUID = 1L;}
    public static class OpenFilesEvent extends FilesEvent {
    	private static final long serialVersionUID = 1L;
        public String getSearchTerm() {return null;}
    }
}

