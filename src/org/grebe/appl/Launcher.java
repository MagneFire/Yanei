package org.grebe.appl;

import java.awt.Dimension;
import java.awt.Point;
import java.util.Locale;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.WindowConstants;


public class Launcher {

	private static Monitor monitor;
	
    public static void main(String[] args){
    	//Debug to simulate -debug in command line
    	/*args = new String[2];
    	args[1] = "-lang";
    	args[0] = "-debug";*/
    	//Set the default language used in this application, testing only
    	//Locale locale = new Locale("en");
    	//Locale.setDefault(locale);
    	try {

            System.setProperty("apple.laf.useScreenMenuBar", "true");
            System.setProperty("com.apple.mrj.application.apple.menu.about.name", "Test");
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {}
    	
		if (args.length>0) {
			for (String arg:args) {
				if (arg.equals("-debug")) {
					if (monitor==null) {
						monitor = new Monitor(new Dimension(700, 300));	
					}
				} else if (arg.equals("-lang")) {
					Locale locale = new Locale("en");
					Locale.setDefault(locale);			
				}
			}
		}		
		long time = System.currentTimeMillis()/10;
		System.out.println("<o>Starting Program...</o>");
		System.out.println("Setting language: "+Locale.getDefault().getDisplayLanguage());
		//System.out.println("#Warning: testing warning message");
		
		
		final Main main=new Main();
		main.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        main.setPreferredSize(new Dimension(940, 580));
        main.pack();
        main.setLocationRelativeTo(null); //Center the application
        main.setVisible(true);//Show the actual frame
        main.updateVisibility();
        main.getComponent(0).requestFocusInWindow(); //fixes default focus on ip textfield

		System.out.println("<g>Program Started. (in "+((int)((System.currentTimeMillis()/10)-time))/100.0+" second(s))</g>");


		//Read all the debug options, and if we find the -debug option, then show the monitor
		if (args.length>0) {
			for (String arg:args) {
				if (arg.equals("-debug")) {
					if (monitor!=null) {
						 SwingUtilities.invokeLater(new Runnable() {
					            @Override
					            public void run() {
					            	try {
					            		main.setLocation(main.getLocation().x, main.getLocation().y-160);
					            		Point loc = main.getLocation();
					            		loc.x += (main.getContentPane().getWidth()/2)-(monitor.getWidth()/2); //Center horizontal
					            		loc.y += main.getHeight()+(((main.getWidth()-main.getContentPane().getWidth()))/2);
					            		monitor.setLocation(loc);
					            		monitor.setVisible(true);
					            		main.toFront();
					            	} catch(Exception ex) {
					            		System.out.println("Error while starting monitor!");
					            	}
					            }
					        });
						 break;
					}
				}
			}
		}
    }

}
