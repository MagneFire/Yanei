package org.grebe.appl;

import java.awt.BorderLayout;
import java.awt.Dimension;

import com.apple.eawt.AboutHandler;
import com.apple.eawt.PreferencesHandler;
import com.apple.eawt.QuitHandler;
import com.apple.eawt.Application;
import com.apple.eawt.QuitResponse;
import com.apple.eawt.AppEvent.AboutEvent;
import com.apple.eawt.AppEvent.PreferencesEvent;
import com.apple.eawt.AppEvent.QuitEvent;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.plaf.basic.BasicSplitPaneUI;

import org.grebe.components.Editor;
import org.grebe.controls.CloseController;
import org.grebe.controls.EditorController;
import org.grebe.controls.ExplorerController;
import org.grebe.controls.MenuController;
import org.grebe.controls.ToolbarController;
import org.grebe.controls.upload.SerialController;
import org.grebe.controls.upload.UploadController;
import org.grebe.examples.LuaExamples;
import org.grebe.lang.Lang;
import org.grebe.model.Model;
import org.grebe.utils.FileUtils;
import org.grebe.utils.Preferences;
import org.grebe.utils.Utils;
import org.grebe.views.AboutView;
import org.grebe.views.Console;
import org.grebe.views.PreferencesView;
import org.grebe.views.StatusBarView;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class Main extends JFrame implements Observer{
	
	private static final long serialVersionUID = 1L;
	private JPanel mainPanel;
	private Model model;
	private MenuController menuController;
	private SerialController serialController;
	private UploadController uploadController;
    private boolean isExited = false;
    private EditorController editorController;
    private Console console;
    private ExplorerController explorerController;
    private JSplitPane jspLeft, jspRight;
    private ToolbarController toolBarController;
    private StatusBarView statusBarView;
        
    private void setupLayout() {
        setTitle(Lang.getText(Lang.TITLE)); //Sets the frames title
        //Sets the applications icon
		try {
			setIconImage(ImageIO.read(getClass().getResourceAsStream(Utils.PATH_RESOURCES + "logo.png")));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		//sets the menu bar
    	menuController = new MenuController(this, model);	
    	setJMenuBar(menuController);
        
        
        
		mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
 
        //Create the toolbar.
        toolBarController = new ToolbarController(model);
        toolBarController.setFloatable(false);
        toolBarController.setRollover(true);
        toolBarController.setOpaque(false); //Fix for OSX
 
        //Create the text area used for output.  Request
        //enough space for 5 rows and 30 columns.
       /* textArea = new JTextArea(5, 30);
        textArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(textArea);*/
 
        //Lay out the main panel.
        setPreferredSize(new Dimension(450, 130));
        mainPanel.add(toolBarController, BorderLayout.PAGE_START);

        jspLeft = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        jspLeft.setResizeWeight(0.0);
        jspLeft.setContinuousLayout(true);
        explorerController = new ExplorerController(model);
        jspLeft.setLeftComponent(explorerController);
        
        jspRight = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        jspRight.setContinuousLayout(true);
        jspRight.setBorder(null);
        jspRight.setLeftComponent(editorController = new EditorController(model));        
        
        console  = new Console(model);
        jspRight.setRightComponent(console);
        jspRight.setResizeWeight(1.0);
        
        jspLeft.setRightComponent(jspRight);

        if (model.getDimensionExplorer()!=null) {
        	explorerController.setSize(model.getDimensionExplorer());
        	explorerController.setPreferredSize(model.getDimensionExplorer());
        }
        
        if (model.getDimensionConsole()!=null) {
        	console.setSize(model.getDimensionConsole());   
        	console.setPreferredSize(model.getDimensionConsole());        	
        }

    	
        mainPanel.add(jspLeft);
        mainPanel.add(statusBarView = new StatusBarView(model), BorderLayout.SOUTH);
        setContentPane(mainPanel);
    }
    
    
    /**
     * Loads all the settings
     */
    private void loadSettings() {
    	//Load all the settings
    	if (Preferences.readProperty("selectedPort")!=null) {
    		model.setSelectedDevice(Preferences.readProperty("selectedPort"));
    	}
    	if (Preferences.readProperty("explorerPath")==null) {
        	model.setExplorerPath(new File("."));
    	} else {
        	model.setExplorerPath(new File(Preferences.readProperty("explorerPath")));
    	}
    	if (Preferences.readProperty("consoleBufferSize")!=null) {
    		if (Utils.isNumeric(Preferences.readProperty("consoleBufferSize"))) {
    			model.setConsoleBufferSize(Integer.parseInt(Preferences.readProperty("consoleBufferSize")));
    		}
    	}    	

    	
    	if (Preferences.readProperty("explorerHidden")!=null) {
    		if (Utils.isBoolean(Preferences.readProperty("explorerHidden"))) {
    			model.setVisibleExplorer(Boolean.parseBoolean(Preferences.readProperty("explorerHidden").toLowerCase()));
    		}
    	}
    	if (Preferences.readProperty("consoleHidden")!=null) {
    		if (Utils.isBoolean(Preferences.readProperty("consoleHidden"))) {
    			model.setVisibleConsole(Boolean.parseBoolean(Preferences.readProperty("consoleHidden").toLowerCase()));
    		}
    	}
    	//JPanel pnlAT = new JPanel();
    	//pnlAT.add(new JLabel("AT commands or not implemented",SwingConstants.LEFT));
    	//model.addEditor(pnlAT);
    	//model.setVisibleEditor(pnlAT);
    	if (Preferences.readProperty("lastOpened")!=null) {
    		if (FileUtils.isFileInPath(model.getExplorerPath().getPath()+File.separator+Preferences.readProperty("lastOpened"))) {
    			Editor editor = new Editor(Preferences.readProperty("lastOpened"));
    			editor.setText(FileUtils.readFile(model.getExplorerPath().getPath()+File.separator+Preferences.readProperty("lastOpened")));
    			editor.discardAllEdits();
    			model.addEditor(editor);
    		}
    	}

		if (Preferences.readProperty("dimensionConsole")!=null) {
			if (Utils.isNumeric(Preferences.readProperty("dimensionConsole"))) {
				model.setDimensionConsole(new Dimension(Integer.parseInt(Preferences.readProperty("dimensionConsole")), 0));
			}
		}
		if (Preferences.readProperty("dimensionExplorer")!=null) {
			if (Utils.isNumeric(Preferences.readProperty("dimensionExplorer"))) {
				model.setDimensionExplorer(new Dimension(Integer.parseInt(Preferences.readProperty("dimensionExplorer")), 0));
			}
		}
		if (Preferences.readProperty("uploadMethod")!=null) {
			if (Utils.isNumeric(Preferences.readProperty("uploadMethod"))) {
				model.setUploadMethod(Integer.parseInt(Preferences.readProperty("uploadMethod")));
			}
		}
		
		if (Preferences.readProperty("uploadMode")!=null) {
			if (Utils.isNumeric(Preferences.readProperty("uploadMode"))) {
				model.setUploadMode(Integer.parseInt(Preferences.readProperty("uploadMode")));
			}
		}
		String ip = Preferences.readProperty("telnetIP");
		if ((ip != null) && (ip.length()!=0) && (!ip.equals("")) && (ip.trim().equals(ip))) {
				model.setTelnetIP(Preferences.readProperty("telnetIP"));
		}

		if (Preferences.readProperty("baudrate")!=null) {
			if (Utils.isNumeric(Preferences.readProperty("baudrate"))) {
				model.setBaudRate(Integer.parseInt(Preferences.readProperty("baudrate")));
			}
		}
    }
    
    /**
     * Updates the visibility of the console and explorer
     */
    public void updateVisibility() {
    	
    	BasicSplitPaneUI uiLeft = (BasicSplitPaneUI) jspLeft.getUI();
    	uiLeft.getDivider().setVisible(model.isExplorerVisible());	
    	if (model.isExplorerVisible() && !explorerController.isVisible()) {
    		explorerController.setVisible(model.isExplorerVisible());
    		if ((int)explorerController.getSize().getWidth()<=23) {
    			jspLeft.setDividerLocation((int)explorerController.getPreferredSize().getWidth()); 	    			
    		} else { 
    			if (jspLeft.getLastDividerLocation()<=0) {
    				jspLeft.setDividerLocation((int)explorerController.getSize().getWidth()); 
    			} else {
    				jspLeft.setDividerLocation(jspLeft.getLastDividerLocation());
    			}
    		}
    		model.setDimensionExplorer(explorerController.getSize());
    	} else if (model.isExplorerVisible() != explorerController.isVisible()) {
    		explorerController.setVisible(model.isExplorerVisible());
    	}

    	BasicSplitPaneUI uiRight = (BasicSplitPaneUI) jspRight.getUI();
    	uiRight.getDivider().setVisible(model.isConsoleVisible());	
    	if (model.isConsoleVisible() && !console.isVisible()) { 
        	console.setVisible(model.isConsoleVisible());
    		if ((int)console.getSize().getWidth()<=23) { //Should be no need to set divider location
    			if (jspRight.getSize().width==0) {
    				jspRight.setDividerLocation((int)((jspRight.getPreferredSize().getWidth())-console.getPreferredSize().getWidth()));
    			} else {    				
    				jspRight.setDividerLocation((int)((jspRight.getSize().getWidth())-console.getPreferredSize().getWidth()));
    			}
    		} else {
    			if (jspRight.getLastDividerLocation()<=0) {
    				jspRight.setDividerLocation((int)(jspRight.getSize().getWidth())-(int)console.getSize().getWidth());	 
    			} else {
    				jspRight.setDividerLocation(jspRight.getSize().width-console.getSize().width - uiRight.getDivider().getWidth());
    			}
    		}
    		model.setDimensionConsole(console.getSize());
    	} else if (model.isConsoleVisible() != console.isVisible()) {
        	console.setVisible(model.isConsoleVisible());    		
    	}
    }
    
    
    public Main(){    	
		model = new Model();
		loadSettings();

    	setupLayout();
    	uploadController = new UploadController(model);
		serialController = new SerialController(model);
		uploadController.setSerialController(serialController);


        model.addObserver(this); //Yes this is a controller
		model.addObserver(new CloseController(model));
		model.addObserver(uploadController);
		model.addObserver(toolBarController);
		model.addObserver(console);
		model.addObserver(explorerController);
		model.addObserver(menuController);
		model.addObserver(editorController);	
		model.addObserver(statusBarView);

		explorerController.setDir();
    	if (!model.getEditors().isEmpty()) {
    		if (model.getEditors().get(0) instanceof Editor)
    			explorerController.select(model.getExplorerPath().getPath() + File.separator + ((Editor)model.getEditors().get(0)).getFilename());
    	}
		
        setContentPane(mainPanel);

        final JFrame frame = this;
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
            	//Sent close to all observers
 				model.requestExit();
            }
        });
        if (Utils.getOperatingSystem()==Utils.OS_MACOSX) {
        	System.out.println("<g>Running on a Mac machine</g>");
        	System.setProperty("apple.laf.useScreenMenuBar", "true");
        	 Application.getApplication().setAboutHandler(new AboutHandler() {
     			
     			@Override
     			public void handleAbout(AboutEvent e) {
     				//Show about popup

					AboutView about = new AboutView(frame);	
					about.showDialog();
     			}
     		});
             Application.getApplication().setQuitHandler(new QuitHandler() {
     			
     			@Override
     			public void handleQuitRequestWith(QuitEvent e, QuitResponse response) { //Catch Command-Q
                	//Sent close to all observers
     				model.requestExit();
     				response.cancelQuit(); //Cancel quit
     				//response.performQuit(); //Quit program
     			}			
     		});
             Application.getApplication().setPreferencesHandler(new PreferencesHandler() {
				
				@Override
				public void handlePreferences(PreferencesEvent e) {
					PreferencesView prefs = new PreferencesView(model, frame);	
					prefs.showDialog();
				}
			});
        } else if (Utils.getOperatingSystem()==Utils.OS_WINDOWS) {
        	System.out.println("<g>Running on a Windows machine</g>");
        } else {
        	System.out.println("<o>Running on an unknown machine, not supported</o>");        	
        }
        serialController.start();
        serialController.setEnablePortRefresh(true);
    }    
 
   
    public void close() {
    	//Update one last time, to ensure that the dimensions of explorer
    	//and console are properly saved to the model
    	updateVisibility();
    	//Nothing to do
    }

    public void update(Observable o, Object obj){
    	if (Model.getObserver((Integer)obj)==Model.UPDATE_APPLICATION) {
	    	if (model.getExit()==(model.countObservers()-1)) { //All classes have closed
	    		model.setExit();
	    		System.out.println("<g>Everything is closed</g> <o>Now exiting...</o>");
    			new Thread(new Runnable() {					
					@Override
					public void run() {
			    		System.exit(0);
					}
				}).start();
	    	} else if (model.getExit()>-2) { //Damn perform an exit
	    		if (!isExited) { //allow this class to only call model.setExit() once
	    			isExited = true;
	    			close();
	    			System.out.println("<g>"+o.getClass() + ": "+getClass()+" is closed!</g>");
	    			model.setExit();
	    		}
	    	}  	
    	} else if (Model.getObserver((Integer)obj)==Model.UPDATE_CONNECTION_STATE) {
    		if (model.getConnectionState()==Model.CONNECTION_STATE_FAIL) {
    			JOptionPane.showMessageDialog(this, Lang.getText(Lang.MSG_CONNECTION_ERROR), Lang.getText(Lang.TITLE_CONNECTION_ERROR), JOptionPane.ERROR_MESSAGE);
    			model.setConnectionState(Model.CONNECTION_STATE_CLOSED);
    		}
    	} else if (Model.getObserver((Integer)obj)==Model.UPDATE_VISIBILITY) {
    		updateVisibility();
    	} else if (Model.getObserver((Integer)obj)==Model.UPDATE_MAIN) {
    		if (model.getVisibleEditor() instanceof Editor) {
    			if (((Editor)model.getVisibleEditor()).getFilename().split(LuaExamples.PATH).length==2) {
    				setTitle(Lang.getText(Lang.TITLE)+" - <"+Lang.getText(Lang.EXAMPLES)+">"+((Editor)model.getVisibleEditor()).getFilename().split(LuaExamples.PATH)[1]); //Sets the frames
    			} else {
        			setTitle(Lang.getText(Lang.TITLE)+" - "+((Editor)model.getVisibleEditor()).getFilename()); //Sets the frames
    				
    			}
    		}
    	}
    }
}