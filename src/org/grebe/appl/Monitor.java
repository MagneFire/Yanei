package org.grebe.appl;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

import org.grebe.utils.Utils;


public class Monitor extends JFrame{

	private static final long serialVersionUID = 1L;
	private JTextPane tPane;
	
	public Monitor() {

		BufferedImage img;
		try {
			img = ImageIO.read(getClass().getResourceAsStream(Utils.PATH_RESOURCES+"debug.png"));
	        setIconImage(img);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		tPane = new JTextPane();
		tPane.setEditable(false);
		tPane.setBackground(Color.BLACK);
		System.setOut(new PrintStream(new OutStream(Color.WHITE)));
		System.setErr(new PrintStream(new OutStream(Color.RED)));
		setLayout(new BorderLayout());
		JPanel pnl = new JPanel(new BorderLayout());
		pnl.add(tPane);
		add(new JScrollPane(pnl));
		setTitle("Monitor");
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setPreferredSize(new Dimension(530, 460));
		pack();
	}
	
	public Monitor(Dimension size) {
		this();
		setPreferredSize(size);
		pack();
	}
	
	private void appendToPane(JTextPane tp, String msg, Color c)
    {
        AttributeSet aset = StyleContext.getDefaultStyleContext().addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);

        StyledDocument doc = tp.getStyledDocument();
        try {
			doc.insertString(doc.getLength(), msg, aset);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
        
        int len = tp.getDocument().getLength();
        tp.setCaretPosition(len);
        tp.setCharacterAttributes(aset, false);
    }
	
	private class OutStream extends OutputStream {

		private String line = "";
		private ArrayList<Color> colorStack;		
		
		public OutStream(Color color) {
			colorStack = new ArrayList<Color>();
			colorStack.add(color);
		}
		
		private synchronized void writeString(String line) {
			line = line.replaceAll("\n", "");
			if (line.matches("(.*)<(.*)>(.*)") && line.matches("(.*)</(.*)>(.*)")) {
				char colpre = line.charAt(line.indexOf("<")+1);
				line+="\n";
				int begin = line.indexOf("<"+colpre+">");
				line = line.substring(0, begin)+line.substring(begin+3, line.length());
	
				int end = line.indexOf("</"+colpre+">");
				line = line.substring(0, end)+line.substring(end+4, line.length());
				
				for (int i=0; i<end;i++) {
					if (i==begin) {
						switch(colpre) {
						case 'm':
							colorStack.add(Color.MAGENTA);
						break;
						case 'q':
							colorStack.add(Color.GRAY);
						break;
						case 'c':
							colorStack.add(Color.CYAN);
						break;
						case 'z':
							colorStack.add(Color.BLACK);
						break;
						case 'b':
							colorStack.add(Color.BLUE);
						break;
						case 'y':
							colorStack.add(Color.YELLOW);
						break;
						case 'r':
							colorStack.add(Color.RED);
						break;
						case 'g':
							colorStack.add(Color.GREEN);
						break;
						case 'o':
							colorStack.add(Color.ORANGE);
						break;
							
						}
					}
			        appendToPane(tPane, ""+line.charAt(i), colorStack.get(colorStack.size()-1));						
				}
				colorStack.remove(colorStack.size()-1);
				
				if ((line.length())!=end) {
					writeString(line.substring(end, line.length()));
				}
			} else if (line.startsWith("#")) { //Warning detected
				line+="\n";
		        appendToPane(tPane, line, Color.ORANGE);					
			} else {
				line+="\n";
		        appendToPane(tPane, line, colorStack.get(colorStack.size()-1));	
			}
		}
		
		@Override
		public synchronized void write(int b) throws IOException {
			if (b!=13) { //Fix for windows
				line +=(char)b;
			}
			if (b==10) {
				
				writeString(line);
		        line = "";
			}
		}	
	}
	
}