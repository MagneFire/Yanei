package org.grebe.components;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;

import javax.swing.JPanel;

import org.fife.ui.autocomplete.AutoCompletion;
import org.fife.ui.autocomplete.BasicCompletion;
import org.fife.ui.autocomplete.CompletionProvider;
import org.fife.ui.autocomplete.DefaultCompletionProvider;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;


public class Editor extends JPanel {

	//Undo System needs a rework!, it is broken
	private static final long serialVersionUID = 1L;
	
	private RSyntaxTextArea tPane;
	private String filename;
	private boolean saved = true;
	
	public Editor(String filename) {
		this.filename = filename;
		tPane = new RSyntaxTextArea();
		tPane.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_LUA);
		tPane.setCodeFoldingEnabled(true);
		tPane.setAntiAliasingEnabled(true);
		tPane.setFont(new Font("Lucida Grande", Font.PLAIN, 14));
		//tPane.addKeyListener(this);
		
		setLayout(new BorderLayout());
		add(new RTextScrollPane(tPane));
	    CompletionProvider provider = createCompletionProvider();

	    AutoCompletion ac = new AutoCompletion(provider);
	    ac.install(tPane);
	}
	
	 /**
	    * Create a simple provider that adds some Java-related completions.
	    * 
	    * @return The completion provider.
	    */
	   private CompletionProvider createCompletionProvider() {

	      // A DefaultCompletionProvider is the simplest concrete implementation
	      // of CompletionProvider. This provider has no understanding of
	      // language semantics. It simply checks the text entered up to the
	      // caret position for a match against known completions. This is all
	      // that is needed in the majority of cases.
		  
	      DefaultCompletionProvider provider = new DefaultCompletionProvider();
	     // LanguageAwareCompletionProvider provider = new LanguageAwareCompletionProvider(provider);

	      // Add completions for all Java keywords. A BasicCompletion is just
	      // a straightforward word completion.

	      provider.addCompletion(new BasicCompletion(provider, "function end"));
	      provider.addCompletion(new BasicCompletion(provider, "function"));
	      provider.addCompletion(new BasicCompletion(provider, "function return end"));
	      provider.addCompletion(new BasicCompletion(provider, "end"));
	      provider.addCompletion(new BasicCompletion(provider, "do"));
	      provider.addCompletion(new BasicCompletion(provider, "print(\"\")"));
	      provider.addCompletion(new BasicCompletion(provider, "if"));
	      provider.addCompletion(new BasicCompletion(provider, "else"));
	      provider.addCompletion(new BasicCompletion(provider, "elseif"));
	      provider.addCompletion(new BasicCompletion(provider, "if else end"));
	      provider.addCompletion(new BasicCompletion(provider, "while"));
	      provider.addCompletion(new BasicCompletion(provider, "while do end"));
	      provider.addCompletion(new BasicCompletion(provider, "do end"));
	      provider.addCompletion(new BasicCompletion(provider, "for"));
	      provider.addCompletion(new BasicCompletion(provider, "for do end"));
	      provider.addCompletion(new BasicCompletion(provider, "repeat"));
	      provider.addCompletion(new BasicCompletion(provider, "until"));
	      provider.addCompletion(new BasicCompletion(provider, "repeat until"));
	      provider.addCompletion(new BasicCompletion(provider, "for"));
	      provider.addCompletion(new BasicCompletion(provider, "for key, value in pairs() do\r\nend"));
	      provider.addCompletion(new BasicCompletion(provider, "for do end"));
	      provider.addCompletion(new BasicCompletion(provider, "wifi.setmode()"));
	      provider.addCompletion(new BasicCompletion(provider, "wifi.STATION"));
	      provider.addCompletion(new BasicCompletion(provider, "wifi.SOFTAP"));
	      provider.addCompletion(new BasicCompletion(provider, "wifi.STATIONAP"));
	      provider.addCompletion(new BasicCompletion(provider, "wifi.getmode()"));
	      provider.addCompletion(new BasicCompletion(provider, "wifi.startsmart()"));
	      provider.addCompletion(new BasicCompletion(provider, "wifi.stopsmart()"));
	      provider.addCompletion(new BasicCompletion(provider, "wifi.sta.config(\"SSID\",\"password\")"));
	      provider.addCompletion(new BasicCompletion(provider, "wifi.sta.connect()"));
	      provider.addCompletion(new BasicCompletion(provider, "wifi.sta.disconnect()"));
	      provider.addCompletion(new BasicCompletion(provider, "wifi.sta.autoconnect()"));
	      provider.addCompletion(new BasicCompletion(provider, "wifi.sta.getip()"));
	      provider.addCompletion(new BasicCompletion(provider, "wifi.sta.getmac()"));
	      provider.addCompletion(new BasicCompletion(provider, "wifi.sta.getap()"));
	      provider.addCompletion(new BasicCompletion(provider, "wifi.sta.status()"));
	      provider.addCompletion(new BasicCompletion(provider, "wifi.ap.config()"));
	      provider.addCompletion(new BasicCompletion(provider, "wifi.ap.getip()"));
	      provider.addCompletion(new BasicCompletion(provider, "wifi.ap.getmac()"));
	      provider.addCompletion(new BasicCompletion(provider, "gpio.mode(pin,gpio.OUTPUT)"));
	      provider.addCompletion(new BasicCompletion(provider, "gpio.write(pin,gpio.HIGH)"));
	      provider.addCompletion(new BasicCompletion(provider, "gpio.write(pin,gpio.LOW)"));
	      provider.addCompletion(new BasicCompletion(provider, "gpio.read(pin)"));
	      provider.addCompletion(new BasicCompletion(provider, "gpio.trig(0, \"act\",func)"));
	      provider.addCompletion(new BasicCompletion(provider, "conn=net.createConnection(net.TCP, 0)"));
	      provider.addCompletion(new BasicCompletion(provider, "net.createConnection(net.TCP, 0)"));
	      provider.addCompletion(new BasicCompletion(provider, "on(\"receive\", function(conn, payload) print(payload) end )"));
	      provider.addCompletion(new BasicCompletion(provider, "connect(80,\"0.0.0.0\")"));
	      provider.addCompletion(new BasicCompletion(provider, "send(\"GET / HTTP/1.1\\r\\nHost: www.baidu.com\\r\\nConnection: keep-alive\\r\\nAccept: */*\\r\\n\\r\\n\")"));
	      provider.addCompletion(new BasicCompletion(provider, "srv=net.createServer(net.TCP)"));
	      provider.addCompletion(new BasicCompletion(provider, "srv:listen(80,function(conn) \nconn:on(\"receive\",function(conn,payload) \nprint(payload) \nconn:send(\"<h1> Hello, NodeMcu.</h1>\")\nend) \nconn:on(\"sent\",function(conn) conn:close() end)\nend)"));
	      provider.addCompletion(new BasicCompletion(provider, "net.createServer(net.TCP, timeout)"));
	      provider.addCompletion(new BasicCompletion(provider, "net.server.listen(port,[ip],function(net.socket))"));
	      provider.addCompletion(new BasicCompletion(provider, "dns(domain, function(net.socket, ip))"));
	      provider.addCompletion(new BasicCompletion(provider, "pwm.setduty(0,0)"));
	      provider.addCompletion(new BasicCompletion(provider, "pwm.getduty(0)"));
	      provider.addCompletion(new BasicCompletion(provider, "pwm.setup(0,0,0)"));
	      provider.addCompletion(new BasicCompletion(provider, "pwm.start(0)"));
	      provider.addCompletion(new BasicCompletion(provider, "pwm.close(0)"));
	      provider.addCompletion(new BasicCompletion(provider, "pwm.setclock(0, 100)"));
	      provider.addCompletion(new BasicCompletion(provider, "pwm.getclock(0)"));
	      provider.addCompletion(new BasicCompletion(provider, "pwm.close(0)"));
	      provider.addCompletion(new BasicCompletion(provider, "file.open(\"\",\"r\")"));
	      provider.addCompletion(new BasicCompletion(provider, "file.writeline()"));
	      provider.addCompletion(new BasicCompletion(provider, "file.readline()"));
	      provider.addCompletion(new BasicCompletion(provider, "file.write()"));
	      provider.addCompletion(new BasicCompletion(provider, "file.close()"));
	      provider.addCompletion(new BasicCompletion(provider, "file.remove()"));
	      provider.addCompletion(new BasicCompletion(provider, "file.flush()"));
	      provider.addCompletion(new BasicCompletion(provider, "file.seek()"));
	      provider.addCompletion(new BasicCompletion(provider, "file.list()"));
	      provider.addCompletion(new BasicCompletion(provider, "node.restart()"));
	      provider.addCompletion(new BasicCompletion(provider, "node.dsleep()"));
	      provider.addCompletion(new BasicCompletion(provider, "node.chipid()"));
	      provider.addCompletion(new BasicCompletion(provider, "node.heap()"));
	      provider.addCompletion(new BasicCompletion(provider, "node.key(type, function())"));
	      provider.addCompletion(new BasicCompletion(provider, "node.led()"));
	      provider.addCompletion(new BasicCompletion(provider, "node.input()"));
	      provider.addCompletion(new BasicCompletion(provider, "node.output()"));
	      provider.addCompletion(new BasicCompletion(provider, "tmr.alarm(0,1000,1,function()\nend)"));
	      provider.addCompletion(new BasicCompletion(provider, "tmr.delay()"));
	      provider.addCompletion(new BasicCompletion(provider, "tmr.now()"));
	      provider.addCompletion(new BasicCompletion(provider, "tmr.stop(id)"));
	      provider.addCompletion(new BasicCompletion(provider, "tmr.wdclr()"));
	      provider.addCompletion(new BasicCompletion(provider, "dofile(\"\")"));

	      return provider;

	   }

	
	   
	public void setSaved(boolean saved) {
		this.saved = saved;
	}
	
	public boolean isSaved() {
		return saved;
	}
	public boolean canUndo() {
		return tPane.canUndo();
	}
	
	public void undo() {
		if (tPane.canUndo()) {
			tPane.undoLastAction();			
		}	
	}

	public boolean canRedo() {
		return tPane.canRedo();
	}
	
	public void redo() {
		if (tPane.canRedo()) {
			tPane.redoLastAction();
		}	
	}
	/**
	 * Transfers the currently selected range in the associated text model to the system clipboard, and removing the current selected text in the editor.
	 * Does nothing for null selections.
	 */
	public void cut() {
		tPane.cut();
	}
	
	/**
	 * Transfers the currently selected range in the associated text model to the system clipboard, doing nothing to the current selected text in the editor.
	 * The current selection remains intact. Does nothing for null selections.
	 */
	public void copy() {
		tPane.copy();
	}
	/**
	 * Pastes the current clipboard contents to the editor
	 */
	public void paste() {
		tPane.paste();
	}
	/**
	 * Deletes the selection
	 */
	public void delete() {
		tPane.replaceSelection("");
	}
	
	/**
	 * Adds a keylistener, used to set the editor saved state to false
	 */
	public void addKeyListener(KeyListener listener) {
		tPane.addKeyListener(listener);
	}
	/**
	 * Check if the keylistener already exists
	 * @param listener the keylistener to be checked
	 * @return true if the keylistener exist, else returns false
	 */
	public boolean hasKeyListener(KeyListener listener) {
		for (KeyListener l : tPane.getKeyListeners()) {
			if (l==listener) return true; 
		}
		return false;
	}
	/**
	 * Removes the keylistener, used to set the editor saved state to false
	 */
	public void removeKeyListener(KeyListener listener) {
		tPane.removeKeyListener(listener);
	}

	/**
	 * Check if the mouselistener already exists
	 * @param listener the mouselistener to be checked
	 * @return true if the mouselistener exist, else returns false
	 */
	public boolean hasMouseListener(MouseListener listener) {
		for (MouseListener l : tPane.getMouseListeners()) {
			if (l==listener) return true; 
		}
		return false;
	}
	/**
	 * Adds a mouselistener to the editor, used for the popup menu(copy/paste menu)
	 */
	public void addMouseListener(MouseListener listener) {
		tPane.addMouseListener(listener);
	}
	/**
	 * Removes a mouselistener from the editor
	 */
	public void removeMouseListener(MouseListener listener) {
		tPane.removeMouseListener(listener);
	}
	/**
	 * Returns the filename used for saving, basically the identifier of the editor
	 * @return the filename of the editors text source/target
	 */
	public String getFilename() {
		return filename;
	}
	/**
	 * Selects all the text in the editor. Does nothing on a null or empty document.
	 */
	public void sellectAll() {
		tPane.selectAll();
	}
	
	/**
	 * Returns the selected text contained in this TextComponent. If the selection is null or the document empty, returns null.
	 * @return The selected text
	 */
	public String getSelectedText() {
		return tPane.getSelectedText();
	}
	
	/**
	 * Returns the text at the current line of the caret, returns null if unsuccessful
	 * @return The line at which the caret is positioned
	 */
	public String getCurrentLine() {
		try {
		return tPane.getText().split("\n")[tPane.getCaretLineNumber()];
		} catch (Exception e) {};
		return null;
	}

	/**
	 * Returns all the text in the editor
	 * @return the text
	 */
	public String getText() {
		return tPane.getText();
	}
	
	/**
	 * Sets the text in the editor
	 * @param text the text to be set
	 */
	public void setText(String text) {
		tPane.setText(text);
	}
	
	public void discardAllEdits() {
		tPane.discardAllEdits();
	}
}
