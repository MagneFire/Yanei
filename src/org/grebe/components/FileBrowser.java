package org.grebe.components;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.io.File;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.TransferHandler.TransferSupport;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;



public class FileBrowser extends JScrollPane {
	private static final long serialVersionUID = 1L;
	private File root;
	private JTree tree;
	private ActionListener copyListener;
  /** Construct a FileTree */
  public FileBrowser() {
    //setLayout(new BorderLayout());

    
    // Make a tree list with all the nodes, and make it a JTree
    tree = new JTree(new DefaultMutableTreeNode("No files found..."));
    tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);

    tree.setCellRenderer(new FileTreeCellRenderer());
    
	tree.setDragEnabled(false);
	tree.setTransferHandler(new FSTransfer());
    
    // Lastly, put the JTree into a JScrollPane.
    getViewport().add(tree);
    //add(BorderLayout.CENTER, scrollpane);
  }

  public FileBrowser(File dir) {
	  this();

    root = dir;

    refresh();

    
  }
  public void setMouseListener(MouseAdapter ma) {
	  tree.addMouseListener(ma);
  }

  public void setTreeSelectionListener(TreeSelectionListener listener) {
	  tree.addTreeSelectionListener(listener);
  }
  public void setDragListener(ActionListener listener) {
	  copyListener = listener;
  }
  
  public void setRoot(File dir) {
	  root = dir;
  }
  
  public void selectNode(File f) {
	  if (f!=null) {
		  DefaultMutableTreeNode searchNode =(DefaultMutableTreeNode) tree.getModel().getRoot();
		  //System.out.println("IN SELECTION NODE BEGIN: "+f.getPath());
		  for (String dir : (f.getPath()).split((File.separator.equals("\\") ? File.separator+File.separator : File.separator))) {
			  //System.out.println(dir);
			  for (int i=0;i<searchNode.getChildCount();i++) {
				  if (dir.equals(searchNode.getChildAt(i).toString())) {
					  searchNode = (DefaultMutableTreeNode) searchNode.getChildAt(i);
					  break;
				  }
			  }
			  //searchNode = searchNode.getChi
		  }
	      if (searchNode != null) {
		          TreeNode[] nodes = ((DefaultTreeModel) tree.getModel()).getPathToRoot(searchNode);
		          TreePath tpath = new TreePath(nodes);
		          tree.scrollPathToVisible(tpath);
		          tree.setSelectionPath(tpath);
	      }
	  } else {
		  //Now deselect the selected path.
		  //We need to temporary remove all TreeSelectionListeners other wise
		  //These are called, because we are changing the selection
		  TreeSelectionListener tsls[] = tree.getTreeSelectionListeners();
		  for (TreeSelectionListener tsl : tsls)
			  tree.removeTreeSelectionListener(tsl);
		  tree.setSelectionPath(null);
		  for (TreeSelectionListener tsl : tsls)
			  tree.addTreeSelectionListener(tsl);
	  }
  }
  /*
  private void listNodes(DefaultMutableTreeNode nodes) {
	  for (int i=0;i<nodes.getChildCount();i++) {
		  System.out.println(nodes.getChildAt(i));
		  if (nodes.getChildAt(i).getChildCount()!=0) {
			  listNodes((DefaultMutableTreeNode) nodes.getChildAt(i));
		  }
	  }
  }*/
  
  public void refresh() {
	  //Will at this moment clear the selection
	  //DefaultMutableTreeNode newList = addNodes(null, root);
	  //listNodes(newList);
	    // Make a tree list with all the nodes, and make it a JTree
	    tree.setModel(new DefaultTreeModel(addNodes(null, root)));

	    tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
	    //System.out.println(((DefaultMutableTreeNode)tree.getModel().getRoot()).getChildCount());
  }
  
  /** Add nodes from under "dir" into curTop. Highly recursive.
   * WILL NEED CHANGING
   */
  DefaultMutableTreeNode addNodes(DefaultMutableTreeNode curTop, File dir) {
    String curPath = dir.getName();
    DefaultMutableTreeNode curDir = new DefaultMutableTreeNode(curPath);
    if (curTop != null) { // should only be null at root
      curTop.add(curDir);
    }
    Vector<String> ol = new Vector<String>();
    String[] tmp = dir.list();
    for (int i = 0; i < tmp.length; i++)
      ol.addElement(tmp[i]);
    Collections.sort(ol, String.CASE_INSENSITIVE_ORDER);
    File f;
    Vector<String> files = new Vector<String>();
    // Make two passes, one for Dirs and one for Files. This is #1.
    for (int i = 0; i < ol.size(); i++) {
      String thisObject = (String) ol.elementAt(i);
      String newPath;
        newPath = dir.getPath() + File.separator + thisObject;
      if ((f = new File(newPath)).isDirectory()) {
    	  if (!f.getName().startsWith(".")) {
    		  addNodes(curDir, f);
    	  }
      } else {
        files.addElement(thisObject);
      }
    }
    // Pass two: for files.
    for (int fnum = 0; fnum < files.size(); fnum++) {
      curDir.add(new DefaultMutableTreeNode(files.elementAt(fnum)));
    }
    return curDir;
  }

  public Dimension getMinimumSize() {
    return new Dimension(140, 200);
  }

  public Dimension getPreferredSize() {
    return new Dimension(140, 200);
  }
  class FSTransfer extends TransferHandler {
	private static final long serialVersionUID = 1L;

	public boolean importData(JComponent comp, Transferable t) {
	    if (!(comp instanceof JTree)) {
	      return false;
	    }
	    if (!t.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
	      return false;
	    }
	    if (copyListener==null)
	    	return false;
	    try {
	      List<?> data = (List<?>) t.getTransferData(DataFlavor.javaFileListFlavor);
	      Iterator<?> i = data.iterator();
	      while (i.hasNext()) {
	        File f = (File) i.next();
	        copyListener.actionPerformed(new ActionEvent(f, 0, "", 0));
	        //Utils.copyFileUsingChannel(f, new File(model.getExplorerPath().getPath()+"/"+f.getName()));
	        //System.out.println(f.getPath());
	      }
	      refresh();
	      return true;
	    } catch (Exception ioe) {
	    	ioe.printStackTrace();
	    }
	    return false;
	  }

	public boolean canImport(TransferSupport supp)
	{
		supp.setShowDropLocation(true);
 
		// Fetch the drop path
		TreePath dropPath = ((JTree.DropLocation) supp.getDropLocation()).getPath();
		
		if (dropPath == null) return false;
		
        SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
		        tree.requestFocus();
		        tree.requestFocusInWindow();
		        tree.repaint();
			}
		});
		// something prevented this import from going forward
		return true;
	}
	}

/** A TreeCellRenderer for a File. */
class FileTreeCellRenderer extends DefaultTreeCellRenderer {
	private static final long serialVersionUID = 1L;

	private FileSystemView fileSystemView;

    private JLabel label;

    FileTreeCellRenderer() {
        label = new JLabel();
        label.setOpaque(true);
        fileSystemView = FileSystemView.getFileSystemView();
    }

    @Override
    public Component getTreeCellRendererComponent(
        JTree tree,
        Object value,
        boolean selected,
        boolean expanded,
        boolean leaf,
        int row,
        boolean hasFocus) {

        DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;
        String path = "";
        if (node != node.getRoot()) {
        	path = (String) node.getUserObject();
	        while (node.getParent()!=null && (node.getParent() != node.getRoot())) {        	
	        	node = (DefaultMutableTreeNode) node.getParent();
	        	path = (String) node.getUserObject() + File.separator + path;
	        }
        }
        path = root.getPath() + File.separator + path;
        File file = new File(path);
        label.setIcon(fileSystemView.getSystemIcon(file));
        label.setText(fileSystemView.getSystemDisplayName(file));
        label.setToolTipText(file.getPath());

        if (selected) {
            label.setBackground(backgroundSelectionColor);
            label.setForeground(textSelectionColor);
        } else {
            label.setBackground(backgroundNonSelectionColor);
            label.setForeground(textNonSelectionColor);
        }

        return label;
    }
}
}