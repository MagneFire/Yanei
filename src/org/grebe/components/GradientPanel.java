package org.grebe.components;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JComponent;
import javax.swing.JPanel;

public class GradientPanel extends JPanel{

	private static final long serialVersionUID = 1L;
	private Color topColor = getBackground().darker();
	private Color bottomColor = getBackground();
	
	public GradientPanel(JComponent comp) {
		add(comp);
	}
	
	public GradientPanel(JComponent comp, Color topColor, Color bottomColor) {
		add(comp);
		this.topColor = topColor;
		this.bottomColor = bottomColor;
	}

	public void setColorTop(Color color) {
		topColor = color;
	}
	
	public void setColorBottom(Color color) {
		bottomColor = color;
	}
	
	@Override
	protected void paintComponent(Graphics g){
	    // Create the 2D copy
	    Graphics2D g2 = (Graphics2D)g.create();

	    // Apply vertical gradient
	    g2.setPaint(new GradientPaint(0, 0, topColor, 0, getHeight(), bottomColor));
	    g2.fillRect(0, 0, getWidth(), getHeight());

	    // Dipose of copy
	    g2.dispose();
	}
}
