package org.grebe.controls;

import java.util.Observable;
import java.util.Observer;

import org.grebe.components.Editor;
import org.grebe.model.Model;
import org.grebe.utils.Preferences;

public class CloseController implements Observer{

	//CloseController is responsable for saving the settings to the config file
	private Model model;
    private boolean isExited = false;
	public CloseController(Model model) {
		this.model = model;
	}

    public void close() {
    	System.out.println("Saving data...");
    	if (model.getSelectedDevice()!=null) {
    		Preferences.writeProperty("selectedPort", model.getSelectedDevice());
    	}
    	if (model.getExplorerPath().getPath()!=null) {
    		Preferences.writeProperty("explorerPath", model.getExplorerPath().getPath());
    	}
    	if (model.getLineDelay()!=0) {
    		Preferences.writeProperty("lineDelay", String.valueOf(model.getLineDelay()));
    	}
    	if (model.getConsoleBufferSize()!=0) {
    		Preferences.writeProperty("consoleBufferSize", String.valueOf(model.getConsoleBufferSize()));
    	}

    	
    	Preferences.writeProperty("explorerHidden", String.valueOf(model.isExplorerVisible()));
    	Preferences.writeProperty("consoleHidden", String.valueOf(model.isConsoleVisible()));
    	

		if (model.getVisibleEditor()!=null && model.getVisibleEditor() instanceof Editor) {
	    	Preferences.writeProperty("lastOpened", ((Editor)model.getVisibleEditor()).getFilename());
		}
		if (model.getDimensionConsole()!=null) {
	    	Preferences.writeProperty("dimensionConsole", String.valueOf(model.getDimensionConsole().width));
		}
		if (model.getDimensionExplorer()!=null) {
	    	Preferences.writeProperty("dimensionExplorer", String.valueOf(model.getDimensionExplorer().width));
		}

		if ((model.getTelnetIP() != null) && (model.getTelnetIP().length()!=0) && (!model.getTelnetIP().equals("")) && (model.getTelnetIP().trim().equals(model.getTelnetIP()))) {
	    	Preferences.writeProperty("telnetIP", model.getTelnetIP());
		}
    	
    	Preferences.writeProperty("uploadMethod", String.valueOf(model.getUploadMethod()));

    	Preferences.writeProperty("uploadMode", String.valueOf(model.getUploadMode()));


    	
    	if (model.getBaudRate()!=0) {
	    	Preferences.writeProperty("baudrate", String.valueOf(model.getBaudRate()));
    	}
    	
    	System.out.println("Data saved...");
    }

    public void update(Observable o, Object obj){
    	if (Model.getObserver((Integer)obj)==Model.UPDATE_APPLICATION) {
	    	if (model.getExit()>-2) { //Damn perform an exit
	    		if (model.getExit()>=model.countObservers()-2) {//Makes sure that this is the last one to close
		    		if (!isExited) { //allow this class to only call model.setExit() once
		    			isExited = true;
		    			close();
		    			System.out.println("<g>"+o.getClass() + ": "+getClass()+" is closed!</g>");
		    			model.setExit();
		    		}	    			
	    		}
	    	}
    	}
	}

}
