package org.grebe.controls;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;

import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.grebe.components.Editor;
import org.grebe.lang.Lang;
import org.grebe.model.Model;
import org.grebe.utils.FileUtils;
import org.grebe.utils.Utils;


public class EditorController extends JPanel implements KeyListener, Observer, ActionListener {

	//Undo System needs a rework!, it is broken
	private static final long serialVersionUID = 1L;
	
	private Model model;
    private boolean isExited = false;
    private MouseAdapter ma;
	private JMenuItem 	menuUndo, menuRedo,
						menuCut, menuCopy, menuPaste, menuDelete,
						menuSelectAll;
	
	
	/*
	 * 
	 * Will later be useful
	 * InputMap im = tPane.getInputMap(JComponent.WHEN_FOCUSED);
		ActionMap am = tPane.getActionMap();

		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_Z, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()), "Undo");
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_Y, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()), "Redo");
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_S, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()), "Save");
		//Disabled functionality not working proper//im.put(KeyStroke.getKeyStroke(KeyEvent.VK_D, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()), "DelLine");

		am.put("Undo", new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
		    public void actionPerformed(ActionEvent e) {
		        try {
		            if (undoManager.canUndo()) {
		                undoManager.undo();
		            }
		        } catch (CannotUndoException exp) {
		            exp.printStackTrace();
		        }
		    }
		});
		am.put("Redo", new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
		    public void actionPerformed(ActionEvent e) {
		        try {
		            if (undoManager.canRedo()) {
		                undoManager.redo();
		            }
		        } catch (CannotUndoException exp) {
		            exp.printStackTrace();
		        }
		    }
		});
		am.put("Save", new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
		    public void actionPerformed(ActionEvent e) {
				saveCodeToFile();
		    }
		});
		
		am.put("DelLine", new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
		    public void actionPerformed(ActionEvent e) {
				String text = tPane.getText();
				int line = 0;
				int pos = 0;
				//System.out.println("DD"+tPane.getCaretPosition());
				while (line<text.length()) {
					pos = 0;
					while ((line+pos)<text.length() && text.charAt(line+pos)!='\n') {
						pos++;
					}
					//System.out.println(tPane.getCaretPosition()+"          POS");
					//System.out.println(line);
					//System.out.println(pos);
					/*if (tPane.getCaretPosition()>=(line) && tPane.getCaretPosition()<=(line+pos)) {
						//tPane.setText(text.substring(0, line) + text.substring(pos));
						try {
							tPane.getDocument().remove(line, pos);
						} catch (BadLocationException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						//saveCodeToModel();
						break;
					}*/
					/*

			        StyledDocument doc = tPane.getStyledDocument();

			        //Set the default text to the color black
			        AttributeSet aset = StyleContext.getDefaultStyleContext().addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, Color.GREEN);
					doc.setCharacterAttributes(line, 1, aset, false);
					
					//System.out.println(text.substring(line, line+pos-1));
					
					line +=pos+1;
				}
		    }
		});
	 *
	 *
	 *
	 *
	 *
	 */
	
	
	public EditorController(Model model) {
		this.model = model;
		setLayout(new BorderLayout());
		loadEditor();
		
		final EditorController ctrl = this;
		final Model mdl = model;
    	ma = new MouseAdapter() {
		private void myPopupEvent(MouseEvent e) {
    		int x = e.getX();
    		int y = e.getY();
    		RSyntaxTextArea tree = (RSyntaxTextArea)e.getSource();
    			
			JPopupMenu popup = new JPopupMenu();
			
  	  	        
  	        menuUndo = new JMenuItem(Lang.getText(Lang.UNDO));
  	        if (Utils.isMac()) {
  	        menuUndo.setAccelerator(KeyStroke.getKeyStroke(
  	        			KeyEvent.VK_Z, ActionEvent.META_MASK));
  	        } else {
  	        menuUndo.setAccelerator(KeyStroke.getKeyStroke(
  	        			KeyEvent.VK_Z, ActionEvent.CTRL_MASK));    		
  	        }
  	        menuUndo.addActionListener(ctrl);
  	        popup.add(menuUndo);
  	        menuRedo = new JMenuItem(Lang.getText(Lang.REDO));
  	        if (Utils.isMac()) {
  	        menuRedo.setAccelerator(KeyStroke.getKeyStroke(
  	        			KeyEvent.VK_Y, ActionEvent.META_MASK));
  	        } else {
  	        menuRedo.setAccelerator(KeyStroke.getKeyStroke(
  	        			KeyEvent.VK_Y, ActionEvent.CTRL_MASK));    		
  	        }
  	        menuRedo.addActionListener(ctrl);
  	        popup.add(menuRedo);
			popup.addSeparator();
  	        menuCut = new JMenuItem(Lang.getText(Lang.CUT));
  	        if (Utils.isMac()) {
  	        menuCut.setAccelerator(KeyStroke.getKeyStroke(
  	        			KeyEvent.VK_X, ActionEvent.META_MASK));
  	        } else {
  	        menuCut.setAccelerator(KeyStroke.getKeyStroke(
  	        			KeyEvent.VK_X, ActionEvent.CTRL_MASK));    		
  	        }
  	        menuCut.addActionListener(ctrl);
  	        popup.add(menuCut);
  	        menuCopy = new JMenuItem(Lang.getText(Lang.COPY));
  	        if (Utils.isMac()) {
  	        menuCopy.setAccelerator(KeyStroke.getKeyStroke(
  	        			KeyEvent.VK_C, ActionEvent.META_MASK));
  	        } else {
  	        menuCopy.setAccelerator(KeyStroke.getKeyStroke(
  	        			KeyEvent.VK_C, ActionEvent.CTRL_MASK));    		
  	        }
  	        menuCopy.addActionListener(ctrl);
  	        popup.add(menuCopy);
  	        menuPaste = new JMenuItem(Lang.getText(Lang.PASTE));
  	        if (Utils.isMac()) {
  	        menuPaste.setAccelerator(KeyStroke.getKeyStroke(
  	        			KeyEvent.VK_V, ActionEvent.META_MASK));
  	        } else {
  	        menuPaste.setAccelerator(KeyStroke.getKeyStroke(
  	        			KeyEvent.VK_V, ActionEvent.CTRL_MASK));    		
  	        }
  	        menuPaste.setEnabled((Utils.getClipboardContents()==null || Utils.getClipboardContents().isEmpty()) ? false : true);
  	        menuPaste.addActionListener(ctrl);
  	        popup.add(menuPaste);
  	        menuDelete = new JMenuItem(Lang.getText(Lang.DELETE));
  	        menuDelete.addActionListener(ctrl);
  	        popup.add(menuDelete);
  	        popup.addSeparator();
  	        menuSelectAll = new JMenuItem(Lang.getText(Lang.SELECT_ALL));
  	        if (Utils.isMac()) {
  	        menuSelectAll.setAccelerator(KeyStroke.getKeyStroke(
  	        			KeyEvent.VK_A, ActionEvent.META_MASK));
  	        } else {
  	        menuSelectAll.setAccelerator(KeyStroke.getKeyStroke(
  	        			KeyEvent.VK_A, ActionEvent.CTRL_MASK));    		
  	        }
  	        menuSelectAll.addActionListener(ctrl);
  	        

  	        //Now set the states of the menus
    		if (mdl.getVisibleEditor()!=null && mdl.getVisibleEditor() instanceof Editor) {
        	    menuUndo.setEnabled(((Editor)mdl.getVisibleEditor()).canUndo());
        	    menuRedo.setEnabled(((Editor)mdl.getVisibleEditor()).canRedo());
        	    menuCut.setEnabled(((Editor)mdl.getVisibleEditor()).getSelectedText()==null ? false : true);
        	    menuCopy.setEnabled(((Editor)mdl.getVisibleEditor()).getSelectedText()==null ? false : true);
        	    menuPaste.setEnabled((Utils.getClipboardContents()==null || Utils.getClipboardContents().isEmpty()) ? false : true);
        	    menuDelete.setEnabled(((Editor)mdl.getVisibleEditor()).getSelectedText()==null ? false : true);
        	    menuSelectAll.setEnabled(true);
    		} else {
        	    menuUndo.setEnabled(false);
        	    menuRedo.setEnabled(false);
        	    menuCut.setEnabled(false);
        	    menuCopy.setEnabled(false);
        	    menuPaste.setEnabled(false);
        	    menuDelete.setEnabled(false);
        	    menuSelectAll.setEnabled(false);        			
    		}
  	        
  	        popup.add(menuSelectAll);
			popup.show(tree, x, y);
		}
		public void mousePressed(MouseEvent e) {
			if (e.isPopupTrigger()) myPopupEvent(e);
		}
		public void mouseReleased(MouseEvent e) {
			if (e.isPopupTrigger()) myPopupEvent(e);
		}
    	};
	}
	
	@Override
	public void keyTyped(KeyEvent e) {}
	@Override
	public void keyPressed(KeyEvent e) {}
	@Override
	public void keyReleased(KeyEvent e) {
		if (model.getVisibleEditor() instanceof Editor)
			((Editor)model.getVisibleEditor()).setSaved(false);
	}

    public void close() {
    }

    private void loadEditor() {
		if (model.getVisibleEditor()!=null) {
	    	//the editorPanel should only have 1 component
	    	//Only change the visible editor if it is not already viewed
			if ((getComponentCount()==0) || getComponents()[0] != model.getVisibleEditor()) {
		        model.updateObserver(Model.UPDATE_MAIN, Model.UPDATE_ACTION_GENERAL);
				//setTitle(Lang.getText(Lang.TITLE)+" - "+model.getVisibleEditor().getFilename()); //Sets the frames title
		    	removeAll();
		    	//Only add the keylistener and the mouse listener if the editor is of type Editor
		    	if (model.getVisibleEditor() instanceof Editor) {
			    	if (!((Editor)model.getVisibleEditor()).hasKeyListener(this)) {
			    		model.getVisibleEditor().addKeyListener(this);
			    		//} catch (Exception ex) {System.out.println(ex.getMessage());}
			    	}
			    	if (!((Editor)model.getVisibleEditor()).hasMouseListener(ma)) {
			    		model.getVisibleEditor().addMouseListener(ma);
			    		//} catch (Exception ex) {System.out.println(ex.getMessage());}
			    	}
		    	}
		    	add(model.getVisibleEditor());
		    	//((Editor) getComponent(0)).doCodeRecognition();
		    	repaint();
		    	revalidate();
	    		//model.getVisibleEditor().doCodeRecognition();
			}
	    } else {
	        	JPanel pnlEditorPlaceHolder = new JPanel(new BorderLayout());
	        	pnlEditorPlaceHolder.setBackground(new Color(240, 240, 240));
	        	pnlEditorPlaceHolder.add(new JLabel("<html><center>"+Lang.getText(Lang.MSG_CHOOSE_FILE)+"</center></html>", JLabel.CENTER));
	        	removeAll();
	        	add(pnlEditorPlaceHolder);   
		    	repaint();
		    	revalidate();
		}
    }
    
    public void update(Observable o, Object obj){

    	if (Model.getObserver((Integer)obj)==Model.UPDATE_APPLICATION) {
	    	if (model.getExit()>-2) { //Damn perform an exit
	    		if (!isExited) { //allow this class to only call model.setExit() once
	    			isExited = true;
	    			close();
	    			System.out.println("<g>"+o.getClass() + ": "+getClass()+" is closed!</g>");
	    			model.setExit();
	    		}
	    	}
    	} else if (Model.getObserver((Integer)obj)==Model.UPDATE_EDITOR) {
    		System.out.println("UPDATE IN EDITOR!");
    		switch(Model.getAction((Integer)obj)) {
    		case Model.UPDATE_EDITOR_LOAD:
    			loadEditor();
    			break;
    		case Model.UPDATE_EDITOR_SAVE:
    			if (model.getVisibleEditor() instanceof Editor) {
	    			if (FileUtils.isFileInPath(model.getExplorerPath()+File.separator+((Editor)model.getVisibleEditor()).getFilename())) {
	    				FileUtils.saveFile(model.getExplorerPath()+File.separator+((Editor)model.getVisibleEditor()).getFilename(), ((Editor)model.getVisibleEditor()).getText());
	    				((Editor)model.getVisibleEditor()).setSaved(true);
	    				System.out.println("File Saved.");
	    			} else if (FileUtils.isFileInClassPath(((Editor)model.getVisibleEditor()).getFilename())) {
	    				System.out.println("You cannot save an example!");
	    			} else {
	    				System.out.println("Could not save file, unknown error");
	    			}
    			}
    			break;
    		}
    	}
	}

	@Override
	public void actionPerformed(ActionEvent ev) {
		if (ev.getSource() == menuUndo) {
			if (model.getVisibleEditor() instanceof Editor)
				((Editor)model.getVisibleEditor()).undo();
		} else if (ev.getSource() == menuRedo) {
			if (model.getVisibleEditor() instanceof Editor)
				((Editor)model.getVisibleEditor()).redo();
		} else if (ev.getSource() == menuCut) {
			if (model.getVisibleEditor() instanceof Editor)
				((Editor)model.getVisibleEditor()).cut();
		} else if (ev.getSource() == menuCopy) {
			if (model.getVisibleEditor() instanceof Editor)
				((Editor)model.getVisibleEditor()).copy();
		} else if (ev.getSource() == menuPaste) {
			if (model.getVisibleEditor() instanceof Editor)
				((Editor)model.getVisibleEditor()).paste();
		} else if (ev.getSource() == menuDelete) {
			if (model.getVisibleEditor() instanceof Editor)
				((Editor)model.getVisibleEditor()).delete();
		} else if (ev.getSource() == menuSelectAll) {
			if (model.getVisibleEditor() instanceof Editor)
				((Editor)model.getVisibleEditor()).sellectAll();
		}
	}
}
