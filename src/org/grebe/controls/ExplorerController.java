package org.grebe.controls;


import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.tree.*;

import org.grebe.components.Editor;
import org.grebe.components.ExtensionFileFilter;
import org.grebe.components.FileBrowser;
import org.grebe.lang.Lang;
import org.grebe.model.Model;
import org.grebe.utils.FileUtils;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.io.*;

public class ExplorerController extends JPanel implements Observer, ActionListener{
	private static final long serialVersionUID = 1L;
    
    private Model model;
    private FileBrowser fileBrowser;
    private boolean isExited = false;
    private File selectedFile;
    private boolean cancelSelection;
	private JMenuItem 	menuNew, menuNewDir, menuOpen, menuImport,
						menuCopy, menuPaste, menuRename, menuDelete;
    private JFileChooser fc;
    public ExplorerController(Model modell) {
    	this.model = modell;

        fc = new JFileChooser();

	    FileFilter filter1 = new ExtensionFileFilter("Lua files (*.lua)", new String[] { "LUA" });
		fc.setFileFilter(filter1);
		
    	fileBrowser = new FileBrowser();
  	  	fileBrowser.setRoot(model.getExplorerPath());
  	  	fileBrowser.refresh();
  	    // Add a listener
  	    fileBrowser.setTreeSelectionListener(new TreeSelectionListener() {
  	      public void valueChanged(TreeSelectionEvent e) {
	  	        DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.getPath().getLastPathComponent();
	  	        String path1 = (String) node.getUserObject();
	  	        String path = path1;
	  	        while (node.getParent()!=null) {     
	  	        	path = path1;
	  	        	node = (DefaultMutableTreeNode) node.getParent();
	  	        	path1 = (String) node.getUserObject() + File.separator + path;
	  	        }
	  	        //System.out.println("IN SELECTION CHANGED: "+path);
	  	        final File file = new File(path);
	  	        if (new File(model.getExplorerPath().getPath() + File.separator + file.getPath()).exists() && !new File(model.getExplorerPath().getPath() + File.separator + file.getPath()).isDirectory()) {
	  	        	selectedFile = new File(model.getExplorerPath().getPath() + File.separator + file.getPath());
	  	        	if (cancelSelection) return;
	  	        	ArrayList<JPanel> editors = model.getEditors();
	            	boolean isNew = true;
	            	for (JPanel editor : editors) {
	            		//System.out.println(editor.getFilename());
	            		//System.out.println(file.getPath());
	            		if (editor instanceof Editor) {
		            		if (((Editor)editor).getFilename().equals(file.getPath())) {
		            			model.setVisibleEditor((Editor)editor);
		            			isNew = false;
		            		}
	            		}
	            	}
	            	if (isNew) {
	            		//Load the editor in a new thread, in case we have a big file
	            		new Thread(new Runnable() {							
							@Override
							public void run() {
			            		Editor editor = new Editor(file.getPath());
								try {
				          	        String code = FileUtils.readFile(model.getExplorerPath().getPath() + File.separator + file.getPath());
			                    	editor.setText(code);
			                    	editor.discardAllEdits();
				          	        model.addEditor(editor);	
		                		} catch (Exception ex) {
		                    		model.addEditor(editor);
		                		}
			            		model.setVisibleEditor(editor);
							}
						}).start();
	            	}
	  	        }
  	        //System.out.println("You selected " + path);
  	      }
  	    });
  	  
  	    fileBrowser.setDragListener(new ActionListener() {
  			
  			@Override
  			public void actionPerformed(ActionEvent e) {
  		        try {
  		        	FileUtils.copyFileUsingChannel((File)e.getSource(), new File(model.getExplorerPath()+File.separator+((File)e.getSource()).getName()));
  				} catch (IOException e1) {
  					// TODO Auto-generated catch block
  					e1.printStackTrace();
  				}
  		        fileBrowser.refresh();
  			}
  		});
  	    
  	    final ExplorerController pnl = this;

  	     // add MouseListener to tree
  	    	MouseAdapter ma = new MouseAdapter() {
  	    		private void myPopupEvent(MouseEvent e) {
  	    			cancelSelection = true;
  	    			int x = e.getX();
  	    			int y = e.getY();
  	    			JTree tree = (JTree)e.getSource();
  	    			TreePath path = tree.getPathForLocation(x, y);
  	    			//if (path == null)
  	    			//	return;	

  	    			
  	    			JPopupMenu popup = new JPopupMenu();
  	    			
  	    			popup.addPopupMenuListener(new PopupMenuListener() {
						
						@Override
						public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
						}
						
						@Override
						public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
							cancelSelection = false;
						}
						
						@Override
						public void popupMenuCanceled(PopupMenuEvent e) {
						}
					});
  	    			
  	    			if (path!=null) {
  	  	    			tree.setSelectionPath(path);
  	  	    			DefaultMutableTreeNode node = (DefaultMutableTreeNode)path.getLastPathComponent();
  	  	    			//System.out.println(node);  	   

	  	  	  	        String path1 = (String) node.getUserObject();
	  	  	  	        String path2 = path1;
	  	  	  	        while (node.getParent()!=null) {     
	  	  	  	        	path2 = path1;
	  	  	  	        	node = (DefaultMutableTreeNode) node.getParent();
	  	  	  	        	path1 = (String) node.getUserObject() + File.separator + path2;
	  	  	  	        }
	  	  	  	        //System.out.println("PATH: "+path2);
	  	  	  	        JMenuItem menuItem = new JMenuItem(path2);
	  	  	  	        popup.add(menuItem);
	  	  	  	        menuNew = new JMenuItem(Lang.getText(Lang.NEW_FILE));
	  	  	  	        menuNew.addActionListener(pnl);
	  	  	  	        popup.add(menuNew);
	  	  	  	        menuNewDir = new JMenuItem(Lang.getText(Lang.NEW_FOLDER));
	  	  	  	        menuNewDir.addActionListener(pnl);
	  	  	  	        popup.add(menuNewDir);
	  	  	  	        menuOpen = new JMenuItem(Lang.getText(Lang.OPEN_FILE));
	  	  	  	        menuOpen.addActionListener(pnl);
	  	  	  	        popup.add(menuOpen);
	  	  	  	        menuImport = new JMenuItem(Lang.getText(Lang.IMPORT_FILE));
	  	  	  	        menuImport.addActionListener(pnl);
	  	  	  	        popup.add(menuImport);
	  	  	  	        popup.addSeparator();
	  	  	  	        menuCopy = new JMenuItem(Lang.getText(Lang.COPY));
	  	  	  	        menuCopy.addActionListener(pnl);
	  	  	  	        popup.add(menuCopy);
	  	  	  	        menuPaste = new JMenuItem(Lang.getText(Lang.PASTE));
	  	  	  	        menuPaste.addActionListener(pnl);
	  	  	  	        popup.add(menuPaste);
	  	  	  	        popup.addSeparator();
	  	  	  	        menuRename = new JMenuItem(Lang.getText(Lang.RENAME));
	  	  	  	        menuRename.addActionListener(pnl);
	  	  	  	        popup.add(menuRename);
	  	  	  	        menuDelete = new JMenuItem(Lang.getText(Lang.DELETE));
	  	  	  	        menuDelete.addActionListener(pnl);
	  	  	  	        popup.add(menuDelete);
  	    			}
  	  	  	        JMenuItem menuItem = new JMenuItem(Lang.getText(Lang.NEW_FILE));
  	  	  	        popup.add(menuItem);
  	  	  	        menuItem = new JMenuItem(Lang.getText(Lang.NEW_FOLDER));
  	  	  	        popup.add(menuItem);
	  	  	        JMenuItem menuNYI = new JMenuItem(Lang.getText(Lang.NYI));
	  	  	        menuNYI.setEnabled(false);
	    			popup.add(menuNYI);
  	    			popup.show(tree, x, y);
  	    		}
  	    		public void mousePressed(MouseEvent e) {
  	    			if (e.isPopupTrigger()) myPopupEvent(e);
  	    		}
  	    		public void mouseReleased(MouseEvent e) {
  	    			if (e.isPopupTrigger()) myPopupEvent(e);
  	    		}
  	    	};
  	    	fileBrowser.setMouseListener(ma);
  	    
  	  setLayout(new BorderLayout());
  	  add(fileBrowser);
    	
    }
    
    public void setDir() {
    	fileBrowser.refresh();
    }

    public void select(String file) {
    	//System.out.println("IN SET SELECTION: "+file);
    	fileBrowser.selectNode(new File(file));
		model.setUploadProgress(-1);//Cheating to update the toolbar buttons
    }    
    
    
    public void close() {
    	//Nothing to do
    }

    public void update(Observable o, Object obj){

    	if (Model.getObserver((Integer)obj)==Model.UPDATE_APPLICATION) {
	    	if (model.getExit()>-2) { //Damn perform an exit
	    		if (!isExited) { //allow this class to only call model.setExit() once
	    			isExited = true;
	    			close();
	    			System.out.println("<g>"+o.getClass() + ": "+getClass()+" is closed!</g>");
	    			model.setExit();
	    		}
	    	}
    	} else if (Model.getObserver((Integer)obj)==Model.UPDATE_EXPLORER) {
    		if (Model.getAction((Integer)obj)==Model.UPDATE_EXPLORER_FILES) {
    			fileBrowser.setRoot(model.getExplorerPath());
    			fileBrowser.refresh();
    		} else if (Model.getAction((Integer)obj)==Model.UPDATE_EXPLORER_SELECTION) {
    			//Only set the selection when the visible editor is of type Editor
    			if (model.getVisibleEditor() instanceof Editor && FileUtils.isFileInPath(((Editor)model.getVisibleEditor()).getFilename()) && (((Editor)model.getVisibleEditor()).getFilename().contains(model.getExplorerPath().getAbsolutePath()))) {
    				fileBrowser.selectNode(new File(((Editor)model.getVisibleEditor()).getFilename()));
    			} else {
    				fileBrowser.selectNode(null);
    			}
    		}
		}
    }

	@Override
	public void actionPerformed(ActionEvent e) {
		String text = ((JMenuItem) e.getSource()).getText();
		System.out.println("HAY"+text);
		if (text.equals(Lang.getText(Lang.DELETE))) {
			if (selectedFile==null) return; 
			if (selectedFile.isDirectory()) {
				FileUtils.deleteFolder(selectedFile);
			} else {
				if (model.getVisibleEditor()!=null && model.getVisibleEditor() instanceof Editor && selectedFile.getPath().equals(model.getExplorerPath().getPath() + File.separator + ((Editor)model.getVisibleEditor()).getFilename())) {
					model.setVisibleEditor(null);
				}
				if (!selectedFile.delete()) {
					System.err.println("Error while deleting file!");
				}				
			}
			fileBrowser.refresh();
			System.out.println("Deleting file: "+selectedFile.getAbsolutePath());
		} else {
			fc.setCurrentDirectory(model.getExplorerPath());

			
            int returnVal = fc.showSaveDialog(this);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
            	
                File file = fc.getSelectedFile();

                if(!(file.getName().toLowerCase()).endsWith(".lua") ) {
                    file = new File(file.getPath() + ".lua");
                }
                FileUtils.saveFile(file.getPath(), "");
                if (FileUtils.isFileInPath(file.getPath())) {
        			Editor editor = new Editor(FileUtils.getRelativePath(file, model.getExplorerPath()));
        			editor.setText(FileUtils.readFile(file.getPath()));
        			editor.discardAllEdits();
        			model.addEditor(editor);
            		model.setVisibleEditor(editor);
    				model.updateObserver(Model.UPDATE_EXPLORER, Model.UPDATE_EXPLORER_FILES);
    				model.updateObserver(Model.UPDATE_EXPLORER, Model.UPDATE_EXPLORER_SELECTION);
            		
        		}
            }
			
		}
	}
    
}

