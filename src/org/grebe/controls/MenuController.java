package org.grebe.controls;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.KeyStroke;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import javax.swing.filechooser.FileFilter;

import org.grebe.components.Editor;
import org.grebe.components.ExtensionFileFilter;
import org.grebe.examples.LuaExamples;
import org.grebe.lang.Lang;
import org.grebe.model.Model;
import org.grebe.utils.FileUtils;
import org.grebe.utils.Utils;
import org.grebe.views.AboutView;
import org.grebe.views.PreferencesView;


public class MenuController extends JMenuBar implements Observer, ActionListener, MenuListener {
	private static final long serialVersionUID = 1L;
	
	private Model model;
    private boolean isExited = false;
	private JFrame frame;
	private JMenu		menuRootFile, menuRootEdit, menuRootRun, menuRootDevice;
	private JMenuItem 	menuNew, menuOpen, menuImport, menuSave, menuSaveAs, menuExamples, menuAbout, menuPreferences, menuQuit,
						menuUndo, menuRedo, menuCut, menuCopy, menuPaste, menuDelete, menuSelectAll, 
						menuRun, menuUpload, 
						menuMode, menuPorts, menuRadioSerial, menuRadioTelnet, menuRestart, menuOpenCloseConnection;//, menuCloseConnection, menuOpenConnection;

    private JFileChooser fc;
    
    /*
     * Might later be useful 
		if ((model.getTelnetIP() != null) && (model.getTelnetIP().length()!=0) && (!model.getTelnetIP().equals("")) && (model.getTelnetIP().trim().equals(model.getTelnetIP()))) {
    		if (model.getConnectionState()==Model.CONNECTION_STATE_OPEN) {
    			menuCloseConnection.setEnabled(true);
    			menuOpenConnection.setEnabled(false);
    		} else if (model.getConnectionState()==Model.CONNECTION_STATE_CLOSED) {
    			menuCloseConnection.setEnabled(false);
    			menuOpenConnection.setEnabled(true);    			
    		}
			
		}
     * 
     * 
     * 
    	} else if ((obj==(Object)Model.UPDATE_UPLOAD_MODE) || (obj==(Object)Model.UPDATE_CONNECTION_STATE)) {
    		if (model.getUploadMethod()==Model.UPLOAD_MODE_SERIAL) {
    			menuPorts.setVisible(true);
    		} else if (model.getUploadMethod()==Model.UPLOAD_MODE_TELNET) {
    			menuPorts.setVisible(false);
	    		if (model.getConnectionState()==Model.CONNECTION_STATE_OPEN) {
	    			menuCloseConnection.setEnabled(true);
	    			menuOpenConnection.setEnabled(false);
	    		} else if (model.getConnectionState()==Model.CONNECTION_STATE_CLOSED) {
		    		if ((model.getTelnetIP() != null) && (model.getTelnetIP().length()!=0) && (!model.getTelnetIP().equals("")) && (model.getTelnetIP().trim().equals(model.getTelnetIP()))) {
    	    			menuCloseConnection.setEnabled(false);
    	    			menuOpenConnection.setEnabled(true);   
		    		}
	    		}
    		}
    		if (model.getUploadMethod()==Model.UPLOAD_MODE_SERIAL) {
    			if (model.getConnectionState()==Model.CONNECTION_STATE_OPEN) {
	    			menuCloseConnection.setEnabled(true);
	    			menuOpenConnection.setEnabled(false);  			
		    	} else {	
					ArrayList<String> cPorts = model.getSerialPorts();
					if (cPorts != null) {
						for (int i=0;i<cPorts.size();i++) {
							if (cPorts.get(i).equals(model.getSelectedDevice())) {
				    			menuCloseConnection.setEnabled(false);
				    			menuOpenConnection.setEnabled(true);  	
								
							}
						}
					}
	    		}
    		}
    		
     *
     *
     *
     *
    	} else if (obj==(Object)Model.UPDATE_PORTS) {    		
    		menuPorts.removeAll();
        	ArrayList<String> cPorts = model.getSerialPorts();
        	boolean portIsSelected = false;
        	if (cPorts.size()==0) {
        		menuPorts.setEnabled(false);
        	} else {
        		menuPorts.setEnabled(true);
	        	for (String serialItem:cPorts) {
				    	JRadioButtonMenuItem menuItem = new JRadioButtonMenuItem(serialItem);
				    	if (serialItem.equals(model.getSelectedDevice())) {
					    	menuItem.setSelected(true);				    	
					    	portIsSelected = true;
				    	} else {
					    	menuItem.setSelected(false);				    		
				    	}
				        menuItem.addActionListener(menuController);
				        menuPorts.add(menuItem);
	        	}   
        	}
        	menuRun.setEnabled(portIsSelected);
        	menuUpload.setEnabled(portIsSelected);
        	menuRestart.setEnabled(portIsSelected);
        	model.setUploadProgress(-1);
		}
     */
    
	public MenuController(JFrame frame, Model model) {
		this.model = model;
		this.frame = frame;
        fc = new JFileChooser();

	    FileFilter filter1 = new ExtensionFileFilter("Lua files (*.lua)", new String[] { "LUA" });
		fc.setFileFilter(filter1);
		
		//Where the GUI is created:
    	JMenuItem menuItem;

    	//Build the first menu.
    	menuRootFile = new JMenu(Lang.getText(Lang.FILE));
    	menuRootFile.addMenuListener(this);
    	
    	menuNew = new JMenuItem(Lang.getText(Lang.NEW));
    	if (Utils.isMac()) {
    		menuNew.setAccelerator(KeyStroke.getKeyStroke(
    				KeyEvent.VK_N, ActionEvent.META_MASK));
    	} else {
    		menuNew.setAccelerator(KeyStroke.getKeyStroke(
        	        KeyEvent.VK_N, ActionEvent.CTRL_MASK));    		
    	}
        menuNew.addActionListener(this);
    	menuRootFile.add(menuNew);
    	
    	menuOpen = new JMenuItem(Lang.getText(Lang.OPEN_FILE));
    	if (Utils.isMac()) {
    		menuOpen.setAccelerator(KeyStroke.getKeyStroke(
    				KeyEvent.VK_O, ActionEvent.META_MASK));
    	} else {    
    		menuOpen.setAccelerator(KeyStroke.getKeyStroke(
        	        KeyEvent.VK_O, ActionEvent.CTRL_MASK));    		
    	}
        menuOpen.addActionListener(this);
        menuRootFile.add(menuOpen);

    	menuImport = new JMenuItem(Lang.getText(Lang.IMPORT_FILE));
        menuImport.addActionListener(this);
        menuRootFile.add(menuImport);
    	
        menuRootFile.addSeparator();
    	
    	menuSave = new JMenuItem(Lang.getText(Lang.SAVE));
    	if (Utils.isMac()) {
    		menuSave.setAccelerator(KeyStroke.getKeyStroke(
    				KeyEvent.VK_S, ActionEvent.META_MASK));
    	} else {
        	menuSave.setAccelerator(KeyStroke.getKeyStroke(
        	        KeyEvent.VK_S, ActionEvent.CTRL_MASK));    		
    	}
    	menuSave.addActionListener(this);
    	menuRootFile.add(menuSave);
    	
    	menuSaveAs = new JMenuItem(Lang.getText(Lang.SAVE_AS));
    	menuSaveAs.addActionListener(this);
    	menuRootFile.add(menuSaveAs);

    	menuRootFile.addSeparator();
    	
    	menuExamples = new JMenu(Lang.getText(Lang.EXAMPLES));
    	menuExamples.addActionListener(this);
    	for (String example : LuaExamples.getAllDisplayNames()) {
    		menuItem = new JMenuItem(example);
    		menuItem.addActionListener(this);
    		menuExamples.add(menuItem);
    	}
    	menuRootFile.add(menuExamples);
    	
        if (!Utils.isMac()) { //If the application does not run on Mac we need to add a preferences menu
        	menuRootFile.addSeparator();
        	menuPreferences = new JMenuItem(Lang.getText(Lang.PREFERENCES));
        	menuPreferences.setAccelerator(KeyStroke.getKeyStroke(
        	        KeyEvent.VK_P, ActionEvent.CTRL_MASK));
        	menuPreferences.addActionListener(this);
        	menuRootFile.add(menuPreferences);
        	menuRootFile.addSeparator();

        	menuAbout = new JMenuItem(Lang.getText(Lang.ABOUT));
        	menuAbout.addActionListener(this);
        	menuRootFile.add(menuAbout);
	    	
	    	menuQuit = new JMenuItem(Lang.getText(Lang.QUIT));
	    	menuQuit.setAccelerator(KeyStroke.getKeyStroke(
	                KeyEvent.VK_Q, ActionEvent.CTRL_MASK));
	        menuQuit.addActionListener(this);
	        menuRootFile.add(menuQuit);
	    	
    	}
    	
    	add(menuRootFile);

    	//Begin Edit
    	//Start edit
    	menuRootEdit = new JMenu(Lang.getText(Lang.EDIT));
    	menuRootEdit.addMenuListener(this);
    	
    	menuUndo = new JMenuItem(Lang.getText(Lang.UNDO));
    	if (Utils.isMac()) {
    		menuUndo.setAccelerator(KeyStroke.getKeyStroke(
    	        KeyEvent.VK_Z, ActionEvent.META_MASK));
    	} else {
    		menuUndo.setAccelerator(KeyStroke.getKeyStroke(
        	        KeyEvent.VK_Z, ActionEvent.CTRL_MASK));    		
    	}
	    menuUndo.addActionListener(this);
	    menuRootEdit.add(menuUndo);
	    menuRedo = new JMenuItem(Lang.getText(Lang.REDO));
    	if (Utils.isMac()) {
    		menuRedo.setAccelerator(KeyStroke.getKeyStroke(
    	        KeyEvent.VK_Y, ActionEvent.META_MASK));
    	} else {
    		menuRedo.setAccelerator(KeyStroke.getKeyStroke(
        	        KeyEvent.VK_Y, ActionEvent.CTRL_MASK));    		
    	}
	    menuRedo.addActionListener(this);
	    menuRootEdit.add(menuRedo);
	    menuRootEdit.addSeparator();
	    menuCut = new JMenuItem(Lang.getText(Lang.CUT));
    	if (Utils.isMac()) {
    		menuCut.setAccelerator(KeyStroke.getKeyStroke(
    	        KeyEvent.VK_X, ActionEvent.META_MASK));
    	} else {
    		menuCut.setAccelerator(KeyStroke.getKeyStroke(
        	        KeyEvent.VK_X, ActionEvent.CTRL_MASK));    		
    	}
	    menuCut.addActionListener(this);
	    menuRootEdit.add(menuCut);
	    menuCopy = new JMenuItem(Lang.getText(Lang.COPY));
    	if (Utils.isMac()) {
    		menuCopy.setAccelerator(KeyStroke.getKeyStroke(
    	        KeyEvent.VK_C, ActionEvent.META_MASK));
    	} else {
    		menuCopy.setAccelerator(KeyStroke.getKeyStroke(
        	        KeyEvent.VK_C, ActionEvent.CTRL_MASK));    		
    	}
	    menuCopy.addActionListener(this);
	    menuRootEdit.add(menuCopy);
	    menuPaste = new JMenuItem(Lang.getText(Lang.PASTE));
    	if (Utils.isMac()) {
    		menuPaste.setAccelerator(KeyStroke.getKeyStroke(
    	        KeyEvent.VK_V, ActionEvent.META_MASK));
    	} else {
    		menuPaste.setAccelerator(KeyStroke.getKeyStroke(
        	        KeyEvent.VK_V, ActionEvent.CTRL_MASK));    		
    	}
	    menuPaste.addActionListener(this);
	    menuRootEdit.add(menuPaste);
	    menuDelete = new JMenuItem(Lang.getText(Lang.DELETE));
	    menuDelete.addActionListener(this);
	    menuRootEdit.add(menuDelete);
	    menuRootEdit.addSeparator();
	    menuSelectAll = new JMenuItem(Lang.getText(Lang.SELECT_ALL));
	    if (Utils.isMac()) {
    		menuSelectAll.setAccelerator(KeyStroke.getKeyStroke(
    	        KeyEvent.VK_A, ActionEvent.META_MASK));
    	} else {
    		menuSelectAll.setAccelerator(KeyStroke.getKeyStroke(
        	        KeyEvent.VK_A, ActionEvent.CTRL_MASK));    		
    	}
	    menuSelectAll.setEnabled(model.getVisibleEditor()==null ? false : true);
	    menuSelectAll.addActionListener(this);
	    menuRootEdit.add(menuSelectAll);
    	
    	//Now set some states

		if (model.getVisibleEditor()!=null && model.getVisibleEditor() instanceof Editor) {
    	    menuUndo.setEnabled(((Editor)model.getVisibleEditor()).canUndo());
    	    menuRedo.setEnabled(((Editor)model.getVisibleEditor()).canRedo());
    	    menuCut.setEnabled(((Editor)model.getVisibleEditor()).getSelectedText()==null ? false : true);
    	    menuCopy.setEnabled(((Editor)model.getVisibleEditor()).getSelectedText()==null ? false : true);
    	    menuPaste.setEnabled((Utils.getClipboardContents()==null || Utils.getClipboardContents().isEmpty()) ? false : true);
    	    menuDelete.setEnabled(((Editor)model.getVisibleEditor()).getSelectedText()==null ? false : true);
    	    menuSelectAll.setEnabled(true);
		} else {
    	    menuUndo.setEnabled(false);
    	    menuRedo.setEnabled(false);
    	    menuCut.setEnabled(false);
    	    menuCopy.setEnabled(false);
    	    menuPaste.setEnabled(false);
    	    menuDelete.setEnabled(false);
    	    menuSelectAll.setEnabled(false);        			
		}
	    
    	add(menuRootEdit);
    	//End Edit

    	//Begin Run menu
    	menuRootRun = new JMenu(Lang.getText(Lang.RUN));
    	menuRootRun.addMenuListener(this);

    	menuRun = new JMenuItem(Lang.getText(Lang.RUN));
    	menuRun.addActionListener(this);
    	menuRootRun.add(menuRun);
    	
    	menuUpload = new JMenuItem(Lang.getText(Lang.UPLOAD));
    	menuUpload.addActionListener(this);
    	menuRootRun.add(menuUpload);

    	add(menuRootRun);
    	//End Run menu

    	//Begin Device
    	menuRootDevice = new JMenu(Lang.getText(Lang.DEVICE));
    	menuRootDevice.addMenuListener(this);
    	
    	menuMode = new JMenu(Lang.getText(Lang.UPLOAD_MODE));

    	menuMode.add(menuRadioSerial = new JRadioButtonMenuItem(Lang.getText(Lang.METHOD_SERIAL)));
    	menuRadioSerial.addActionListener(this);
    	
    	menuMode.add(menuRadioTelnet = new JRadioButtonMenuItem(Lang.getText(Lang.METHOD_TELNET)));
    	menuRadioTelnet.addActionListener(this);
    	
    	if (model.getUploadMethod()==Model.UPLOAD_MODE_SERIAL) {
    		menuRadioSerial.setSelected(true);
    	} else if (model.getUploadMethod()==Model.UPLOAD_MODE_TELNET) {
    		menuRadioTelnet.setSelected(true);
    	}
    	menuRootDevice.add(menuMode);
    	
    	menuPorts = new JMenu(Lang.getText(Lang.CONNECT));
    	menuPorts.setEnabled(false);

    	menuRootDevice.add(menuPorts);

    	menuRestart = new JMenuItem(Lang.getText(Lang.RESTART_DEVICE));
    	menuRestart.addActionListener(this);
    	menuRootDevice.add(menuRestart);

    	menuOpenCloseConnection = new JMenuItem(Lang.getText(Lang.OPEN_CONNECTION));
    	menuOpenCloseConnection.setEnabled(false);
    	menuOpenCloseConnection.addActionListener(this);
    	menuRootDevice.add(menuOpenCloseConnection);

    	add(menuRootDevice);
    	//End Device

    	menuRun.setEnabled(false);
    	menuUpload.setEnabled(false);
    	menuRestart.setEnabled(false);
		
	}


	public void updateMenu() {
		switch (model.getUploadMethod()) {
		case Model.UPLOAD_MODE_SERIAL:
			menuPorts.setVisible(true);
			break;
		case Model.UPLOAD_MODE_TELNET:
			menuPorts.setVisible(false);
			break;
		}
		switch(model.getConnectionState()) {
		case Model.CONNECTION_STATE_FAIL:
		case Model.CONNECTION_STATE_CLOSED:
			//Connection is closed, make sure we have all buttons disabled, only enabled them
			//if we have a port selected (Serial mode) or if we have a valid telnet ip(Telnet mode)
			menuOpenCloseConnection.setText(Lang.getText(Lang.OPEN_CONNECTION));
			menuOpenCloseConnection.setEnabled(false);
        	menuRun.setEnabled(false);
        	menuUpload.setEnabled(false);
        	menuRestart.setEnabled(false);
			switch (model.getUploadMethod()) {
			case Model.UPLOAD_MODE_SERIAL:
				//Connection is closed only enable the buttons them if we have a port selected
	        	//Check if a port is selected
				ArrayList<String> cPorts = model.getSerialPorts();
				if (cPorts != null) {
					for (int i=0;i<cPorts.size();i++) {
						if (cPorts.get(i).equals(model.getSelectedDevice())) {
							menuOpenCloseConnection.setEnabled(true);
				        	menuRun.setEnabled(true);
				        	menuUpload.setEnabled(true);
				        	menuRestart.setEnabled(true);
						}
					}
				}
				break;
			case Model.UPLOAD_MODE_TELNET:
				//Connection is closed, check if the telnet ip is valid, if so, enable buttons
	    		if ((model.getTelnetIP() != null) && (model.getTelnetIP().length()!=0) && (!model.getTelnetIP().equals("")) && (model.getTelnetIP().trim().equals(model.getTelnetIP()))) {
	    			menuOpenCloseConnection.setEnabled(true); 
	            	menuRun.setEnabled(true);
	            	menuUpload.setEnabled(true);
	            	menuRestart.setEnabled(true);
	    		}
				break;
			}
			break;
		case Model.CONNECTION_STATE_OPEN:
			//We have an open connection, allow to close
			menuOpenCloseConnection.setText(Lang.getText(Lang.CLOSE_CONNECTION));
			menuOpenCloseConnection.setEnabled(true);
        	menuRun.setEnabled(true);
        	menuUpload.setEnabled(true);
        	menuRestart.setEnabled(true);
			break;
		case Model.CONNECTION_STATE_CONNECTING:
			//While connecting we are not allowed to send any data to the device
			menuOpenCloseConnection.setText(Lang.getText(Lang.OPEN_CONNECTION));
			menuOpenCloseConnection.setEnabled(false);
        	menuRun.setEnabled(false);
        	menuUpload.setEnabled(false);
        	menuRestart.setEnabled(false);
			break;	    	
		}
	}
	

    public void close() {
    }

    public void update(Observable o, Object obj){
    	if (Model.getObserver((Integer)obj)==Model.UPDATE_APPLICATION) {
	    	if (model.getExit()>-2) { //Damn perform an exit
	    		if (!isExited) { //allow this class to only call model.setExit() once
	    			isExited = true;
	    			close();
	    			System.out.println("<g>"+o.getClass() + ": "+getClass()+" is closed!</g>");
	    			model.setExit();
	    		}
	    	}
    	} else if (Model.getObserver((Integer)obj)==Model.UPDATE_MENU) {
    		switch(Model.getAction((Integer)obj)) {
    		case Model.UPDATE_MENU_PORTS:
    			if (model.getUploadMethod()==Model.UPLOAD_MODE_SERIAL) {
	    			menuPorts.removeAll();
	            	ArrayList<String> cPorts = model.getSerialPorts();
	            	if (cPorts.size()==0) {
	            		menuPorts.setEnabled(false);
	            	} else {
	            		menuPorts.setEnabled(true);
	    	        	for (String serialItem:cPorts) {
	    				    	JRadioButtonMenuItem menuItem = new JRadioButtonMenuItem(serialItem);
	    				    	if (serialItem.equals(model.getSelectedDevice())) {
	    					    	menuItem.setSelected(true);
	    				    	} else {
	    					    	menuItem.setSelected(false);				    		
	    				    	}
	    				        menuItem.addActionListener(this);
	    				        menuPorts.add(menuItem);
	    	        	}   
	            	}
	        		updateMenu();
	            	model.updateObserver(Model.UPDATE_TOOLBAR, Model.UPDATE_ACTION_GENERAL);
    			}
            	break;    		
    		}
    	} else if ((Model.getObserver((Integer)obj)==Model.UPDATE_UPLOAD_MODE) || (Model.getObserver((Integer)obj)==Model.UPDATE_CONNECTION_STATE)) {
			updateMenu();
    	}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JMenuItem) {
			//A menu item was pressed
			final JMenuItem item = ((JMenuItem)e.getSource());
			JPopupMenu menu = (JPopupMenu) item.getParent();
			JMenu actMenu = (JMenu) menu.getInvoker();
			//System.out.println(item.getText());
			if (actMenu == menuPorts) {
				ArrayList<String> cPorts = model.getSerialPorts();
				if (cPorts != null) {
					for (int i=0;i<cPorts.size();i++) {
						if (cPorts.get(i).equals(item.getText())) {
							model.setSelectedDevice(cPorts.get(i));
							model.changeSerialPort(i, cPorts.get(i));
						} else if (cPorts.get(i).equals(model.getSelectedDevice())) {
							model.changeSerialPort(i, cPorts.get(i));							
						}
					}
				}
				System.out.println("Serial: "+actMenu.getText());				
			} else if (item.getText().equals(Lang.getText(Lang.MONITOR))) {	
			} else if (item == menuRestart) {
				model.setToUploader("node.restart()\n");
				model.updateObserver(Model.UPDATE_UPLOADER, Model.UPDATE_UPLOADER_UPLOAD);
			} else if (item == menuOpenCloseConnection) {
				if (model.getConnectionState()==Model.CONNECTION_STATE_CLOSED || model.getConnectionState()==Model.CONNECTION_STATE_FAIL) {
					new Thread(new Runnable() {					
						@Override
						public void run() {
							model.updateObserver(Model.UPDATE_UPLOADER, Model.UPDATE_UPLOADER_OPEN);
						}
					}).start();						
				} else if (model.getConnectionState()==Model.CONNECTION_STATE_OPEN) {
					new Thread(new Runnable() {					
						@Override
						public void run() {
							model.updateObserver(Model.UPDATE_UPLOADER, Model.UPDATE_UPLOADER_CLOSE);
						}
					}).start();						
				}
			} else if (item == menuUpload) {	
			} else if (item == menuRun) {		
			} else if (item == menuAbout) {
				AboutView about = new AboutView(frame);	
				about.showDialog();		
			} else if (item == menuPreferences) {
				PreferencesView prefs = new PreferencesView(model, frame);	
				prefs.showDialog();
			} else if (item == menuQuit) {
 				model.requestExit();
			} else if (item == menuSave) {
				System.out.println("Saving file");
				model.updateObserver(Model.UPDATE_EDITOR, Model.UPDATE_EDITOR_SAVE);
			} else if (item ==menuSaveAs) {
				fc.setCurrentDirectory(model.getExplorerPath());				
	            int returnVal = fc.showDialog(frame, Lang.getText(Lang.SAVE));
	            if (returnVal == JFileChooser.APPROVE_OPTION) {	            	
	                File file = fc.getSelectedFile();
	                if(!(file.getName().toLowerCase()).endsWith(".lua") ) {
	                    file = new File(file.getPath() + ".lua");
	                }
	    			if (model.getVisibleEditor() instanceof Editor) {
		    			if (FileUtils.isFileInPath(file.getAbsolutePath())) {
		    				//file already exist
		                	int result = JOptionPane.showConfirmDialog(frame, Lang.getText(Lang.MSG_OVERWRITE_FILE), Lang.getText(Lang.TITLE_OVERWRITE_FILE), JOptionPane.YES_NO_OPTION);
		                	if (result!=JOptionPane.YES_OPTION) return;
		    			}
	    				FileUtils.saveFile(file.getAbsolutePath(), ((Editor)model.getVisibleEditor()).getText());
	    				((Editor)model.getVisibleEditor()).setSaved(true);
	    				System.out.println("File Saved.");
	    			}
	            }
			} else if (item == menuNew) {
				System.out.println("New file");
				fc.setCurrentDirectory(model.getExplorerPath());
	            int returnVal = fc.showDialog(frame, Lang.getText(Lang.CREATE));
	            if (returnVal == JFileChooser.APPROVE_OPTION) {	            	
	                File file = fc.getSelectedFile();
	                if(!(file.getName().toLowerCase()).endsWith(".lua") ) {
	                    file = new File(file.getPath() + ".lua");
	                }
	                FileUtils.saveFile(file.getPath(), "");
	                if (FileUtils.isFileInPath(file.getPath())) {
	        			Editor editor = new Editor(FileUtils.getRelativePath(file, model.getExplorerPath()));
	        			editor.setText(FileUtils.readFile(file.getPath()));
	        			editor.discardAllEdits();
	        			model.addEditor(editor);
	            		model.setVisibleEditor(editor);
	    				model.updateObserver(Model.UPDATE_EXPLORER, Model.UPDATE_EXPLORER_FILES);
	    				model.updateObserver(Model.UPDATE_EXPLORER, Model.UPDATE_EXPLORER_SELECTION);	            		
	        		}
	            }
			} else if (item == menuOpen) {
	            int returnVal = fc.showOpenDialog(frame);

	            if (returnVal == JFileChooser.APPROVE_OPTION) {
	                File file = fc.getSelectedFile();
	                

	            	boolean isNew = true;
	            	for (JPanel editor : model.getEditors()) {
	            		if (editor instanceof Editor) {
		            		if (((Editor)editor).getFilename().equals(file.getPath())) {
		            			model.setVisibleEditor((Editor)editor);
		            			isNew = false;
		            		}
	            		}
	            	}
	            	if (isNew) {
	            		Editor editor = new Editor(file.getPath());
	            		editor.setText(FileUtils.readFile(file.getPath()));
	        			editor.discardAllEdits();
	            		model.addEditor(editor);
	    	            model.setVisibleEditor(editor);
	    				model.updateObserver(Model.UPDATE_EXPLORER, Model.UPDATE_EXPLORER_SELECTION);
	            	}
	            }
			} else if (item == menuImport) {
	            int returnVal = fc.showOpenDialog(frame); //Show the import dialog
	            if (returnVal == JFileChooser.APPROVE_OPTION) {
	            	//If user selected a file, then copy it
	                File file = fc.getSelectedFile();
	                if (new File(model.getExplorerPath()+File.separator+file.getName()).exists()) {
	                	int result = JOptionPane.showConfirmDialog(frame, Lang.getText(Lang.MSG_OVERWRITE_FILE), Lang.getText(Lang.TITLE_OVERWRITE_FILE), JOptionPane.YES_NO_OPTION);
	                	if (result!=JOptionPane.YES_OPTION) return;
	                }
  					try {
  						FileUtils.copyFileUsingChannel(file, new File(model.getExplorerPath()+File.separator+file.getName()));
	    				model.updateObserver(Model.UPDATE_EXPLORER, Model.UPDATE_EXPLORER_FILES);
	    				model.updateObserver(Model.UPDATE_EXPLORER, Model.UPDATE_EXPLORER_SELECTION);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
	            }
			} else if (item == menuUndo) {
				if (model.getVisibleEditor() instanceof Editor)
					((Editor)model.getVisibleEditor()).undo();
			} else if (item == menuRedo) {
				if (model.getVisibleEditor() instanceof Editor)
					((Editor)model.getVisibleEditor()).redo();
			} else if (item == menuCut) {
				if (model.getVisibleEditor() instanceof Editor)
					((Editor)model.getVisibleEditor()).cut();
			} else if (item == menuCopy) {
				if (model.getVisibleEditor() instanceof Editor)
					((Editor)model.getVisibleEditor()).copy();
			} else if (item == menuPaste) {
				if (model.getVisibleEditor() instanceof Editor)
					((Editor)model.getVisibleEditor()).paste();
			} else if (item == menuDelete) {
				if (model.getVisibleEditor() instanceof Editor)
					((Editor)model.getVisibleEditor()).delete();
			} else if (item == menuSelectAll) {
				if (model.getVisibleEditor() instanceof Editor)
					((Editor)model.getVisibleEditor()).sellectAll();
			} else if (actMenu == menuExamples) {
				//Search if we still have the file opened in the background.
            	boolean isNew = true;
            	for (JPanel editor : model.getEditors()) {
            		if (editor instanceof Editor) {
	            		if (((Editor)editor).getFilename().equals(LuaExamples.PATH+LuaExamples.getFileName(item.getText()))) {
	            			model.setVisibleEditor((Editor)editor);
	            			isNew = false;
	            		}
            		}
            	}
            	//If the file has not been opened before, open it
            	if (isNew) {
            		Editor editor = new Editor(LuaExamples.PATH+LuaExamples.getFileName(item.getText()));
            		editor.setText(FileUtils.readFile(getClass().getResourceAsStream(LuaExamples.PATH+LuaExamples.getFileName(item.getText()))));
        			editor.discardAllEdits();
            		model.addEditor(editor);
    	            model.setVisibleEditor(editor);
    				model.updateObserver(Model.UPDATE_EXPLORER, Model.UPDATE_EXPLORER_SELECTION);
					//model.updateExplorer(Model.UPDATE_EXPLORER_SELECTION); //To disable the selection
            	}

			} else if (actMenu == menuMode) {
				for (int i=0;i<actMenu.getMenuComponentCount();i++) {
					((JRadioButtonMenuItem)actMenu.getMenuComponent(i)).setSelected(false);
				}
				item.setSelected(true);
				//Check if the mode change is a good change, and close the connection if we have one open
				if (model.getUploadMethod()==Model.UPLOAD_MODE_TELNET) {
					if ((model.getConnectionState()==Model.CONNECTION_STATE_OPEN) || (model.getConnectionState()==Model.CONNECTION_STATE_CONNECTING)) model.updateObserver(Model.UPDATE_UPLOADER, Model.UPDATE_UPLOADER_CLOSE);
					model.setUploadMethod(Model.UPLOAD_MODE_SERIAL);
				} else if (model.getUploadMethod()==Model.UPLOAD_MODE_SERIAL) {
					if ((model.getConnectionState()==Model.CONNECTION_STATE_OPEN) || (model.getConnectionState()==Model.CONNECTION_STATE_CONNECTING)) model.updateObserver(Model.UPDATE_UPLOADER, Model.UPDATE_UPLOADER_CLOSE);
					model.setUploadMethod(Model.UPLOAD_MODE_TELNET);
				}
				//System.out.println("Serial: "+actMenu.getText());
			}
		}
		
	}


    @Override
    public void menuSelected(MenuEvent e) {
        if (e.getSource() instanceof JMenu) {
        	JMenu item = (JMenu) e.getSource();
        	//System.out.println(item.getText());
        	if (item == menuRootFile) {
        		
        	} else if (item == menuRootEdit) {
        		if (model.getVisibleEditor()!=null && model.getVisibleEditor() instanceof Editor) {
	        	    menuUndo.setEnabled(((Editor)model.getVisibleEditor()).canUndo());
	        	    menuRedo.setEnabled(((Editor)model.getVisibleEditor()).canRedo());
	        	    menuCut.setEnabled(((Editor)model.getVisibleEditor()).getSelectedText()==null ? false : true);
	        	    menuCopy.setEnabled(((Editor)model.getVisibleEditor()).getSelectedText()==null ? false : true);
	        	    menuPaste.setEnabled((Utils.getClipboardContents()==null || Utils.getClipboardContents().isEmpty()) ? false : true);
	        	    menuDelete.setEnabled(((Editor)model.getVisibleEditor()).getSelectedText()==null ? false : true);
	        	    menuSelectAll.setEnabled(true);
        		} else {
            	    menuUndo.setEnabled(false);
            	    menuRedo.setEnabled(false);
            	    menuCut.setEnabled(false);
            	    menuCopy.setEnabled(false);
            	    menuPaste.setEnabled(false);
            	    menuDelete.setEnabled(false);
            	    menuSelectAll.setEnabled(false);        			
        		}
        	} 
        }
    }
    @Override
    public void menuDeselected(MenuEvent e) {}
    @Override
    public void menuCanceled(MenuEvent e) {}
}

