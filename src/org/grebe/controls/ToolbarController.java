package org.grebe.controls;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.imageio.ImageIO;
import javax.swing.*;

import org.grebe.components.Editor;
import org.grebe.components.PlaceholderTextField;
import org.grebe.lang.Lang;
import org.grebe.model.Model;
import org.grebe.utils.Utils;

public class ToolbarController extends JToolBar implements Observer, ActionListener, KeyListener {
	private static final long serialVersionUID = 1L;
	
	private Model model;
    private JProgressBar pbUpload;
    private PlaceholderTextField tfTelnet;
	private JToggleButton btnRun, btnStop, btnExplorer, btnConsole;

    private boolean isExited = false;
	
	public ToolbarController(Model model) {
		this.model = model;
		
		setLayout(new BorderLayout());

		JPanel pnlLeft = new JPanel();
		pnlLeft.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(2, 2, 2, 2);
		
		pnlLeft.setOpaque(false); //Sets the background to transparent

		add(pnlLeft, BorderLayout.WEST);

		BufferedImage img =null;
		try {
			img = ImageIO.read(getClass().getResourceAsStream(Utils.PATH_RESOURCES + "run.png"));
			btnRun = new JToggleButton(new ImageIcon(img));
			btnRun.setFocusable(false);
			btnRun.setOpaque(false);
			btnRun.addActionListener(this);
			btnRun.setEnabled(false);
			pnlLeft.add(btnRun, gbc);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		try {
			img = ImageIO.read(getClass().getResourceAsStream(Utils.PATH_RESOURCES + "stop.png"));
			btnStop = new JToggleButton(new ImageIcon(img));
			btnStop.setFocusable(false);
			btnStop.setOpaque(false);
			btnStop.setEnabled(false);
			btnStop.addActionListener(this);
			gbc.gridx++;
			pnlLeft.add(btnStop, gbc);
		} catch (IOException e1) {
			e1.printStackTrace();
		}


		JPanel pnlCenter = new JPanel();
		pnlCenter.setOpaque(false);
		add(pnlCenter);
		
		pbUpload = new JProgressBar(0, 100);
		pbUpload.setPreferredSize(new Dimension(100, (int) pbUpload.getPreferredSize().getHeight()));
		pbUpload.setValue(0);
		pbUpload.setOpaque(false);
		pbUpload.setFocusable(false);
		JPanel pnl = new JPanel(new FlowLayout(FlowLayout.LEFT));
		pnl.setOpaque(false);
		pnl.add(pbUpload);
		tfTelnet = new PlaceholderTextField(16);
		tfTelnet.setPreferredSize(new Dimension((int)tfTelnet.getPreferredSize().getWidth(), (int)pbUpload.getPreferredSize().getHeight()));
		tfTelnet.setPlaceholder(Lang.getText(Lang.ENTER_IP));
		tfTelnet.setHorizontalAlignment(JTextField.CENTER);
		tfTelnet.setVisible(model.getUploadMode()==Model.UPLOAD_MODE_TELNET ? true : false);
		if  (Utils.isMac()) {tfTelnet.setOpaque(false);}
		if (model.getTelnetIP()!=null) {
			tfTelnet.setText(model.getTelnetIP());
		}
		tfTelnet.addKeyListener(this);
		pnl.add(tfTelnet);
		pnlCenter.add(pnl);
		JPanel pnlSpacer = new JPanel();
		pnlSpacer.setOpaque(false);
		pnlSpacer.setPreferredSize(new Dimension(0, pnl.getPreferredSize().height));
		pnlCenter.add(pnlSpacer);

		pbUpload.setVisible(false);
		
		JPanel pnlRight = new JPanel();

		pnlRight.setLayout(new GridBagLayout());
		gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(2, 2, 2, 2);
		
		pnlRight.setOpaque(false);//Make background transparent
		add(pnlRight, BorderLayout.EAST);

		if (!model.isExplorerVisible()) {
			try {img = ImageIO.read(getClass().getResourceAsStream(Utils.PATH_RESOURCES + "explorerhide.png"));
			} catch (IOException e1) {e1.printStackTrace();}
		} else {
			try {img = ImageIO.read(getClass().getResourceAsStream(Utils.PATH_RESOURCES + "explorershow.png"));
			} catch (IOException e1) {e1.printStackTrace();}					
		}
		btnExplorer = new JToggleButton(new ImageIcon(img));
		btnExplorer.setSelected(model.isExplorerVisible());
		btnExplorer.setFocusable(false);
		btnExplorer.setOpaque(false);
		btnExplorer.addActionListener(this);
		
		pnlRight.add(btnExplorer, gbc);		

		if (!model.isConsoleVisible()) {
			try {img = ImageIO.read(getClass().getResourceAsStream(Utils.PATH_RESOURCES + "consolehide.png"));
			} catch (IOException e1) {e1.printStackTrace();}
		} else {
			try {img = ImageIO.read(getClass().getResourceAsStream(Utils.PATH_RESOURCES + "consoleshow.png"));
			} catch (IOException e1) {e1.printStackTrace();}					
		}
		btnConsole = new JToggleButton(new ImageIcon(img));
		btnConsole.setSelected(model.isConsoleVisible());
		btnConsole.setFocusable(false);
		btnConsole.setOpaque(false);
		btnConsole.addActionListener(this);
		gbc.gridx++;
		pnlRight.add(btnConsole, gbc);
		//addSeparator();
		updateToolbar(); //Called once at startup, to ensure that the proper state is set to the components
	}

	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JToggleButton) {
			JToggleButton btn = (JToggleButton) e.getSource();
			if (btn == btnRun) {
				if (model.getUploadState()!=Model.UPLOAD_STATE_UPLOADING) { //We can only upload when there is nothing already uploading
					System.out.println("NOT UPLOADING");
					if (model.getVisibleEditor()!=null) {
						if (model.getVisibleEditor() instanceof Editor) {
							ArrayList<JPanel> editors = model.getEditors();
							for (int i=0;i<editors.size();i++) {
								if (editors.get(i)==model.getVisibleEditor()) {
									System.out.println("TOOLBAR: uploading");
									model.setToUploader(((Editor)model.getVisibleEditor()).getText());
									updateToolbar();
									model.updateObserver(Model.UPDATE_UPLOADER, Model.UPDATE_UPLOADER_UPLOAD);
									break;
								}
							}
						}
					}
				} else {
					System.out.println("UPLOADING");
					//btn.setSelected(true);					
				}
			} else if (btn == btnStop) {
				if (model.getUploadState()==Model.UPLOAD_STATE_UPLOADING) { //We can only cancel uploading when we are uploading
					model.updateObserver(Model.UPDATE_UPLOADER, Model.UPDATE_UPLOADER_CANCEL);
					updateToolbar(); //Update the toolbar to to disable the stop button
					//model.requestUploader(Model.UPLOADER_REQUEST_CANCEL);
				} else {
					//btn.setSelected(false);						
				}
			} else if (btn == btnExplorer) {
				model.setVisibleExplorer(!model.isExplorerVisible());
				if (!model.isExplorerVisible()) {
					try {
						btn.setIcon(new ImageIcon(ImageIO.read(getClass().getResourceAsStream(Utils.PATH_RESOURCES+"explorerhide.png"))));
						btn.setSelected(false);
					} catch (IOException e1) {e1.printStackTrace();}
				} else {
					try {
						btn.setIcon(new ImageIcon(ImageIO.read(getClass().getResourceAsStream(Utils.PATH_RESOURCES+"explorershow.png"))));
						btn.setSelected(true);
					} catch (IOException e1) {e1.printStackTrace();}					
				}
			} else if (btn == btnConsole) {
				model.setVisibleConsole(!model.isConsoleVisible());
				if (!model.isConsoleVisible()) {
					try {
						btn.setIcon(new ImageIcon(ImageIO.read(getClass().getResourceAsStream(Utils.PATH_RESOURCES+"consolehide.png"))));
						btn.setSelected(false);
					} catch (IOException e1) {e1.printStackTrace();}
				} else {
					try {
						btn.setIcon(new ImageIcon(ImageIO.read(getClass().getResourceAsStream(Utils.PATH_RESOURCES+"consoleshow.png"))));
						btn.setSelected(true);
					} catch (IOException e1) {e1.printStackTrace();}					
				}
			}
		}
	}
	
	private void close() {
		
	}

	@Override
	protected void paintComponent(Graphics g){
	    // Create the 2D copy
	    Graphics2D g2 = (Graphics2D)g.create();

	    // Apply vertical gradient
	    g2.setPaint(new GradientPaint(0, 0, getBackground(), 0, getHeight(), getBackground().darker()));
	    g2.fillRect(0, 0, getWidth(), getHeight());

	    // Dipose of copy
	    g2.dispose();
	}
	/*
    private void setVisibilityProgress(boolean show) {
    	if (pbUpload.isVisible()!=show) {
    		pbUpload.setVisible(show);
			revalidate();
			repaint();
    	} else {
	    	if (pbUpload.isVisible()==false) {
	    		if (model.getUploadMode()==Model.UPLOAD_MODE_TELNET) {
	    			tfTelnet.setVisible(true);
	    		} else {
	    			tfTelnet.setVisible(false);	    			
	    		}
    			revalidate();
    			repaint();
	    	}
    	}
    }*/
    
    /**
     * This method is responsible for displaying the
     * proper states of the components in the toolbar
     */
    public void updateToolbar() {
    	System.out.println("Updating toolbar...");
		switch(model.getConnectionState()) {
		//CONNECTION FAIL STATE is used in main, to show an error message
		//CONNECTION STATE FAIL will behave the same as CONNECTION STATE CLOSED
		case Model.CONNECTION_STATE_FAIL:
		case Model.CONNECTION_STATE_CLOSED:
			pbUpload.setVisible(false);
			if (model.getUploadMethod()==Model.UPLOAD_MODE_TELNET) {
				tfTelnet.setVisible(true);
				tfTelnet.setEnabled(true);
				revalidate(); //We need to revalidate, otherwise we have layout issues
			} else {
				tfTelnet.setVisible(false);				
			}
			//We have a closed connection
			if (model.getVisibleEditor()!=null) {
    			//We have a closed connection, and a visible editor
				//lastly check if the telnet ip is valid
				switch(model.getUploadMethod()) {
				case Model.UPLOAD_MODE_SERIAL:
					//Check if we have a port selected
	    			boolean portIsSelected = false;
	        		ArrayList<String> cPorts = model.getSerialPorts();
	            	if (cPorts.size()>0) {
	    	        	for (String serialItem:cPorts) {
	    				    if (serialItem.equals(model.getSelectedDevice())) {
	    				    	portIsSelected = true;
	    					   	break;
	    				    } 
	    	        	}   
	            	}
	        		if (portIsSelected) {
	        			//only allow uploading when we have a port selected,
	        			//and have a visible port
	            		btnRun.setEnabled(true);
	        		} else {
	        			btnRun.setEnabled(false);        			
	        		}
					break;
				case Model.UPLOAD_MODE_TELNET:
    				if ((model.getTelnetIP() != null) && (model.getTelnetIP().length()!=0) && (!model.getTelnetIP().equals("")) && (model.getTelnetIP().trim().equals(model.getTelnetIP()))) {
        				//So we can safely allow code uploading
            			btnRun.setEnabled(true);  					
    				} else {
            			//We have a closed connection, and a visible editor
    					//Unfortunaly no valid telnet ip
            			btnRun.setEnabled(false);
    				}
					break;
				}   
			} else {
				//We have an open connection, but no editor is visible
    			btnRun.setEnabled(false);      				
			} 
			//We have a closed connection, so we can never stop uploading, and we are never uploading
			btnRun.setSelected(false);
			btnStop.setSelected(false);
			btnStop.setEnabled(false);
			break;
		case Model.CONNECTION_STATE_OPEN:
			tfTelnet.setVisible(false);
    		switch(model.getUploadState()) {
    		case Model.UPLOAD_STATE_CANCELING:
    			System.out.println("Toolbar Cancelling");
    			pbUpload.setVisible(true);
    			pbUpload.setValue(model.getUploadProgress());
    			btnRun.setSelected(false);
    			btnRun.setEnabled(false);
    			btnStop.setSelected(true);
    			btnStop.setEnabled(false);
    			break;
    		case Model.UPLOAD_STATE_UPLOADING:
    			System.out.println("Toolbar Uploading");
    			pbUpload.setVisible(true);
    			pbUpload.setValue(model.getUploadProgress());
    			btnRun.setSelected(true);
    			btnRun.setEnabled(false);
    			btnStop.setSelected(false);
    			btnStop.setEnabled(true);
    			break;
    		default:
    			//We are not uploading
    			pbUpload.setVisible(false);
    			if (model.getVisibleEditor()!=null) {
        			//We have an open connection, and a visible editor
    				//So we can safely allow code uploading
        			btnRun.setEnabled(true);
    			} else {
    				//We have an open connection, but no editor is visible
        			btnRun.setEnabled(false);       				
    			}
				btnRun.setSelected(false);
    			btnStop.setSelected(false);
    			btnStop.setEnabled(false); 
    		}
    		break;
		case Model.CONNECTION_STATE_CONNECTING:
			if (model.getUploadMethod()==Model.UPLOAD_MODE_TELNET) {
				tfTelnet.setEnabled(false);
			}
			btnRun.setSelected(false);
			btnRun.setEnabled(false);
			btnStop.setSelected(false);
			btnStop.setEnabled(false); 
			break;
		}
    	repaint();
    }
	
	//For now all key events are from the IP JTextField
	@Override
	public void keyTyped(KeyEvent e) {}
	@Override
	public void keyPressed(KeyEvent e) {}
	@Override
	public void keyReleased(KeyEvent e) {
		model.setTelnetIP(((JTextField)e.getSource()).getText());
	}

    public void update(Observable o, Object obj){

    	if (Model.getObserver((Integer)obj)==Model.UPDATE_APPLICATION) {
	    	if (model.getExit()>-2) { //Damn perform an exit
	    		if (!isExited) { //allow this class to only call model.setExit() once
	    			isExited = true;
	    			close();
	    			System.out.println("<g>"+o.getClass() + ": "+getClass()+" is closed!</g>");
	    			model.setExit();
	    		}
	    	}
    	} else if ((Model.getObserver((Integer)obj)==Model.UPDATE_UPLOAD_MODE) || (Model.getObserver((Integer)obj)==Model.UPDATE_CONNECTION_STATE)) {
    		//if (model.getUploadState()!=Model.UPLOAD_STATE_UPLOADING) {
    			updateToolbar();
    			//setVisibilityProgress(false);
    		//}
    	} else if (Model.getObserver((Integer)obj)==Model.UPDATE_TOOLBAR) {
    		//System.out.println("UPDATE IN TOOLBAR");
    		updateToolbar();    		
		}
    }

}
