package org.grebe.controls.upload;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import javax.swing.Timer;

import org.grebe.model.Model;




//Used for JSSC
import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import jssc.SerialPortList;

/*//Used for RXTX
import gnu.io.CommPort;
import java.util.Enumeration;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;*/

public class SerialController extends Thread implements ActionListener{

	private Model model;
	private OutputStream out;
	private InputStream in;
	private SerialPort serialPort;
	private ActionListener actionListener;
	private Timer timerUpdate;
	private ThreadEvent resultsReady = new ThreadEvent();
	
	public SerialController(Model model) {
		this.model = model;
		timerUpdate = new Timer(2000, this);
	}
	
	public void setEnablePortRefresh(boolean enable) {
		if (enable) {
			timerUpdate.start();
		} else {
			timerUpdate.stop();			
		}
	}
	
	public void setActionListener(ActionListener listener) {
		actionListener = listener;
	}

	private void changePorts(ArrayList<String> cSourcePorts, ArrayList<String> cNewPorts) {
		//Function for JSSC

        String[] portNames = SerialPortList.getPortNames();
        for(int i = 0; i < portNames.length; i++){
			 cNewPorts.add(portNames[i]);
        	boolean exist = false;
			for (String cSourceItem:cSourcePorts) {
				if (cSourceItem.equals(portNames[i])) {
					exist = true;
				}
			}
			if (exist == false) {
				model.addSerialPort(portNames[i]);
			}
        }
	}
	/*
	private void changePorts(ArrayList<String> cSourcePorts, ArrayList<String> cNewPorts) {
		//Function for RXTX

		        
				Enumeration<?> port_list = CommPortIdentifier.getPortIdentifiers(); 
				
				while (port_list.hasMoreElements()) {
				   CommPortIdentifier port_id = (CommPortIdentifier)port_list.nextElement();
				   if (port_id.getPortType() == CommPortIdentifier.PORT_SERIAL) {
					   cNewPorts.add(port_id.getName());
						boolean exist = false;
						for (String cSourceItem:cSourcePorts) {
							if (cSourceItem.equals(port_id.getName())) {
								exist = true;
							}
						}
						if (exist == false) {
							model.addSerialPort(port_id.getName());
						}
				   }
				   
			   }
	}*/
	

	//Used to refresh the available COM ports
	public void run() {
		while (true) {
			try {
				resultsReady.await();
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			ArrayList<String> cSourcePorts = model.getSerialPorts();
			ArrayList<String> cNewPorts = new ArrayList<String>();
			changePorts(cSourcePorts, cNewPorts);
	
			if (cSourcePorts.size()>0) {
				try {
					for (String cSourceItem:cSourcePorts) {
						boolean exist = false;
						for (String cNewItem:cNewPorts) {
							if (cNewItem.equals(cSourceItem)) {
								exist = true;
							}
						}
						if (exist == false) {
							model.removeSerialPort(cSourceItem);
						}
					}
				} catch (Exception ex) {}
			}
		}
	}
	
	public void connect(String portName) {
		serialPort = new SerialPort(portName); 
        try {
            serialPort.openPort();//Open port
            serialPort.setParams(model.getBaudRate(), 8, 1, 0);//Set params
            int mask = SerialPort.MASK_RXCHAR;//Prepare mask
            serialPort.setEventsMask(mask);//Set mask
            serialPort.addEventListener(new SerialPortReader());//Add SerialPortEventListener
        }
        catch (SerialPortException ex) {
            System.out.println(ex);
        }
	}
	/*
	 public void connect ( String portName ) throws Exception
	    {
	        CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(portName);
	        if ( portIdentifier.isCurrentlyOwned() )
	        {
	            System.out.println("Error: Port is currently in use");
	        }
	        else
	        {
	            CommPort commPort = portIdentifier.open(this.getClass().getName(),2000);
	            
	            if ( commPort instanceof SerialPort )
	            {
	                serialPort = (SerialPort) commPort;
	                serialPort.setSerialPortParams(model.getBaudRate(),SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
	                
	                in = serialPort.getInputStream();
	                out = serialPort.getOutputStream();
	                
	                (new Thread(new SerialReader(in))).start();
	                //(new Thread(new SerialWriter(out))).start();

	            }
	            else
	            {
	                System.out.println("Error: Only serial ports are handled by this example.");
	            }
	        }     
	    }*/
	    
	    /** */
	    public class SerialReader implements Runnable 
	    {
	        InputStream in;
	        
	        public SerialReader ( InputStream in )
	        {
	            this.in = in;
	        }
	        
	        public void run ()
	        {
	            byte[] buffer = new byte[1024];
	            int len = -1;
	            try
	            {
	            	//String line = "";
	            	do {
		                while ( ( len = this.in.read(buffer)) > -1 )
		                {
	                    	actionListener.actionPerformed(new ActionEvent(new String(buffer,0,len), 0, null));

		                	/*line += new String(buffer,0,len);
		                	if (line.indexOf('\n') !=-1) {
		                    	actionListener.actionPerformed(new ActionEvent(line.substring(0, line.indexOf('\n'))+'\n', 0, null));
		                    	line = line.substring(line.indexOf('\n')+1);
		                	}*/
		                	//System.out.println(new String(buffer,0,len));
		                	//System.out.print(new String(buffer, 0, len));
			                //model.setFromUploader(new String(buffer,0,len));
		                    //System.out.print(new String(buffer,0,len));
		                }
	            	} while (len > -1);
	            }
	            catch ( IOException e )
	            {
	                e.printStackTrace();
	            }            
	        }
	    }
	    
	    /*
	     * In this class must implement the method serialEvent, through it we learn about 
	     * events that happened to our port. But we will not report on all events but only 
	     * those that we put in the mask. In this case the arrival of the data and change the 
	     * status lines CTS and DSR
	     */
	    class SerialPortReader implements SerialPortEventListener {

	        public void serialEvent(SerialPortEvent event) {
	            if(event.isRXCHAR()){//If data is available
	                if(event.getEventValue()>0){//Check bytes count in the input buffer
	                    //Read data, if 10 bytes available 
	                    try {
	                    	actionListener.actionPerformed(new ActionEvent(new String(serialPort.readBytes()), 0, null));
	                        //byte buffer[] = serialPort.readBytes(10);
	                    }
	                    catch (SerialPortException ex) {
	                        System.out.println(ex);
	                    }
	                }
	            }
	        }
	    }
	    
	    public void println(byte[] b) throws IOException, SerialPortException {
	    	serialPort.writeBytes(b); //For JSSC
	    	//out.write(b); //for RXTX
	    }

	    public void close() {
	    	if (serialPort!=null) {
	            try {
	                // close the i/o streams. when in rxtx mode
	                if (out!=null) out.close();
	                if (in!=null) in.close();
	            } catch (IOException ex) {
	                // don't care
	            }
	            // Close the port.
	            //serialPort.close(); //For rxtx
	            try {
					serialPort.closePort(); //For JSSC
				} catch (SerialPortException e) {} 
	    	}
	    }

		@Override
		public void actionPerformed(ActionEvent ev) {
			if (ev.getSource() == timerUpdate) {
				resultsReady.signal();
			}
		}
		public class ThreadEvent {

		    private final Object lock = new Object();

		    public void signal() {
		        synchronized (lock) {
		            lock.notify();
		        }
		    }

		    public void await() throws InterruptedException {
		        synchronized (lock) {
		            lock.wait();
		        }
		    }
		}
}
