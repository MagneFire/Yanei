package org.grebe.controls.upload;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;




public class TCPController{

	private ActionListener actionListener;
	private OutputStream out;
	private Socket clientSocket;
	
	public void setActionListener(ActionListener listener) {
		actionListener = listener;
	}
		
	
	public void connect(InetAddress ip, int port) throws IOException {
		 	// System.out.println("<q>Flag: </q><g>"+(tcpMessage.getFlag()& 0xFF)+"</g><q>, Message Number: </q><g>"+(tcpMessage.getMessageNumber())+"</g>");
	    	 System.out.println("<o>Start communicating...</o>");
	    	// Socket clientSocket = new Socket(ip, 30000);
	    	 clientSocket = new Socket();
	    	 clientSocket.connect(new InetSocketAddress(ip, port), 4000);

             InputStream in = clientSocket.getInputStream();
             out = clientSocket.getOutputStream();
             
             (new Thread(new SerialReader(in))).start();
	    	 
	    	 
	    	 //System.out.println(Arrays.toString(tcpMessage.getAllData()));
	    	 //outToServer.write("HEY".getBytes(), 0, "HEY".getBytes().length);
	    	/* outToServer.write(tcpMessage.getAllData(), 0, tcpMessage.getAllData().length); //Send data to Arduino
	    	 System.out.println("<g>Sent</g>, <o>now receiving...</o>");
	    	 int status = inFromServer.read(dataIn);
	    	 System.out.println("<q>Remote IP: </q><y>"+ip.getHostAddress()+"</y><q>, Status: </q><g>"+status+"</g><q>, Received: </q><g>"+(dataIn[0]& 0xFF)+" "+(dataIn[1]& 0xFF)+" "+(dataIn[2]& 0xFF)+" "+(dataIn[3]& 0xFF)+"</g>");
	    	 
	 		
	    	 clientSocket.close();
	    	 outToServer.close();
	    	 inFromServer.close();*/
	    	// System.out.println("<g>Communication finished succesfully!</g>");
	    	//} catch (Exception ex) {
	    	//	System.err.println(ex.toString());
	    		/*if (ex.getMessage().equals("connect timed out")) {
	    			if (tcpMessage.getTries()>0) {
	    				System.out.println("<o>Connection timeout, retrying..</o>");
						sendBytes(tcpMessage);	    				
	    			} else {
	    				procesTCPMessage(tcpMessage, null);
	    			}
	    		} else {
    				procesTCPMessage(tcpMessage, null); //Unknown error!	    			
	    		}*/
	    //	};
		
	}
	
	
	 /** */
    public class SerialReader implements Runnable {
        InputStream in;
        
        public SerialReader ( InputStream in )
        {
            this.in = in;
        }
        
        public void run ()
        {
            byte[] buffer = new byte[1024];
            int len = -1;
            try
            {
            	//String line = "";
            	do {
	                while (( len = this.in.read(buffer)) > -1 )
	                {
                    	actionListener.actionPerformed(new ActionEvent(new String(buffer,0,len)+'\n', 0, null));
	                	/*line += new String(buffer,0,len);
	                	if (line.indexOf('\n') !=-1) {
	                    	actionListener.actionPerformed(new ActionEvent(line.substring(0, line.indexOf('\n'))+'\n', 0, null));
	                    	line = line.substring(line.indexOf('\n')+1);
	                	}*/
	                	//System.out.println(new String(buffer,0,len));
	                	//System.out.print(new String(buffer, 0, len));
		                //model.setFromUploader(new String(buffer,0,len));
	                    //System.out.print(new String(buffer,0,len));
	                }
            	} while (len > -1);
            }
            catch ( IOException e )
            {
                e.printStackTrace();
            }            
        }
    }
	
	public void close() {
		if (clientSocket!=null) {
			if (clientSocket.isConnected()) {
				try {
					clientSocket.close();
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}				
			}
		}
	}
	    
	public void println(byte[] b) throws IOException {
	   	out.write(b);
	}	
}
