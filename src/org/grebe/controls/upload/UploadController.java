package org.grebe.controls.upload;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.TimerTask;

import org.grebe.model.Model;
import org.grebe.model.PreUploadDictionary;
import org.grebe.utils.Utils;

public class UploadController extends Thread implements Observer, ActionListener{

	private Model model;
    private boolean isExited = false;
	private SerialController serialController;
	private TCPController tcpController;
	private boolean isUploading = false;
	private boolean requestUpload = false;
	private ThreadEvent resultsReady = new ThreadEvent();
	private int bufferReadPosition = 0, bufferReadLinePosition, bufferPosition = 0; // used to determine what part has been read by the uploader
	private char buffer[]; // use a buffer to allow for faster transfer rates, since the serial/tcp controllers only store their data in this buffer
	private ArrayList<String> bufferedLines;
	private javax.swing.Timer timerRead; //Responsible for reading the buffer and uploading in smart mode
	private ArrayList<String> uploadLines;
	private int progress = 0;
	
	public UploadController(Model model) {
		this.model = model;
		buffer = new char[model.getConsoleBufferSize()*1000];
		bufferedLines = new ArrayList<String>();
		tcpController = new TCPController();
		tcpController.setActionListener(this);
		timerRead = new javax.swing.Timer(1000, this); //read the buffer every 100 ms, when uploading this will be lower, to allow faster uploading
		timerRead.start();
		start();
		resultsReady.signal();
	}
	
	public void setSerialController(SerialController controller) {
		serialController = controller;
		serialController.setActionListener(this);
	}

	public boolean connect() {
		if (model.getUploadMethod()==Model.UPLOAD_MODE_SERIAL) {
			model.setConnectionState(Model.CONNECTION_STATE_CONNECTING);
			try {
				serialController.connect(model.getSelectedDevice());
				model.setConnectionState(Model.CONNECTION_STATE_OPEN);
				return true;
			} catch (Exception e) {
				model.setConnectionState(Model.CONNECTION_STATE_FAIL);
				System.out.println(e.getMessage());
				//e.printStackTrace();
				return false;
			}
		} else if (model.getUploadMethod()==Model.UPLOAD_MODE_TELNET) {
			if ((model.getTelnetIP() != null) && (model.getTelnetIP().length()!=0) && (!model.getTelnetIP().equals("")) && (model.getTelnetIP().trim().equals(model.getTelnetIP()))) {
				if (model.getTelnetIP().lastIndexOf(':')!=-1) {
					String ip[] = model.getTelnetIP().split(":");
					if (Utils.isNumeric(ip[1])) {
						model.setConnectionState(Model.CONNECTION_STATE_CONNECTING);
						try {
							tcpController.connect(InetAddress.getByName(ip[0]), Integer.parseInt(ip[1]));
							model.setConnectionState(Model.CONNECTION_STATE_OPEN);
							return true;
						} catch (Exception e) {
							System.out.println(e.getMessage());
							model.setConnectionState(Model.CONNECTION_STATE_FAIL);
							//e.printStackTrace();
							return false;
						}						
					} else {
						return false;
					}
				} else {
					model.setConnectionState(Model.CONNECTION_STATE_CONNECTING);
					try {
						tcpController.connect(InetAddress.getByName(model.getTelnetIP()), 80);
						model.setConnectionState(Model.CONNECTION_STATE_OPEN);
						return true;
					} catch (Exception e) {
						System.out.println(e.getMessage());
						model.setConnectionState(Model.CONNECTION_STATE_FAIL);
						//e.printStackTrace();
						return false;
					}
				}
			} else {
				//Invalid telnet ip
				model.setConnectionState(Model.CONNECTION_STATE_FAIL);
			}
		}
		return false;
	}
	
	public void close() {
		System.out.println("Closing connection...");
		Timer timer = new Timer();
		final Thread thr = new Thread(new Runnable() {
			
			@Override
			public void run() {
				if (model.getUploadMethod()==Model.UPLOAD_MODE_SERIAL) {
					serialController.close();
				} else if (model.getUploadMethod()==Model.UPLOAD_MODE_TELNET) {
					tcpController.close();					
				}
				model.setConnectionState(Model.CONNECTION_STATE_CLOSED);
			}
		});
		

		timer.schedule(new TimerTask() {
			
			@Override
			public void run() {
				thr.interrupt();
			}
		}, 5000);
		
		thr.start();
		try {
			thr.join();
		} catch (InterruptedException e) {
			System.out.println("Took to long to close port!");
			model.setConnectionState(Model.CONNECTION_STATE_FAIL);
			e.printStackTrace();
		}
		System.out.println("Connection closed...");
		//Nothing to do
	}

	    /**
	     * Does some pre-upload stuff, like wait for sending the next line using --$(delay)<num>
	     * @param line line to send
	     * @return true if we need to send this to the device, no if we should not send it to the device
	     */
	    private boolean doPreUpload(String line) {
	    	if ((line.trim()).startsWith(PreUploadDictionary.IDENTIFIER)) {
	    		String action = (line.trim()).substring((line.trim()).indexOf('(')+1, (line.trim()).indexOf(')'));
	    		String param = (line.trim()).substring((line.trim()).indexOf(')')+1);
    			System.out.println("Action: "+action);
    			System.out.println("Parametr: "+param);
    			if (action.equals(PreUploadDictionary.DELAY)) {
    				if (Utils.isNumeric(param)) {
		    			try {Thread.sleep(Integer.parseInt(param));} catch (InterruptedException e) {e.printStackTrace();} //wait while sending    					
    				} else {
    					System.out.println("Parameter is not numeric");
    				}
    				return false;
    			}
			}
	    	return true;
	    }
	    
	    /**
	     * Sends line to the device, using a serial or TCP connection
	     * @param line the line to be send
	     */
	    private void sendLine(String line) {
	    	if (model.getUploadMethod()==Model.UPLOAD_MODE_SERIAL) {
		    	try {
					serialController.println((line+"\n").getBytes());
				} catch (Exception e) {
					System.out.println(e.getMessage());
					requestUpload = false; //cancel upload
					model.setConnectionState(Model.CONNECTION_STATE_FAIL);
				}
		    	if (model.getUploadMode()==Model.UPLOAD_MODE_RESPONSE) {
			    	/*while (Utils.similarity(this.line, line)<0.6) {
			    		System.out.println(Utils.similarity(this.line, line));
			    	}*/
		    	}
	    	} else if (model.getUploadMethod()==Model.UPLOAD_MODE_TELNET) {
		    	try {
					tcpController.println((line+"\n").getBytes());
				} catch (IOException e) {
					System.out.println(e.getMessage());
					requestUpload = false; //cancel upload
					model.setConnectionState(Model.CONNECTION_STATE_FAIL);
				}
		    	if (model.getUploadMode()==Model.UPLOAD_MODE_RESPONSE) {
			    	/*while (Utils.similarity(this.line, line)<0.6) {
			    		System.out.println(Utils.similarity(this.line, line));
			    	}*/
		    	}
	    		
	    	}
	    	
			System.out.println("SENT: "+line);
	    	
	    }
	    
	    /**
	     * This method is responsible for cleaning the code to be send.
	     * It removes all comments, and strips all leading and trailing whitespaces
	     * @param code the code to be cleaned
	     * @return the individual lines in an array
	     */
	    private String[] cleanCode(String code) {
    		String lines[] = code.split("\n");
    		ArrayList<String> linesStipped = new ArrayList<String>();
    		
    		for (String line : lines) {
    			//check if the line doesnt start with '--' and is not empty, or line starts with the precompiller identifier
    			if (((!line.trim().startsWith("--")) &&  (!line.trim().isEmpty())) || (line.trim().startsWith(PreUploadDictionary.IDENTIFIER))) {
    				//Strip comments only if the line doesn't start with the PreUpload Identifier
    				if (!(line.trim().startsWith(PreUploadDictionary.IDENTIFIER))) {
	    				//now strip the comments at the end of a line
	    				//First check if we have quotes
	    				String lineStripped = line.trim();
	    				if ((lineStripped.indexOf('\"')!=-1) || (lineStripped.indexOf('\'')!=-1)) {
	    					//Line contains comments, now we need to figure out where the comment sections start and end
	    					//This is difficult, because we need to check quotes in pairs.
	    					//And we can have multiple pairs
	    		        	int posRec = lineStripped.length();
	    		        	int posQuote = 0;
	    		        	int lastPos = 0;
	    		        	int endQuote = 0;
	    		        	int startRec = 0;
	    		        	while (posQuote<posRec) {
	    		        		endQuote = 0;
	    		            	while(posQuote<lineStripped.length() && lineStripped.charAt(posQuote)!='\"' && lineStripped.charAt(posQuote)!='\'') {
	    		            		posQuote++;
	    		            	}
	    		            	char beginQ = 0;
	    		            	if (posQuote<lineStripped.length()) {
	    		            		beginQ = lineStripped.charAt(posQuote);
	    		            	}
	    		            	
	    		            	endQuote++;
	    		            	while((posQuote+endQuote)<lineStripped.length() && lineStripped.charAt(posQuote+endQuote)!=beginQ && lineStripped.charAt(posQuote+endQuote)!='\n') {
	    		            		endQuote++;
	    		            	}
	    		            	if ((posQuote+endQuote)<lineStripped.length() && lineStripped.charAt(posQuote+endQuote)!='\n') {
	    		                	endQuote++; //Let endQuote only increase if the (posQuote+endQuote)+1 == "
	    		            	}
	    		            	
	    		               // aset = StyleContext.getDefaultStyleContext().addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, LuaDictionary.COLOR_QUOTES);
	    		        	//	doc.setCharacterAttributes(startRec+posQuote, endQuote, aset, false);
	
	    		        		String comm = lineStripped.substring(lastPos, posQuote);
	    		        		
	    		            	if (comm.indexOf("--")>=0) {
	    		                   // aset = StyleContext.getDefaultStyleContext().addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, LuaDictionary.COLOR_COMMENT);
	    		            	//	doc.setCharacterAttributes(startRec+(comm.indexOf("--")+lastPos), posRec-(comm.indexOf("--")+lastPos), aset, false);
	    		            		lineStripped = lineStripped.substring(0, startRec+(comm.indexOf("--")+lastPos)).trim();
	    		            		break;
	    		            	}
	    		        		
	    		        		posQuote+=endQuote;
	    		        		lastPos = posQuote;
	    		        	}
	    				} else if (lineStripped.indexOf("--")!=-1) {
	    					//No comments are present, just search for the first occurrence of --
	    					//And take the first part
	    					lineStripped = lineStripped.substring(0, lineStripped.indexOf("--")).trim();
	    				}
	    				linesStipped.add(lineStripped);	
	    			} else {
	    				linesStipped.add(line.trim());	    				
	    			}
    			}
    		}
    		
    		lines = new String[linesStipped.size()];
    		lines = linesStipped.toArray(lines);
    		return lines;
	    }
	    
	    
	    /**
	     * This method is responsible for uploading the code.
	     */
	    public void run() {
	    	while(true) {
	    		System.out.println("WAITING FOR UPLOAD!");
	    		try {
					resultsReady.await();
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
	    		System.out.println("READY FOR UPLOAD!");
	    		if (requestUpload == true) {
	    			System.out.println("UPLOADIG THREAD");
	    			String uploadCode = model.getToUploader();
		    		if (uploadCode != null) {			
		    			//Complex if, but it checks if the upload method is serial, if so check if we have a device selected, else (in telnet mode) just continue
			    		if ((model.getUploadMethod()==Model.UPLOAD_MODE_SERIAL) ? (model.getSelectedDevice()!=null) : true) {
			    			try {
			    				isUploading = true;
			    				model.setUploadState(Model.UPLOAD_STATE_UPLOADING);
			    				
			    				serialController.setEnablePortRefresh(false);//Disable port refreshing
			    				
					    		System.out.println("Connectiong to: "+((model.getUploadMethod()==Model.UPLOAD_MODE_SERIAL) ? model.getSelectedDevice() : model.getTelnetIP()));
					    		//Open a connection only when the connection is closed
					    		if ((model.getConnectionState()==Model.CONNECTION_STATE_CLOSED) || model.getConnectionState()==Model.CONNECTION_STATE_FAIL) {
					    			//Connect, and only continue when we have successfully connected
						    		if (!connect()) {
						    			//Only continue when we have successfully connected
						    		}
					    		}
					    		if (model.getConnectionState()==Model.CONNECTION_STATE_OPEN) {
						    		System.out.println("Cleaning...");
						    		
						    		uploadLines = new ArrayList<String>(Arrays.asList(cleanCode(uploadCode)));
						    		progress = uploadLines.size();
						    		
						    		System.out.println("------- Stripped code lines: "+uploadLines.size()+" ---------");
						    		System.out.println("------------------- Stripped code ---------------------");
						    		for (String line: uploadLines) {
						    			System.out.println(line);
						    		}
						    		System.out.println("------------------ End stripped code ------------------");
						    		
						    		System.out.println("Uploading...");
			
						    		if (doPreUpload(uploadLines.get(0))) {
						    			sendLine(uploadLines.get(0));
						    		}
						    		//Use delay based uploading when the mode is delay or when we use telnet method and response based, since we cannot rely on response based uploading in telnet mode
						    		if (model.getUploadMode()==Model.UPLOAD_MODE_DELAY || (model.getUploadMethod()==Model.UPLOAD_MODE_TELNET && model.getUploadMode()==Model.UPLOAD_MODE_RESPONSE)) {
						    			timerRead.setDelay(100);
							    		for (int i=1;i<uploadLines.size();i++) {
							    			//First check if we want to cancel the upload
							    			if (requestUpload == false)  {model.setUploadState(Model.UPLOAD_STATE_NONE);break;}
							    			
				
									    	if (model.getUploadMode()==Model.UPLOAD_MODE_DELAY) {
									    		try {Thread.sleep(model.getLineDelay());} catch (InterruptedException e) {} //wait while sending
									    	}
				
							    			if (doPreUpload(uploadLines.get(i))) {
							    				sendLine(uploadLines.get(i));
							    			}
							    			
							    			//Adding to the console is not reliable when receiving
							    			//data, this make the console messy
							    			//model.addToConsoleBuffer("SENT: "+lines[i]);
							    			model.setUploadProgress((int) Utils.map(i, 0, uploadLines.size(), 0, 100));
							    		}
					    				model.setUploadProgress(100);
					    				model.setUploadState(Model.UPLOAD_STATE_DONE);
					    				
					    				serialController.setEnablePortRefresh(true);//Enable port refreshing after the code is uploaded
						    		} else {
						    			//Do response based uploading
						    			timerRead.setDelay(1);
						    		}
					    		} else {
						    		isUploading = false;
						    		requestUpload = false;			    			
					    		}
					    		//closeSerial();
			    			} catch (Exception ex) {
			    				ex.printStackTrace();
					    		isUploading = false;
					    		requestUpload = false;
			    				model.setUploadState(Model.UPLOAD_STATE_FAIL);
			    			}
			    			//model.requestUploader(Model.UPLOADER_REQUEST_NONE);
			    			//model.setUploadProgress(-1); //Force update
			    			model.updateObserver(Model.UPDATE_TOOLBAR, Model.UPDATE_ACTION_GENERAL);
			    		} else {
			    			System.err.println("Cannot upload!");
				    		isUploading = false;
				    		requestUpload = false;
			    		}				    			
		    		}
	    		}
	    	}
	    }
	    
	    
	    public void update(Observable o, Object obj){

	    	if (Model.getObserver((Integer)obj)==Model.UPDATE_APPLICATION) {
		    	if (model.getExit()>-2) { //Damn perform an exit
		    		if (!isExited) { //allow this class to only call model.setExit() once
		    			isExited = true;
		    			close();
		    			System.out.println("<g>"+o.getClass() + ": "+getClass()+" is closed!</g>");
		    			model.setExit();
		    		}
		    	}
	    	} else if (Model.getObserver((Integer)obj)==Model.UPDATE_UPLOADER) {
	    		System.out.println("UPDATE IN UPLOADER");
	    		switch(Model.getAction((Integer)obj)) {
	    		case Model.UPDATE_UPLOADER_CANCEL:
	    			requestUpload = false;
	    			model.setUploadState(Model.UPLOAD_STATE_CANCELING);
	    			break;
	    		case Model.UPDATE_UPLOADER_CLOSE:
	    			//Only close the connection when we are connection or when we are connected
	    			if (model.getConnectionState()==Model.CONNECTION_STATE_CONNECTING || model.getConnectionState()==Model.CONNECTION_STATE_OPEN) {
		    			//If the thread is uploading, cancel it, and wait for it to
		    			//properly stop uploading, then close the connection
		    			if (requestUpload==true && isUploading) {
			    			model.setUploadState(Model.UPLOAD_STATE_CANCELING);
			    			requestUpload = false;
			    			while(isUploading) {}// wait for upload to stop
		    			}
		    			close();
	    			}
	    			break;
	    		case Model.UPDATE_UPLOADER_OPEN:
		    		System.out.println("UPLOADER: OPENING");
	    			//Only allow to open a connection when the connection is closed or the previous connection failed to setup
	    			if (model.getConnectionState()==Model.CONNECTION_STATE_CLOSED || model.getConnectionState()==Model.CONNECTION_STATE_FAIL) {
			    		new Thread(new Runnable() {						
							@Override
							public void run() {
					    		System.out.println("Connectiong to: "+((model.getUploadMethod()==Model.UPLOAD_MODE_SERIAL) ? model.getSelectedDevice() : model.getTelnetIP()));
					    		if (!connect()) {
					    			//Only continue when we have successfully connected
					    			model.setUploadState(Model.UPLOAD_STATE_FAIL);
					    			return;
					    		}
							}
						}).start();
	    			}
	    			break;
	    		case Model.UPDATE_UPLOADER_UPLOAD:
		    		System.out.println("UPLOADER: UPLOADING");
		    		resultsReady.signal();
	    			//Only upload when we are not uploading
	    			if (!isUploading) {
	    				requestUpload = true;
	    			}
	    			break;	    		
	    		}
	    	}
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() instanceof String) {
				int length = ((String)(e.getSource())).toCharArray().length;
				while (length>0) {
					buffer[bufferPosition] = ((String)(e.getSource())).toCharArray()[((String)(e.getSource())).toCharArray().length-length];
					bufferPosition++;
					length--;
				}
				//model.setFromUploader((String)e.getSource());
			} else if (e.getSource() == timerRead) {
				int pos = bufferPosition;
    			if (uploadLines!=null && uploadLines.size()>0 && requestUpload == false)  {
    				uploadLines.clear();
    				uploadLines = null;
    				isUploading = false;
    				model.setUploadState(Model.UPLOAD_STATE_NONE);
    				model.setUploadProgress(100);
    				serialController.setEnablePortRefresh(true);//Enable port refreshing after the code is uploaded		
    				return;
    			} //Clear upload lines to cancel upload
				//Only allow response based uploading in serial mode, telnet does not necessarily return the sent line
				if (uploadLines!=null && uploadLines.size()>0 && model.getUploadMethod()==Model.UPLOAD_MODE_SERIAL && model.getUploadMode()==Model.UPLOAD_MODE_RESPONSE) {
					//Only fetch lines if the unread part of the buffer contains a newline char (\n)
					//Makes the code more efficient when serial is being spammed :)
					if (new String(Arrays.copyOfRange(buffer, bufferReadLinePosition, pos)).contains("\n")) {
						int posr = bufferReadLinePosition;
						char buf[] = Arrays.copyOfRange(buffer, bufferReadLinePosition, pos); //Read unread part
						//System.out.println(Arrays.toString(buffer));
						String line = "";
						//Only read the lines, if the buffer does not end with \n we will look at it later
						for (int i=0;i<buf.length;i++) {
							if (buf[i]=='\n') {
								bufferedLines.add(line);
								line = "";
								bufferReadLinePosition = posr+1+i;
							} else {
								line += buf[i];
							}
						}
						//model.setFromUploader(recv);
						if (bufferedLines.size()>0) {		
							for (String l : bufferedLines) {
								if (l.contains(uploadLines.get(0))) {
					    			uploadLines.remove(0);
					    			if (uploadLines.size()>0) {
						    			model.setUploadProgress((int) Utils.map(progress-uploadLines.size(), 0, progress, 0, 100));
							    		if (doPreUpload(uploadLines.get(0))) {
							    			sendLine(uploadLines.get(0));
							    		}
					    			} else {
					    				//List is now empty
					    				//set upload to done
							    		isUploading = false;
							    		requestUpload = false;
					    				serialController.setEnablePortRefresh(true);//Enable port refreshing after the code is uploaded
					    				model.setUploadProgress(100);
					    				model.setUploadState(Model.UPLOAD_STATE_DONE);
						    			model.updateObserver(Model.UPDATE_TOOLBAR, Model.UPDATE_ACTION_GENERAL);
					    			}
									
								}
							}
							bufferedLines.clear();
						}
					}
				}
				//Always update the console
				String recv = new String(Arrays.copyOfRange(buffer, bufferReadPosition, pos));
				if (!recv.isEmpty()) {
					model.setFromUploader(recv);
					bufferReadPosition = pos;
				}
			}
		}
		public class ThreadEvent {

		    private final Object lock = new Object();

		    public void signal() {
		        synchronized (lock) {
		            lock.notify();
		        }
		    }

		    public void await() throws InterruptedException {
		        synchronized (lock) {
		            lock.wait();
		        }
		    }
		}
	
}
