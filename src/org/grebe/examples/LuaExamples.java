package org.grebe.examples;

import java.util.Arrays;

public class LuaExamples {
	
	public static final String PATH = "/org/grebe/examples/";
	
	public static final String displaynames[][] = {
		{
		"Connect to AP",
		"Simple HTTP Server",
		"Telnet",
		"Toggle pin via web",
		"UDP Client & Server"
		}, {
			"submenu1",
			"displayname"
		}
	};
	public static final String filenames[][] = {
		{
		"connect.lua",
		"httpserver.lua",
		"telnet.lua",
		"webap_toggle.lua",
		"udp.lua"
		}, {
			"ROOTFILENAME,NOT USED",
			"filename"
		}
	};
	
	public static String[] getAllDisplayNames() {
		return displaynames[0];
	}
	
	public static String[] getAllFileNames() {
		return filenames[0];
	}
	
	public static String getFileName(String displayname) {
		for (int i=0;i<displaynames[0].length;i++) {
			if (displaynames[0][i].equals(displayname)) {
				return filenames[0][i];
			}
		}		
		return null;
	}

	public String[] getAllDisplayNames(int i) {
		return Arrays.copyOfRange(displaynames[i], 1, displaynames[i].length);
	}
	
	public String[] getAllFileNames(int i) {
		return Arrays.copyOfRange(filenames[i], 1, filenames[i].length);
	}

	public static String getFileName(int row, String displayname) {
		for (int i=0;i<displaynames[row].length;i++) {
			if (displaynames[row][i].equals(displayname)) {
				return filenames[row][i];
			}
		}		
		return null;
	}
	
	public int getGroups() {
		return displaynames.length;
	}
	
}
