package org.grebe.lang;

/**
 * Created by jheyns on 2014/10/16.
 */
public class ComponentManager {

	private static final ComponentManager instance;

	static {
		instance = new ComponentManager(new ComponentTextDefaults());
	}

	private ComponentTextDefaults componentTextDefaults;

	private ComponentManager(ComponentTextDefaults componentTextDefaults) {
		this.componentTextDefaults = componentTextDefaults;
	}

	public static ComponentManager getInstance() {
		return instance;
	}

	public ComponentTextDefaults getComponentTextDefaults() {
		return componentTextDefaults;
	}

	public void setComponentTextDefaults(
			ComponentTextDefaults componentTextDefaults) {
		this.componentTextDefaults = componentTextDefaults;
	}


}
