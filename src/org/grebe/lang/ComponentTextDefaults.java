package org.grebe.lang;

import java.util.Enumeration;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * Created by jheyns on 2014/10/16.
 */
public class ComponentTextDefaults {

	private Properties texts;

	public ComponentTextDefaults() {
		try {
			texts = toProperties(ResourceBundle.getBundle("org.grebe.lang.Text", Locale.getDefault()));
		} catch (Exception ex) {
			System.out.println("#Warning: Could not find resources for language: '"+Locale.getDefault()+"'");
			//We assume the language English is always available
			texts = toProperties(ResourceBundle.getBundle("org.grebe.lang.Text", Locale.ENGLISH));			
		}
		
	}

	private Properties toProperties(ResourceBundle resource) {
		Properties result = new Properties();
		Enumeration<String> keys = resource.getKeys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			result.put(key, resource.getString(key));
		}
		return result;
	}

	public String getText(String key) {
		return texts.getProperty(key);
	}
}
