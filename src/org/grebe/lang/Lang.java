package org.grebe.lang;




public class Lang {

	//Define the available strings
	public static String AUTHOR 			= "file.author";
	
	public static String OK 				= "text.ok";
	public static String CANCEL 			= "text.cancel";
	public static String CLEAR 				= "text.clear";
	public static String SEND 				= "text.send";
	public static String CLOSE 				= "text.close";
	public static String VERSION			= "text.version";
	public static String YES				= "text.yes";
	public static String NO					= "text.no";
	public static String LOADING			= "text.loading";
	public static String CONNECTING			= "text.connecting";
	public static String CONNECT			= "text.connect";
	public static String ERROR				= "text.error";
	public static String PREFERENCES		= "text.preferences";
	public static String QUIT				= "text.quit";
	public static String ABOUT				= "text.about";
	public static String PREFIX_PORT		= "text.prefixport";
	public static String RESTART_DEVICE		= "text.restartdevice";
	public static String CLOSE_CONNECTION	= "text.closeconnection";
	public static String OPEN_CONNECTION	= "text.openconnection";
	public static String FILE				= "text.file";
	public static String EDIT				= "text.edit";
	public static String RUN				= "text.run";
	public static String UPLOAD				= "text.upload";
	public static String DEVICE				= "text.device";
	public static String LUA				= "text.lua";
	public static String MONITOR			= "text.monitor";
	public static String UNDO				= "text.undo";
	public static String REDO				= "text.redo";
	public static String NEW				= "text.new";
	public static String OPEN_FILE			= "text.openfile";
	public static String IMPORT_FILE		= "text.importfile";
	public static String SAVE				= "text.save";
	public static String EXAMPLES			= "text.examples";

	public static String NEW_FILE			= "text.newfile";
	public static String NEW_FOLDER			= "text.newfolder";
	public static String RENAME				= "text.rename";
	public static String DELETE				= "text.delete";
	public static String COPY				= "text.copy";
	public static String CUT				= "text.cut";
	public static String PASTE				= "text.paste";
	public static String NYI				= "text.nyi";
	public static String SAVE_AS			= "text.saveas";
	public static String APPLY				= "text.apply";
	public static String CREATE				= "text.create";

	public static String ENTER_IP			= "text.enterip";
	public static String UPLOAD_MODE		= "text.uploadmode";
	public static String METHOD_SERIAL		= "text.method.serial";
	public static String METHOD_TELNET		= "text.method.telnet";
	public static String MODE_DELAY			= "text.mode.delay";
	public static String MODE_RESPONSE		= "text.mode.response";
	public static String LINE_DELAY			= "text.linedelay";
	public static String SELECT_ALL			= "text.selectall";

	public static String MSG_CLOSE 								= "text.msg.close";
	public static String MSG_ABOUT								= "text.msg.about";
	public static String MSG_ERROR_OCCURRED						= "text.msg.erroroccurred";
	public static String MSG_CHOOSE_FILE						= "text.msg.choosefile";
	public static String MSG_OVERWRITE_FILE						= "text.msg.fileoverwrite";
	public static String MSG_CONNECTION_ERROR					= "text.msg.connectionerror";
	public static String MSG_FONT_NOT_NUMERIC					= "text.msg.mustnumeric.fontsize";
	public static String MSG_LINE_DELAY_NOT_NUMERIC				= "text.msg.mustnumeric.linedelay";
	public static String MSG_CONSOLE_BUFFER_SIZE_NOT_NUMERIC	= "text.msg.mustnumeric.consolebuffer";

	public static String TITLE 					= "text.title";
	public static String TITLE_FULL				= "text.titlefull";
	public static String TITLE_OVERWRITE_FILE	= "text.title.fileoverwrite";
	public static String TITLE_CONNECTION_ERROR	= "text.title.connectionerror";
	public static String TITLE_FORMAT_ERROR		= "text.title.formaterror";

	public static String CONVERT_TO_RELATIVE		= "text.converttorelative";
	public static String LOCATION_EXPLORER			= "text.locationexplorer";
	public static String FONT_SIZE					= "text.fontsize";
	public static String CONSOLE_BUFFER_SIZE		= "text.consolebuffersize";
	

	
	private static ComponentTextDefaults getTexts() {
        return ComponentManager.getInstance().getComponentTextDefaults();
    }
	public static String getText(String type) {
		return getTexts().getText(type);
	}
}
