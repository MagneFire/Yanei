package org.grebe.model;

import java.awt.Color;
import java.util.HashMap;

public class LuaDictionary {
	public static final String COMMENT = "--";
	public static final String IF = "if";
	public static final String END = "end";
	public static final String QUOTE = "\"";

	public static final Color COLOR_QUOTES = Color.ORANGE.darker();
	public static final Color COLOR_COMMENT = Color.GREEN.darker().darker();
	public static final Color COLOR_NUMBER = Color.MAGENTA.darker();
	
	public static final HashMap<String, Color> DICTIONARY = new HashMap<String, Color>(){
		private static final long serialVersionUID = 1L;
	{
	       put("node",Color.BLUE);
	       put("file",Color.BLUE);
	       put("wifi",Color.BLUE);
	       put("sta",Color.CYAN.darker());
	       put("ap",Color.CYAN.darker());
	       put("timer",Color.BLUE);
	       put("gpio",Color.BLUE);
	       put("pwm",Color.BLUE);
	       put("net",Color.BLUE);
	       put("socket",Color.CYAN);
	       put("server",Color.CYAN);
	       put("i2c",Color.BLUE);
	       put("adc",Color.BLUE);
	       put("uart",Color.BLUE);
	       put("ow",Color.BLUE);
	       put("bit",Color.BLUE);
	       put("spi",Color.BLUE);
	       put("mqtt",Color.BLUE);
	       put("print",Color.BLUE);
	       put("function",Color.BLUE.darker());
	       put("if",Color.MAGENTA.darker());
	       put("then",Color.MAGENTA.darker());
	       put("end",Color.MAGENTA.darker());
	}};

}
