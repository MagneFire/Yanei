package org.grebe.model;
import java.awt.Dimension;
import java.io.File;
import java.util.ArrayList;
import java.util.Observable;

import javax.swing.JPanel;





public class Model extends Observable{	

	public static final int APPLICATION_VERSION = 1;
	
	public static final Dimension APPLICATION_SIZE 	= new Dimension(1000, 650);
	public static final Dimension PREFERENCES_SIZE 	= new Dimension(400, 360);
	public static final Dimension ABOUT_SIZE	 	= new Dimension(400, 330);
	
	//Update mechanism works as follows: for the observer we use a the third digit
	//of the number. so: 000, update main application, 400 update editor, save all
	// 402, update editor, delete file
	//when updating a observer, we can pass a object through, we will use this function to
	//determine which view needs to be updated.
	public static final int UPDATE_APPLICATION 				= 0;
	public static final int UPDATE_MAIN 					= 1;
	public static final int UPDATE_MENU 					= 2;
	public static final int UPDATE_TOOLBAR 					= 3;
	public static final int UPDATE_EDITOR 					= 4;
	public static final int UPDATE_CONSOLE 					= 6;
	public static final int UPDATE_VISIBILITY				= 7; //Used to set the visibility of the explorer and / or the console
	public static final int UPDATE_EXPLORER					= 8;
	//Below are network/connection updates related
	public static final int UPDATE_UPLOADER					= 5;
	public static final int UPDATE_UPLOAD_MODE				= 9;
	public static final int UPDATE_CONNECTION_STATE			= 10;
	
	//Below are update actions
	public static final int UPDATE_ACTION_GENERAL			= 0;

	public static final int UPDATE_EXPLORER_SELECTION		= 1;
	public static final int UPDATE_EXPLORER_FILES			= 2;

	public static final int UPDATE_EDITOR_LOAD 				= 1;
	public static final int UPDATE_EDITOR_SAVE 				= 2;
	
	public static final int UPDATE_UPLOADER_UPLOAD			= 1;
	public static final int UPDATE_UPLOADER_CANCEL			= 2;
	public static final int UPDATE_UPLOADER_CLOSE			= 3;
	public static final int UPDATE_UPLOADER_OPEN			= 4;

	public static final int UPDATE_MENU_PORTS 				= 1;
	
	
	public static final byte UPLOAD_STATE_NONE				= 0;
	public static final byte UPLOAD_STATE_UPLOADING			= 1;
	public static final byte UPLOAD_STATE_CANCELING			= 2;
	public static final byte UPLOAD_STATE_DONE				= 3;
	public static final byte UPLOAD_STATE_FAIL				= 4;
	
	public static final byte CONNECTION_STATE_CLOSED		= 0;
	public static final byte CONNECTION_STATE_OPEN			= 1;
	public static final byte CONNECTION_STATE_CONNECTING	= 2;
	public static final byte CONNECTION_STATE_FAIL			= 3;

	public static final byte UPLOAD_MODE_SERIAL				= 0;
	public static final byte UPLOAD_MODE_TELNET				= 1;	

	public static final byte UPLOAD_MODE_DELAY				= 0;
	public static final byte UPLOAD_MODE_RESPONSE			= 1;

	
	
	
	private int doExit = -2; //-2 do not perform exit, -1 perform exit, if doExit==countObserables, exit application
	
	private ArrayList<String> cPorts;
	private String selectedPort;
	private int baudrate = 9600;
	
	private String telnetIP;
	
	//For editors we use the JPanel object to
	//Allow for different implementations later
	//Like Guide and AT support
	private ArrayList<JPanel> editors;
	private JPanel visibleEditor;
	
	private File explorerPath;

	private int uploadState = UPLOAD_STATE_NONE;

	private String toUploader;
	private String fromUploader;
	private String bufferConsole = "";
	
	private int lineDelay = 200;

	private boolean visibilityConsole = true;
	private boolean visibilityExplorer = true;
	
	private int uploadProgress = -1;	

	private Dimension dimensionConsole;
	private Dimension dimensionExplorer;

	private int uploadMethod = UPLOAD_MODE_SERIAL;

	private int connectionState = CONNECTION_STATE_CLOSED;
	
	private int uploadMode = UPLOAD_MODE_RESPONSE;
	private int consoleBufferSize = 100; //buffer size in KB
	
	public Model() {
		cPorts = new ArrayList<String>();
		editors = new ArrayList<JPanel>();
	}

	//I know that the following three methods are not offical MVC
	//But they make it so much easier to communicate between controllers :)
	public void updateObserver(int obsever, int action) {
		setChanged();
		notifyObservers(obsever*100+action);
	}
	
	public static int getObserver(int updateNumber) {
		return updateNumber/100;
	}
	
	public static int getAction(int updateNumber) {
		return updateNumber%100;
	}
	
	public void setConsoleBufferSize(int size) {
		consoleBufferSize = size;
	}
	
	public int getConsoleBufferSize() {
		return consoleBufferSize;
	}
	
	public void setBaudRate(int rate) {
		baudrate = rate;
	}
	
	public int getBaudRate() {
		return baudrate;
	}
	
	public int getUploadMode() {
		return uploadMode;
	}
	
	public void setUploadMode(int mode) {
		uploadMode = mode;
	}
	
	public void setTelnetIP(String ip) {
		telnetIP = ip;
		updateObserver(UPDATE_TOOLBAR, UPDATE_ACTION_GENERAL);
	}
	
	public String getTelnetIP() {
		return telnetIP;
	}

	public void setConnectionState(int state) {
		connectionState = state;
		updateObserver(UPDATE_CONNECTION_STATE, UPDATE_ACTION_GENERAL);
	}
	
	public int getConnectionState() {
		return connectionState;
	}
	
	public void setUploadMethod(int mode) {
		uploadMethod = mode;
		updateObserver(UPDATE_UPLOAD_MODE, UPDATE_ACTION_GENERAL);
	}
	
	public int getUploadMethod() {
		return uploadMethod;
	}

	public void setDimensionExplorer(Dimension dim) {
		dimensionExplorer = dim;
	}
	
	public Dimension getDimensionExplorer() {
		return dimensionExplorer;
	}

	public void setDimensionConsole(Dimension dim) {
		dimensionConsole = dim;
	}
	
	public Dimension getDimensionConsole() {
		return dimensionConsole;
	}
	
	public int getUploadProgress() {
		return uploadProgress;
	}
	
	public void setUploadProgress(int progress) {
		uploadProgress = progress;
		updateObserver(UPDATE_TOOLBAR, UPDATE_ACTION_GENERAL);
	}
	
	public void setVisibleConsole(boolean setVisible) {
		visibilityConsole = setVisible;
		updateObserver(UPDATE_VISIBILITY, UPDATE_ACTION_GENERAL);
	}
	
	public boolean isConsoleVisible() {
		return visibilityConsole;
	}
	
	public void setVisibleExplorer(boolean setVisible) {
		visibilityExplorer = setVisible;
		updateObserver(UPDATE_VISIBILITY, UPDATE_ACTION_GENERAL);
	}
	
	public boolean isExplorerVisible() {
		return visibilityExplorer;
	}

	public int getLineDelay() {
		return lineDelay;
	}
	
	public void setLineDelay(int delay) {
		lineDelay = delay;
	}
	
	public String getToUploader() {
		return toUploader;
	}
	public void setToUploader(String sendstr) {
		toUploader = sendstr;
		updateObserver(UPDATE_UPLOADER, UPDATE_ACTION_GENERAL);
	}
	public String getFromUploader() {
		return fromUploader;
	}
	public void setFromUploader(String recv) {
		fromUploader = recv;
		updateObserver(UPDATE_CONSOLE, UPDATE_ACTION_GENERAL);
	}
	
	public void addToConsoleBuffer(String msg) {
		bufferConsole = msg;
		updateObserver(UPDATE_CONSOLE, UPDATE_ACTION_GENERAL);
	}
	
	public String getConsoleBuffer() {
		return bufferConsole;
	}
	
	public int getUploadState() {
		return uploadState;
	}

	public void setUploadState(int state) {
		uploadState = state;
	}	

	public void setVisibleEditor(JPanel editor) {
		visibleEditor = editor;
		updateObserver(UPDATE_EDITOR, UPDATE_EDITOR_LOAD);
	}
	
	public JPanel getVisibleEditor() {
		return visibleEditor;
	}

	public ArrayList<JPanel> getEditors() {
		return editors;
	}
	
	public void addEditor(JPanel editor) {
		editors.add(editor);
	}
	public void removeEditor(JPanel editor) {
		editors.remove(editor);
	}
	
	public void addSerialPort(String item) {
		cPorts.add(item);
		updateObserver(UPDATE_MENU, UPDATE_MENU_PORTS);
	}
	
	public void removeSerialPort(String item) {
		cPorts.remove(item);
		updateObserver(UPDATE_MENU, UPDATE_MENU_PORTS);
	}

	public ArrayList<String> getSerialPorts() {
		return cPorts;
	}

	public void changeSerialPort(int index, String item) {
		cPorts.set(index, item);
		updateObserver(UPDATE_MENU, UPDATE_MENU_PORTS);
	}
	
	public void setSelectedDevice(String item) {
		selectedPort = item;
	}
	
	public String getSelectedDevice() {
		return selectedPort;
	}
	
	/**
	 * Sets the explorer's path
	 * @param the explorer's working directory
	 */
	public void setExplorerPath(File path) {
		explorerPath = path;
	}

	/**
	 * Gets the exlorer's path
	 * @return the explorer's working directory
	 */
	public File getExplorerPath() {
		return explorerPath;
	}
	
	/**
	 * Request an exit, should only be called once per session
	 * This methods notifies all observers, and attempts a safe shutdown
	 */
	public void requestExit() {
		doExit = -1;
		updateObserver(UPDATE_APPLICATION, UPDATE_ACTION_GENERAL);
	}
	
	/**
	 * Every observer should only call this method once per session
	 * When there is an request for shutdown, all observers have to 
	 * execute this method to inform the application that a observer
	 * is now ready to be fully closed.
	 */
	public void setExit() {
		doExit++;
		updateObserver(UPDATE_APPLICATION, UPDATE_ACTION_GENERAL);
	}
	
	/**
	 * Getter, used to inform the application when it's ready to shutdown
	 * @return the amount of observers that are closed, else returns -2 to not close, or -1 to perform a close
	 */
	public int getExit() {
		return doExit;
	}
	
}
