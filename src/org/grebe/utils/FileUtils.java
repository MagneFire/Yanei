/*
 * Utils class, used for some basic, but useful static functions
 */
package org.grebe.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.channels.FileChannel;
import java.util.regex.Pattern;


public class FileUtils {
	  
	  /**
	   * Deletes a folder with contents
	   * @param folder the folder to be deleted
	   */
	  public static void deleteFolder(File folder) {
		    File[] files = folder.listFiles();
		    if(files!=null) { //some JVMs return null for empty dirs
		        for(File f: files) {
		            if(f.isDirectory()) {
		                deleteFolder(f);
		            } else {
		                f.delete();
		            }
		        }
		    }
		    folder.delete();
		}
	// returns null if file isn't relative to folder
	public static String getRelativePath(File file, File folder) {
		try {
		    String filePath = file.getAbsolutePath();
		    String folderPath = folder.getCanonicalPath();

		    if (filePath.startsWith(folderPath)) {
		        return filePath.substring(folderPath.length() + 1);
		    } else {
		        return null;
		    }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static String getRelativePath(String targetPath, String basePath, 
	        String pathSeparator) {

	    //  We need the -1 argument to split to make sure we get a trailing 
	    //  "" token if the base ends in the path separator and is therefore
	    //  a directory. We require directory paths to end in the path
	    //  separator -- otherwise they are indistinguishable from files.
	    String[] base = basePath.split(Pattern.quote(pathSeparator), -1);
	    String[] target = targetPath.split(Pattern.quote(pathSeparator), 0);

	    //  First get all the common elements. Store them as a string,
	    //  and also count how many of them there are. 
	    String common = "";
	    int commonIndex = 0;
	    for (int i = 0; i < target.length && i < base.length; i++) {
	        if (target[i].equals(base[i])) {
	            common += target[i] + pathSeparator;
	            commonIndex++;
	        }
	        else break;
	    }

	    if (commonIndex == 0)
	    {
	        //  Whoops -- not even a single common path element. This most
	        //  likely indicates differing drive letters, like C: and D:. 
	        //  These paths cannot be relativized. Return the target path.
	        return targetPath;
	        //  This should never happen when all absolute paths
	        //  begin with / as in *nix. 
	    }

	    String relative = "";
	    if (base.length == commonIndex) {
	        //  Comment this out if you prefer that a relative path not start with ./
	        //relative = "." + pathSeparator;
	    }
	    else {
	        int numDirsUp = base.length - commonIndex - 1;
	        //  The number of directories we have to backtrack is the length of 
	        //  the base path MINUS the number of common path elements, minus
	        //  one because the last element in the path isn't a directory.
	        for (int i = 1; i <= (numDirsUp); i++) {
	            relative += ".." + pathSeparator;
	        }
	    }
	    relative += targetPath.substring(common.length());

	    return relative;
	}
	
	public static boolean isFileInClassPath(String file) {
		try {
			FileUtils.class.getClass().getResourceAsStream(file);
			return true;
		} catch (Exception ex) {}		
		return false;
	}
	
	public static boolean isFileInPath(String file) {
		File f = new File(file);
		if (f.exists()) {
			return true;
		}		
		return false;
	}
	
	public static void saveFile(String filename, String text) {
		try {
			PrintWriter out = new PrintWriter(filename);
			//out.println(text);
			out.print(text);
			out.close();
		} catch (Exception ex) {ex.printStackTrace();}
	}
	
	public static String readFile(String file) {
		try {
			
    		BufferedReader br = new BufferedReader(new FileReader(file));
    	    try {
    	        StringBuilder sb = new StringBuilder();
    	        String line = br.readLine();

    	        while (line != null) {
    	            sb.append(line);
    	            sb.append(System.getProperty("line.separator"));
    	            //sb.append(System.lineSeparator()); //not supported in java 1.6
    	            line = br.readLine();
    	        }
    	        String everything = sb.toString();
        		return everything;
    	    } finally {
    	        br.close();
    	    }
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	public static String readFile(InputStream file) {
		try {
    	    try {
    	        StringBuilder sb = new StringBuilder();
    	        int content;
    			while ((content = file.read()) != -1) {
    				sb.append((char)content);
    			}
    	        String everything = sb.toString();
        		return everything;
    	    } finally {
    	    }
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public static void copyFileUsingChannel(File source, File dest) throws IOException {
	    FileChannel sourceChannel = null;
	    FileChannel destChannel = null;
	    try {
	        sourceChannel = new FileInputStream(source).getChannel();
	        destChannel = new FileOutputStream(dest).getChannel();
	        destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
	       }finally{
	           sourceChannel.close();
	           destChannel.close();
	       }
	}
}
