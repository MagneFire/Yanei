package org.grebe.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;





public class Preferences {
/*
	public static ArrayList<String> getStoredEditors(String dir) {
		ArrayList<String> files = new ArrayList<String>();
			File root = new File(dir);
            for (File file : root.listFiles()) {
                if (!file.isDirectory()) {
                	//System.out.println(file.getName());
                	if (file.getName().charAt(0)!='.') {
                	 files.add(file.getName());
                	}
                    //node.add(new DefaultMutableTreeNode(file));                    	
                }
            }
            if (files.size()>0)  {
            	return files;
            }
		return null;
	}
*/
	/*public static Editor loadEditor(String filename) {
		try { 
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
		String xml = "";
		String currLine = reader.readLine();
		while (currLine != null) {
			xml+=currLine+"\n";
			currLine = reader.readLine();
		}
		System.out.println(xml);
		reader.close();
		XStream xstream = new XStream(new DomDriver());
		return (Editor)xstream.fromXML(xml);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static void saveEditor(Editor editor, String filename) {
		try { 
			XStream xstream = new XStream(new DomDriver());
			
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename+".xml"), "utf-8"));
			writer.write(xstream.toXML(editor));
			writer.flush();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
	
	private static void checkAndCreateConfig() {
		OutputStream output = null;
		File f = new File("config.properties");
		if(!f.exists()) { 
			try {
				output = new FileOutputStream("config.properties");
				output.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public static String readProperty(String key) {
		Properties prop = new Properties();
		InputStream input = null;
		checkAndCreateConfig();
		try {
	 
			input = new FileInputStream("config.properties");
	 
			// load a properties file
			prop.load(input);
	 
			return prop.getProperty(key);
			/*// get the property value and print it out
			System.out.println(prop.getProperty("database"));
			System.out.println(prop.getProperty("dbuser"));
			System.out.println(prop.getProperty("dbpassword"));*/
	 
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}
	
	public static void writeProperty(String key, String value) {
		Properties prop = new Properties();
		OutputStream output = null;
		InputStream input = null;
		checkAndCreateConfig();
		try {
	 
			input = new FileInputStream("config.properties");

			// load a properties file
			prop.load(input);
			

			output = new FileOutputStream("config.properties");
			// set the properties value
			prop.setProperty(key, value);
	 
			// save properties to project root folder
			prop.store(output, null);
	 
		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	 
		}
	}
	
}
