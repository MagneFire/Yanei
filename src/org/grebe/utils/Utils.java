/*
 * Utils class, used for some basic, but useful static functions
 */
package org.grebe.utils;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;


public class Utils {
	
	public static final String PATH_RESOURCES = "/org/grebe/resources/";

	public static final int OS_UNKNOWN			= 0; //Most likely a Linux distro
	public static final int OS_MACOSX			= 1;
	public static final int OS_WINDOWS 			= 2;

	/**
	   * Calculates the similarity (a number within 0 and 1) between two strings.
	   */
	  public static double similarity(String s1, String s2) {
	    String longer = s1, shorter = s2;
	    if (s1.length() < s2.length()) { // longer should always have greater length
	      longer = s2; shorter = s1;
	    }
	    int longerLength = longer.length();
	    if (longerLength == 0) { return 1.0; /* both strings are zero length */ }
	    /* // If you have StringUtils, you can use it to calculate the edit distance:
	    return (longerLength - StringUtils.getLevenshteinDistance(longer, shorter)) /
	                               (double) longerLength; */
	    return (longerLength - editDistance(longer, shorter)) / (double) longerLength;

	  }
	  
	  
	  // Example implementation of the Levenshtein Edit Distance
	  // See http://rosettacode.org/wiki/Levenshtein_distance#Java
	  public static int editDistance(String s1, String s2) {
	    s1 = s1.toLowerCase();
	    s2 = s2.toLowerCase();

	    int[] costs = new int[s2.length() + 1];
	    for (int i = 0; i <= s1.length(); i++) {
	      int lastValue = i;
	      for (int j = 0; j <= s2.length(); j++) {
	        if (i == 0)
	          costs[j] = j;
	        else {
	          if (j > 0) {
	            int newValue = costs[j - 1];
	            if (s1.charAt(i - 1) != s2.charAt(j - 1))
	              newValue = Math.min(Math.min(newValue, lastValue),
	                  costs[j]) + 1;
	            costs[j - 1] = lastValue;
	            lastValue = newValue;
	          }
	        }
	      }
	      if (i > 0)
	        costs[s2.length()] = lastValue;
	    }
	    return costs[s2.length()];
	  }

	public static long map(long x, long in_min, long in_max, long out_min, long out_max)
	{
	  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
	}
	
	public static boolean isBoolean(String str) {
		return "true".equals(str.toLowerCase()) || "false".equals(str.toLowerCase());
	}
	
	public static boolean isNumeric(String str)
	{
	    for (char c : str.toCharArray()) {
	        if (!Character.isDigit(c)) return false;
	    }
	    return true;
	}

	public static boolean isMac() {
		if (getOperatingSystem()==OS_MACOSX) {
			return true;
		}
		return false;
	}
	
	public static boolean isWindows() {
		if (getOperatingSystem()==OS_WINDOWS) {
			return true;
		}
		return false;
	}
	
	public static int getOperatingSystem() {
		//return OS_UNKNOWN; //Debug :)
        String osName = System.getProperty("os.name").toLowerCase();
        if (osName.startsWith("mac os x")) {
        	return OS_MACOSX;
        } else if (osName.startsWith("windows")) {
        	return OS_WINDOWS;
        }
        return OS_UNKNOWN;
	}
	
	public static InetAddress[] getAllLocalAddresses() {
		List<InetAddress> addrList = new ArrayList<InetAddress>();
		Enumeration<NetworkInterface> interfaces = null;
		try {
		    interfaces = NetworkInterface.getNetworkInterfaces();
		} catch (SocketException e) {
		    e.printStackTrace();
		}

		InetAddress localhost = null;

		try {
		    localhost = InetAddress.getByName("127.0.0.1");
		} catch (UnknownHostException e) {
		    e.printStackTrace();
		}

		while (interfaces.hasMoreElements()) {
		    NetworkInterface ifc = interfaces.nextElement();
		    Enumeration<InetAddress> addressesOfAnInterface = ifc.getInetAddresses();

		    while (addressesOfAnInterface.hasMoreElements()) {
		        InetAddress address = addressesOfAnInterface.nextElement();

		        if (!address.equals(localhost) && !address.toString().contains(":")) {
		            addrList.add(address);
		        }
		    }
		}
		return (InetAddress[]) addrList.toArray(new InetAddress[addrList.size()]);
	}

	/**
	  * Get the String residing on the clipboard.
	  *
	  * @return any text found on the Clipboard; if none found, return an
	  * empty String.
	  */
	  public static String getClipboardContents() {
	    String result = "";
	    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
	    //odd: the Object param of getContents is not currently used
	    Transferable contents = clipboard.getContents(null);
	    boolean hasTransferableText =
	      (contents != null) &&
	      contents.isDataFlavorSupported(DataFlavor.stringFlavor)
	    ;
	    if (hasTransferableText) {
	      try {
	        result = (String)contents.getTransferData(DataFlavor.stringFlavor);
	      }
	      catch (Exception ex){
	        System.out.println(ex);
	        ex.printStackTrace();
	      }
	    }
	    return result;
	  }
	
	
	public static boolean compareArrays(byte[] arr1, byte[] arr2) {
		if (arr1.length!=arr2.length) {
			return false;
		}
		for (int i=0;i<arr1.length;i++) {
			if (arr1[i]!=arr2[i]) {
				return false;
			}
		}		
		return true;
	}
}
