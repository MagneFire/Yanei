package org.grebe.views;


import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;

import javax.swing.*;

import org.grebe.components.GradientPanel;
import org.grebe.lang.Lang;
import org.grebe.model.Model;
import org.grebe.utils.Utils;


public class AboutView extends JDialog{

	private static final long serialVersionUID = 1L;
	
	private JFrame frame;
	
	public AboutView(JFrame frame) {
	    super(frame, true); //Disable acces to main frame
		this.frame = frame;

		setupLayout();
		pack();
		setLocationRelativeTo(null);
	}

	public JLabel getLink(final String link) {
		JLabel lbl = new JLabel("<html><a href=\"#\">"+link+"</a></html>");
		lbl.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
		lbl.addMouseListener(new MouseListener() {			
			@Override
			public void mouseReleased(MouseEvent e) {}			
			@Override
			public void mousePressed(MouseEvent e) {}			
			@Override
			public void mouseExited(MouseEvent e) {}			
			@Override
			public void mouseEntered(MouseEvent e) {}			
			@Override
			public void mouseClicked(MouseEvent e) {
	            URI uri;
				try {
					uri = new java.net.URI(link);
		            (new LinkRunner(uri)).execute();	
				} catch (URISyntaxException e1) {
					e1.printStackTrace();
				}			
			}
		});
		return lbl;
	}
	
	private void setupLayout() {
		setLayout(new BorderLayout());
		JPanel contentPane = new JPanel();
		contentPane.setLayout(new BorderLayout());
		contentPane.add(new GradientPanel(new JLabel("<html>"
				+ "<table border=\"0\">"
                + "<tr><td><img height=\"64\" width=\"64\" src=\""+getClass().getResource(Utils.PATH_RESOURCES+ "logo.png")+"\" /></td><td><font size=\"5\"><b>"+Lang.getText(Lang.TITLE)+"</b></font> "+Lang.getText(Lang.VERSION)+" "+Model.APPLICATION_VERSION+"<br />"+Lang.getText(Lang.TITLE_FULL)+"</td></tr>"
                + "</table>"
                +"</html>", JLabel.CENTER)), BorderLayout.NORTH);
		contentPane.add(new JLabel("<html>"
				+"<center>"
                + "<p>Grebe Software<br />"
                + "Copyright � 2015 by MagneFire</p>"
                +"</center></html>", JLabel.CENTER), BorderLayout.SOUTH);
		JPanel pnlLinks = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(0, 5, 0, 5);
		gbc.fill = GridBagConstraints.BOTH;

		gbc.gridwidth = 2;
		pnlLinks.add(new JLabel("<html>"
				+"<center>"
                + "<p>A Lua IDE for the ESP8266.<br />Developed to be simple and elegant to use.<br />For licenses look in the licenses directory<br />"
                + Lang.getText(Lang.AUTHOR)
                + "</p>"
                +"</center></html>", JLabel.CENTER), gbc);
		gbc.gridwidth = 1;
		
		gbc.gridx = 0;
		gbc.gridy++;
		pnlLinks.add(new JLabel("Internet:"), gbc);
		gbc.gridx++;
		pnlLinks.add(getLink("http://idanl.t15.org/"), gbc);
		gbc.gridx = 0;
		gbc.gridy++;
		pnlLinks.add(new JLabel("Product page:"), gbc);
		gbc.gridx++;
		pnlLinks.add(getLink("http://idanl.t15.org/?goto=yanei"), gbc);
		gbc.gridx = 0;
		gbc.gridy++;
		pnlLinks.add(new JLabel("Forums Thread:"), gbc);
		gbc.gridx++;
		pnlLinks.add(getLink("http://www.esp8266.com/viewtopic.php?f=22&t=1650"), gbc);
		gbc.gridx = 0;
		gbc.gridy++;
		pnlLinks.add(new JLabel("NodeMcu on GitHub:"), gbc);
		gbc.gridx++;
		pnlLinks.add(getLink("https://github.com/nodemcu/nodemcu-firmware"), gbc);
		gbc.gridx = 0;
		gbc.gridy++;
		pnlLinks.add(new JLabel("NodeMcu Forum: "), gbc);
		gbc.gridx++;
		pnlLinks.add(getLink("http://www.esp8266.com/viewforum.php?f=17"), gbc);
		gbc.gridx = 0;
		gbc.gridy++;
		pnlLinks.add(new JLabel("ESP8266 Forums: "), gbc);
		gbc.gridx++;
		pnlLinks.add(getLink("http://www.esp8266.com/"), gbc);
		
		contentPane.add(pnlLinks, BorderLayout.CENTER);
		add(contentPane, BorderLayout.CENTER);
		setTitle(Lang.getText(Lang.ABOUT));
		setResizable(false);
		setPreferredSize(Model.ABOUT_SIZE);
		JPanel pnlButtons = new JPanel();
		JButton btnOK;
		pnlButtons.add(btnOK = new JButton(Lang.getText(Lang.OK)));
		btnOK.setPreferredSize(new Dimension(90, btnOK.getPreferredSize().height));
		btnOK.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		add(pnlButtons, BorderLayout.SOUTH);
	}
	  public void showDialog() {
	    setLocationRelativeTo(this.frame);
	    setVisible(true);
	  }
	  private static class LinkRunner extends SwingWorker<Void, Void> {

		    private final URI uri;

		    private LinkRunner(URI u) {
		        if (u == null) {
		            throw new NullPointerException();
		        }
		        uri = u;
		    }

		    @Override
		    protected Void doInBackground() throws Exception {
		        Desktop desktop = java.awt.Desktop.getDesktop();
		        desktop.browse(uri);
		        return null;
		    }

		    @Override
		    protected void done() {
		        try {
		            get();
		        } catch (ExecutionException ee) {
		            handleException(uri, ee);
		        } catch (InterruptedException ie) {
		            handleException(uri, ie);
		        }
		    }

		    private static void handleException(URI u, Exception e) {
		        JOptionPane.showMessageDialog(null, "Sorry, a problem occurred while trying to open this link in your system's standard browser.", "A problem occured", JOptionPane.ERROR_MESSAGE);
		    }
		}
}
