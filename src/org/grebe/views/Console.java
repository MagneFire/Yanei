package org.grebe.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

import org.grebe.model.Model;

public class Console extends JPanel implements Observer{
	private static final long serialVersionUID = 1L;
	private JTextPane tPane;
	private Model model;
    private boolean isExited = false;
	
	Random rand = new Random();
	
	public Console(Model model) {
		this.model = model;
		setLayout(new BorderLayout());
		setPreferredSize(new Dimension(200, (int)getPreferredSize().getHeight()));
		tPane = new JTextPane();
		//tPane.setFont(new Font("Lucida Grande", Font.PLAIN, 14));
		tPane.setBackground(Color.WHITE);
		tPane.setForeground(Color.BLACK);
		tPane.setEditable(false);
		setLayout(new BorderLayout());
		JPanel pnl = new JPanel(new BorderLayout());
		pnl.add(tPane);
		add(new JScrollPane(pnl));
	}

	private void appendToPane(JTextPane tp, String msg, Color c)
    {
        AttributeSet aset = StyleContext.getDefaultStyleContext().addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);

        StyledDocument doc = tp.getStyledDocument();
        try {
			doc.insertString(doc.getLength(), msg, aset);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
        
        int len = tp.getDocument().getLength();
        tp.setCaretPosition(len);
        tp.setCharacterAttributes(aset, false);
    }

    public void close() {
    }

    public void update(Observable o, Object obj){

    	if (Model.getObserver((Integer)obj)==Model.UPDATE_APPLICATION) {
	    	if (model.getExit()>-2) { //Damn perform an exit
	    		if (!isExited) { //allow this class to only call model.setExit() once
	    			isExited = true;
	    			close();
	    			System.out.println("<g>"+o.getClass() + ": "+getClass()+" is closed!</g>");
	    			model.setExit();
	    		}
	    	}
    	} else if (Model.getObserver((Integer)obj)==Model.UPDATE_CONSOLE) {
    		if (model.getFromUploader()!=null) {
    			appendToPane(tPane, model.getFromUploader(), Color.BLACK);
    			model.setFromUploader(null); //clear the line
    		}
    		if (model.getConsoleBuffer()!=null) {
    			appendToPane(tPane, model.getConsoleBuffer(), Color.GRAY);
    			model.addToConsoleBuffer(null); //clear the line
    		}
    		
    	}
	}

}
