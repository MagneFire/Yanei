package org.grebe.views;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.*;
import javax.swing.Box.Filler;

import org.grebe.components.GradientPanel;
import org.grebe.lang.Lang;
import org.grebe.model.Model;
import org.grebe.utils.FileUtils;
import org.grebe.utils.Preferences;
import org.grebe.utils.Utils;

public class PreferencesView extends JDialog implements ActionListener{

	private static final long serialVersionUID = 1L;
	
	private JFrame frame;
	private Model model;
	private JButton btnOK, btnCancel, btnApply, btnBrowse, btnConvertToRelative;
    private JFileChooser fc;
    private JTextField tfExplorerPath, tfLineDelay, tfConsoleBufferSize;
    private JRadioButton btnUploadModeDelay, btnUploadModeResponse;
    private boolean refreshGUI = false;
	
	/*
	 * Entries:
	 * 	Font size
	 * 	line delay
	 * 	Explorer path
	 *  upload mode
	 *  console buffer size
	 */
	
	public PreferencesView(Model model, JFrame frame) {
	    super(frame, true); //Disable acces to main frame
		this.frame = frame;
		this.model = model;
		fc = new JFileChooser();
		fc.setAcceptAllFileFilterUsed(false);
	    fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		setupLayout();
		loadSettings();
		pack();
		setLocationRelativeTo(null);
	}
	
	private void setupLayout() {
		setLayout(new BorderLayout());
		

		JPanel contentPane = new JPanel();
		contentPane.setLayout(new BorderLayout());
		//For now the lblHeader is to fill space :)
		JLabel lblHeader = new JLabel("<html>"
				+ "<table border=\"0\">"
                + "<tr><td><img height=\"64\" width=\"64\" src=\""+getClass().getResource(Utils.PATH_RESOURCES+ "logo.png")+"\" /></td><td><font size=\"5\"><b>"+Lang.getText(Lang.TITLE)+"</b></font> "+Lang.getText(Lang.VERSION)+" "+Model.APPLICATION_VERSION+"<br />"+Lang.getText(Lang.TITLE_FULL)+"</td></tr>"
                + "</table>"
                +"</html>", JLabel.CENTER);
	
		contentPane.add(new GradientPanel(lblHeader), BorderLayout.NORTH);

		JPanel pnlS = new JPanel();
        JPanel pnlCenter = new JPanel();
        pnlCenter.setLayout(new BoxLayout(pnlCenter, BoxLayout.Y_AXIS));
        
		JPanel pnlLinks = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(0, 5, 5, 5);
		gbc.fill = GridBagConstraints.BOTH;
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(0, 5, 5, 5);

		gbc.gridwidth = 2;
		pnlLinks.add(new JLabel(Lang.getText(Lang.LOCATION_EXPLORER)+":", SwingConstants.LEFT), gbc);
		gbc.gridwidth = 1;

		gbc.gridx = 0;
		gbc.gridy++;
		pnlLinks.add(tfExplorerPath = new JTextField(35), gbc);
		gbc.gridx++;
		btnBrowse = new JButton("\u2026");
		btnBrowse.addActionListener(this);
		pnlLinks.add(btnBrowse, gbc);
		gbc.gridy++;
		gbc.gridx = 0;
		gbc.gridwidth = 2;
		btnConvertToRelative = new JButton(Lang.getText(Lang.CONVERT_TO_RELATIVE));
		btnConvertToRelative.addActionListener(this);
		pnlLinks.add(btnConvertToRelative, gbc);
		JPanel pnlL = new JPanel(new FlowLayout(FlowLayout.LEFT));
		//pnlL.add(new JPanel());
		pnlL.add(pnlLinks);
		pnlCenter.add(pnlL);
		

		pnlLinks = new JPanel(new GridBagLayout());
		gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(0, 5, 0, 5);
		gbc.fill = GridBagConstraints.BOTH;

		ButtonGroup bg = new ButtonGroup();
		btnUploadModeDelay = new JRadioButton(Lang.getText(Lang.MODE_DELAY));
		btnUploadModeResponse = new JRadioButton(Lang.getText(Lang.MODE_RESPONSE));
		bg.add(btnUploadModeDelay);
		bg.add(btnUploadModeResponse);
		pnlLinks.add(new JLabel(Lang.getText(Lang.UPLOAD_MODE)+":"), gbc);
		gbc.gridx++;
		pnlLinks.add(btnUploadModeDelay, gbc);
		gbc.gridx++;
		pnlLinks.add(btnUploadModeResponse, gbc);
		pnlL = new JPanel(new FlowLayout(FlowLayout.LEFT));
		//pnlL.add(new JPanel());
		//pnlL.setBorder(BorderFactory.createTitledBorder(Lang.getText(Lang.UPLOAD_MODE)));

		pnlL.add(pnlLinks);
		pnlCenter.add(pnlL);
		
		pnlLinks = new JPanel(new GridBagLayout());
		gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(0, 5, 0, 5);
		gbc.fill = GridBagConstraints.BOTH;

		pnlLinks.add(new JLabel(Lang.getText(Lang.LINE_DELAY)+":"), gbc);
		gbc.gridx++;
		tfLineDelay = new JTextField(4) {
			private static final long serialVersionUID = 1L;

			public void processKeyEvent(KeyEvent ev) {
				    char c = ev.getKeyChar();
				    try {
				      // Ignore all non-printable characters. Just check the printable ones.
				      if (c > 31 && c < 65535 && c != 127) {
				        Integer.parseInt(c + "");
				      }
				      super.processKeyEvent(ev);
				    }
				    catch (NumberFormatException nfe) {
				      // Do nothing. Character inputted is not a number, so ignore it.
				    }
				  }
				};
		pnlLinks.add(tfLineDelay, gbc);

		gbc.gridx++;
		pnlLinks.add(new JLabel(Lang.getText(Lang.CONSOLE_BUFFER_SIZE)+":"), gbc);
		gbc.gridx++;
		tfConsoleBufferSize = new JTextField(4){
			private static final long serialVersionUID = 1L;

			public void processKeyEvent(KeyEvent ev) {
				    char c = ev.getKeyChar();
				    try {
				      // Ignore all non-printable characters. Just check the printable ones.
				      if (c > 31 && c < 65535 && c != 127) {
				        Integer.parseInt(c + "");
				      }
				      super.processKeyEvent(ev);
				    }
				    catch (NumberFormatException nfe) {
				      // Do nothing. Character inputted is not a number, so ignore it.
				    }
				  }
				};
		pnlLinks.add(tfConsoleBufferSize, gbc);
		
		pnlL = new JPanel(new FlowLayout(FlowLayout.LEFT));
		//pnlL.add(new JPanel());
		pnlL.add(pnlLinks);
		pnlCenter.add(pnlL);
		

		
		Box.Filler glue = (Filler) Box.createVerticalGlue();
	    glue.changeShape(glue.getMinimumSize(), 
	                    new Dimension(0, Short.MAX_VALUE), // make glue greedy
	                    glue.getMaximumSize());
		pnlCenter.add(glue);
		pnlS.add(pnlCenter);
		contentPane.add(pnlS, BorderLayout.CENTER);
		add(contentPane, BorderLayout.CENTER);
		
		setTitle(Lang.getText(Lang.PREFERENCES));
		setResizable(false);
		setPreferredSize(Model.PREFERENCES_SIZE);
		JPanel pnlButtons = new JPanel();
		pnlButtons.add(btnOK = new JButton(Lang.getText(Lang.OK)));
		pnlButtons.add(btnCancel = new JButton(Lang.getText(Lang.CANCEL)));
		pnlButtons.add(btnApply = new JButton(Lang.getText(Lang.APPLY)));
		btnOK.setPreferredSize(new Dimension(90, btnOK.getPreferredSize().height));
		btnCancel.setPreferredSize(new Dimension(90, btnCancel.getPreferredSize().height));
		btnApply.setPreferredSize(new Dimension(90, btnApply.getPreferredSize().height));
		btnOK.addActionListener(this);
		btnCancel.addActionListener(this);
		btnApply.addActionListener(this);
		add(pnlButtons, BorderLayout.SOUTH);
	}
	
	private boolean saveSettings() {
		refreshGUI = true;
		model.setExplorerPath(new File(tfExplorerPath.getText()));

    	if (model.getExplorerPath().getPath()!=null) {
    		Preferences.writeProperty("explorerPath", model.getExplorerPath().getPath());
    	}
    	if (Utils.isNumeric(tfLineDelay.getText())) {
    		//save to model, and to file
    		model.setLineDelay(Integer.parseInt(tfLineDelay.getText()));
			Preferences.writeProperty("lineDelay", tfLineDelay.getText());
    	} else {
    		JOptionPane.showMessageDialog(frame, Lang.getText(Lang.MSG_LINE_DELAY_NOT_NUMERIC), Lang.getText(Lang.TITLE_FORMAT_ERROR), JOptionPane.ERROR_MESSAGE);
    		tfLineDelay.setText(""+model.getLineDelay());
    		return false;
    	}
    	if (Utils.isNumeric(tfConsoleBufferSize.getText())) {
    		//save to model, and to file
    		model.setConsoleBufferSize(Integer.parseInt(tfConsoleBufferSize.getText()));
			Preferences.writeProperty("consoleBufferSize", tfConsoleBufferSize.getText());
    	} else {
    		JOptionPane.showMessageDialog(frame, Lang.getText(Lang.MSG_CONSOLE_BUFFER_SIZE_NOT_NUMERIC), Lang.getText(Lang.TITLE_FORMAT_ERROR), JOptionPane.ERROR_MESSAGE);
    		tfConsoleBufferSize.setText(""+model.getConsoleBufferSize());
    		return false;
    	}
    	if (btnUploadModeResponse.isSelected()) {
    		model.setUploadMode(Model.UPLOAD_MODE_RESPONSE);
    	} else if (btnUploadModeDelay.isSelected()) {
    		model.setUploadMode(Model.UPLOAD_MODE_DELAY);
    	}
    	Preferences.writeProperty("uploadMode", String.valueOf(model.getUploadMode()));

    	return true;//If everything saved succesfully return true
	}
	
	private void loadSettings() {
		tfLineDelay.setText(""+model.getLineDelay());
		tfConsoleBufferSize.setText(""+model.getConsoleBufferSize());
		fc.setCurrentDirectory(model.getExplorerPath().getAbsoluteFile());
		if (model.getUploadMode()==Model.UPLOAD_MODE_DELAY) {
			btnUploadModeDelay.setSelected(true);
		} else if (model.getUploadMode()==Model.UPLOAD_MODE_RESPONSE) {
			btnUploadModeResponse.setSelected(true);
		}
		tfExplorerPath.setText(model.getExplorerPath().getPath());
		btnConvertToRelative.setEnabled(model.getExplorerPath().isAbsolute()); //if the file is relative it will not be absolute, so when its absolute, we can conver it
	}
	
	public void refreshGUI() {
		model.updateObserver(Model.UPDATE_EXPLORER, Model.UPDATE_EXPLORER_FILES);
		model.updateObserver(Model.UPDATE_EXPLORER, Model.UPDATE_EXPLORER_SELECTION);
	}
	
	public void showDialog() {
		loadSettings();
	    setLocationRelativeTo(this.frame);
	    setVisible(true);
	  }

	@Override
	public void actionPerformed(ActionEvent ev) {
		if (ev.getSource() instanceof JButton) {
			JButton btn = (JButton) ev.getSource();
			if (btn == btnOK) {
				saveSettings();
				setVisible(false);	
				if (refreshGUI) {
					refreshGUI();
				}
			} else if (btn == btnCancel) {
				setVisible(false);
				if (refreshGUI) {
					refreshGUI();
				}
			} else if (btn ==  btnApply) {
				saveSettings();
			} else if (btn ==  btnBrowse) {
	            int returnVal = fc.showOpenDialog(frame);
	            
	            if (returnVal == JFileChooser.APPROVE_OPTION) {
	                File file = fc.getSelectedFile().getAbsoluteFile();
	                String path = FileUtils.getRelativePath(file, new File(".")); //Get the relative path without any ".." or ".", convert to relative allows to use those
	                if (path==null) {
		                //Path is not relative
	                	tfExplorerPath.setText(file.getPath());
		        		btnConvertToRelative.setEnabled(file.isAbsolute()); //if the file is relative it will not be absolute, so when its absolute, we can conver it
	                } else {
	                	//Path is relative, so we cannot change the path to relative again :)
	                	tfExplorerPath.setText(path);	    
		        		btnConvertToRelative.setEnabled(false);
	                }
	            }
			} else if (btn ==  btnConvertToRelative) {
			    String relPath;
				try {
					relPath = FileUtils.getRelativePath(tfExplorerPath.getText(), new File(".").getCanonicalPath()+File.separator, File.separator);
				    tfExplorerPath.setText(relPath);
				} catch (IOException e) {
					System.err.println("Error while trying to convert to relative path\n"+e.getMessage());
					//e.printStackTrace();
				}
			}
		}
	}

}
