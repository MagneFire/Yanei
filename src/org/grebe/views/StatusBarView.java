package org.grebe.views;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

import org.grebe.model.Model;

public class StatusBarView extends JPanel implements Observer{
	private static final long serialVersionUID = 1L;
	private Model model;
    private boolean isExited = false;
	private JLabel lblStatus, lblConnected;
    
	public StatusBarView(Model model) {
		this.model = model;
		setLayout(new BorderLayout(5, 0));
		
		setBorder(new BevelBorder(BevelBorder.LOWERED));
		setPreferredSize(new Dimension(this.getPreferredSize().width, 16));
		//setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		lblStatus = new JLabel("status: IDLE");
		lblStatus.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
		add(lblStatus, BorderLayout.CENTER);
		lblConnected = new JLabel("Connected to COM4");
		lblConnected.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
		//add(lblConnected, BorderLayout.EAST);
	}

	private void setStatus(String text) {
		lblStatus.setText("status: "+text);
	}
	
    public void close() {
    }

    public void update(Observable o, Object obj){

    	if (Model.getObserver((Integer)obj)==Model.UPDATE_APPLICATION) {
	    	if (model.getExit()>-2) { //Damn perform an exit
	    		if (!isExited) { //allow this class to only call model.setExit() once
	    			isExited = true;
	    			close();
	    			System.out.println("<g>"+o.getClass() + ": "+getClass()+" is closed!</g>");
	    			model.setExit();
	    		}
	    	}
    	} else if ((Model.getObserver((Integer)obj)==Model.UPDATE_UPLOAD_MODE) || (Model.getObserver((Integer)obj)==Model.UPDATE_CONNECTION_STATE)) {
    		switch(model.getConnectionState()) {
    		case Model.CONNECTION_STATE_FAIL:
    			setStatus("Failed to connect!");
    			break;
    		case Model.CONNECTION_STATE_CLOSED:
    			setStatus("Connection is closed");
    			break;
    		case Model.CONNECTION_STATE_CONNECTING:
    			setStatus("Connecting...");
    			break;
    		case Model.CONNECTION_STATE_OPEN:
    			setStatus("Connected");
    			break;
    		}
    	} else if ((Model.getObserver((Integer)obj)==Model.UPDATE_UPLOADER) || (Model.getObserver((Integer)obj)==Model.UPDATE_TOOLBAR)) {
    		switch(model.getUploadState()) {
    		case Model.UPLOAD_STATE_CANCELING:
    			setStatus("Cancelling upload...");
    			break;
    		case Model.UPLOAD_STATE_DONE:
    			setStatus("Upload completed");
    			break;
    		case Model.UPLOAD_STATE_FAIL:
    			setStatus("Error while uploading!");
    			break;
    		case Model.UPLOAD_STATE_NONE:
    			setStatus("-");
    			break;
    		case Model.UPLOAD_STATE_UPLOADING:
    			setStatus("Uploading...");
    			if (model.getUploadProgress()>=0 || model.getUploadProgress()<=100) {
    				setStatus("Uploading "+model.getUploadProgress()+"%");
    			}
    			break;
    		}
    		//if (model.getUploadState()!=Model.UPLOAD_STATE_UPLOADING) {
			//setVisibilityProgress(false);
			//}
    	} 
    }	
}
